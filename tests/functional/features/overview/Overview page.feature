@OverviewPage
Feature: Overview Page

Scenario: Overview 
    When I open the merchant portal
    Then The selected tab is "Overview"
    
    When I click the Your Sales tab 
    Then The url should contain "your-sales"
    
    # When I click the Your Customers tab 
    # Then The url should contain "your-customers"
    
    #When I click on the signout link 
    #Then The signout popup should show
    
    #When I click on the signout button in the signout popup
    #Then The URL should change