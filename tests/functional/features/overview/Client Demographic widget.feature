@ClientDemographicWidget
Feature: Overview Page

Scenario: Client demographic widget
	When I open the merchant portal
	Then The client demographic widget is visible
	And The client demographic widget title is "Client demographic"
	
Scenario: Race section
	And The section title is "Race"
	#And The donut chart has "5" slices

 Scenario: Gender section
 	And The section title is "Gender"
 	And The total of the male and female graphs must add up to "100"