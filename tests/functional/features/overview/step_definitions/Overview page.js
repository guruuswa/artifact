module.exports = function () {

    var expect = require('../../../helpers/expect');
    var tableHelper = require('../../../helpers/table_helper');


    this.setDefaultTimeout(30000);
   
    this.When('I open the merchant portal', function () {
        return browser.driver.get(browser.baseUrl);
    });

    this.Then('The selected tab is "$tab"', function(tab) {
       return expect(element(by.css('.nav-link.is-active')).getText()).to.eventually.equal(tab);
    });

    this.When('I click the Your Sales tab', function() {
        return element(by.css('a[href="#/your-sales"]')).click();
    });

    this.When('I click the Your Customers tab', function() {
        return element(by.css('a[href="#/your-customers"]')).click();
    });

    this.Then('The url should contain "$url"', function(url) {
        return expect(browser.getLocationAbsUrl()).to.eventually.contains(url);
    });

    this.When('I click on the signout link', function(){
        return element(by.css('a[ng-click="openSignOut()"]')).click(); 
    });

    this.Then('The signout popup should show',function(){
        return expect(element(by.css('.sb-signout')).isDisplayed()).to.eventually.equal(true);
    });

    this.When('I click on the signout button in the signout popup',function(){
        return element(by.css('div[ng-click="signout()"]')).click();       
    });

    this.Then('The URL should change',function(){
        return expect(browser.getCurrentUrl()).to.eventually.contains('https://merchantonline.standardbank.co.za/MerchantPortal/MerchantPortal/MerchAuthentication/Login');
    });
};