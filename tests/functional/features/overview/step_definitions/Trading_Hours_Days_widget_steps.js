var Trading_Time = function () {

    var expect = require('../../../helpers/expect');
    var tableHelper = require('../../../helpers/table_helper');    

    this.setDefaultTimeout(30000);


    this.Then('the Trading Days widget is visible', function () {
        return expect(element(by.css('.sb-tradingDays')).isDisplayed()).to.eventually.equal(true);
    });

    this.Then('the Trading Days widget title is "$TradingDays"', function(TradingDays) {
       return expect(element(by.css('.sb-tradingDays .widget__title')).getText()).to.eventually.equal(TradingDays);
    });

    this.Then('the footer title for Trading Days widget is "$Footer"',function(Footer){
        return expect(element(by.css('.sb-tradingDays .widget__footer')).getText()).to.eventually.equal(Footer);
    });

    this.Then('the legend for trading days is displayed representing "$Highest" and "$Lowest" values', function(Highest,Lowest) {
      
        if(element(by.css('.sb-tradingDays .tradingDays-item__chartLegend')).isDisplayed()){
            //console.log("Legend is displayed");
            
            element.all(by.css('.sb-tradingDays .tradingDays-item__chartLegend .tradingDays-item__content__headingText')).first().getText().then(function(Headtxt){
                if(Headtxt==Highest){
                    //console.log("Highest value is displayed");
                } 
            })

            element.all(by.css('.sb-tradingDays .tradingDays-item__chartLegend .tradingDays-item__content__headingText')).last().getText().then(function(Headtxt){
                if(Headtxt==Lowest){
                    //console.log("Lowest value is displayed");
                } 
            })

        }
        return expect(element(by.css('.sb-tradingDays .tradingDays-item__chartLegend')).isDisplayed()).to.eventually.equal(true);

    });

    this.Then('the "Sales value" section is displayed in Trading Days widget', function () {
        return expect(element(by.css('.sb-tradingDays .salesByDay')).isDisplayed()).to.eventually.equal(true);
    });

    this.Then('the Sales value icon is displayed in Trading Days widget', function () {
        return expect(element(by.css('.sb-tradingDays .salesByDay .tradingDays-item__icon-wrapper')).isDisplayed()).to.eventually.equal(true);
    });

    this.Then('a colour bar representing week days is displayed in Sales value section', function () {
        return expect(element.all(by.css('.sb-tradingDays .salesByDay  .tradingDays-item__row .tradingDays-item__content__dayBlock')).count()).to.eventually.equal(7); 
    });

    this.Then('the week starts from "Monday" for Sales value', function () {
       return expect(element.all(by.css('.sb-tradingDays .salesByDay  .tradingDays-item__row .tradingDays-item__content__dayBlock')).first().getText()).to.eventually.equal('M'); 
    });
     
    this.Then('the box has "Dark" colour for "Highest" sales value', function() {
        var SalesValAry =[]
        return element.all(by.css('.sb-tradingDays .salesByDay  .tradingDays-item__row .tradingDays-item__content__dayBlock')).then(function(days){
            days.forEach(function(days,i){
                days.click();
                 element.all(by.css('.sb-tradingDays  .sb-tradingDays__tooltip__text')).first().getText().then(function(SalesVal){
                    var str = SalesVal.slice(1);
                    var str1 = str.replace(/,/g,"");

                    SalesValAry.push({day: i,val: str1})
                    SalesValAry.sort(function(a,b){return a.val-b.val});
                    if(SalesValAry.length == 7){
                          return  element.all(by.css('.sb-tradingDays .salesByDay  .tradingDays-item__content__dayBlock')).get(SalesValAry[SalesValAry.length-1].day).isDisplayed().then(function(){ 
                            element(by.css('.sb-tradingDays .salesByDay  .blockDark')).isDisplayed().then(function(){
                                switch(SalesValAry[SalesValAry.length-1].day){
                                    case 0:
                                        //console.log('Monday has the highest sales value');
                                        break;
                                    case 1:
                                        //console.log('Tuesday has the highest sales value');
                                        break;
                                    case 2:
                                        //console.log('Wednesday has the highest sales value');
                                        break;
                                    case 3:
                                        //console.log('Thursday has the highest sales value');
                                        break;
                                    case 4:
                                        //console.log('Friday has the highest sales value');
                                        break;
                                    case 5:
                                        //console.log('Saturday has the highest sales value');
                                        break;
                                    case 6:
                                        //console.log('Sunday has the highest sales value');
                                        break;
                                    default:
                                        //console.log("Default");
                                }
                            })
                        });
                    }
                });
            });
        });
    });

    this.Then('the box is in "Medium" colour for the next 3 values of sales value', function () {
        var SalesValAryMed =[]
                    
        return element.all(by.css('.sb-tradingDays .salesByDay  .tradingDays-item__row .tradingDays-item__content__dayBlock')).then(function(days){
            days.forEach(function(days,i){
                days.click();
                 element.all(by.css('.sb-tradingDays  .sb-tradingDays__tooltip__text')).first().getText().then(function(SalesVal){
                    var str = SalesVal.slice(1);
                    var str1 = str.replace(/,/g,"");

                    SalesValAryMed.push({day: i,val: str1})
                    SalesValAryMed.sort(function(a,b){return a.val-b.val});
        
                    var MedSaleDay = "";
                    
                    if(SalesValAryMed.length == 7){
                        //console.log(SalesValAryMed.valueOf());
                        return element.all(by.css('.sb-tradingDays .salesByDay  .blockMedium')).then(function(medEle){
                            medEle.forEach(function(medElem,v){
                                if (element.all(by.css('.sb-tradingDays .salesByDay  .tradingDays-item__content__dayBlock')).get(SalesValAryMed[v+3].day).isDisplayed()){
                                    medElem.getText().then(function(Txt){
                                        switch(SalesValAryMed[v+3].day){
                                            case 0:
                                                    //console.log('Monday has the Medium sales value');
                                                break;
                                            case 1:
                                                    //console.log('Tuesday has the Medium sales value');
                                                break;
                                            case 2:
                                                    //console.log('Wednesday has the Medium sales value');
                                                break;
                                            case 3:
                                                    //console.log('Thursday has Medium sales value');
                                                break;
                                            case 4:
                                                    //console.log('Friday has Medium sales value');
                                                break;
                                            case 5:
                                                    //console.log('Saturday has the Medium sales value');
                                                break;
                                            case 6:
                                                    //console.log('Sunday has the Medium sales value');
                                                break;
                                            default:
                                                //console.log("Default");
                                        }
                                    })   
                                }
                                
                            })
                        })
                    }
                });
            });
        });
    });

    this.Then('for the last 3 values of sales value the box has "Light" colour', function () {
       var SalesValLightAry =[]
                    
        return element.all(by.css('.sb-tradingDays .salesByDay  .tradingDays-item__row .tradingDays-item__content__dayBlock')).then(function(days){
            days.forEach(function(days,i){
                days.click();
                 element.all(by.css('.sb-tradingDays  .sb-tradingDays__tooltip__text')).first().getText().then(function(SalesVal){
                    var str = SalesVal.slice(1);
                    var str1 = str.replace(/,/g,"");

                    SalesValLightAry.push({day: i,val: str1})
                    SalesValLightAry.sort(function(a,b){return a.val-b.val});
                    
                    if(SalesValLightAry.length == 7){
                        //console.log(SalesValLightAry.valueOf());
                        return element.all(by.css('.sb-tradingDays .salesByDay  .blockLight')).then(function(lighEle){
                            lighEle.forEach(function(lighElem,v){
                                if (element.all(by.css('.sb-tradingDays .salesByDay  .tradingDays-item__content__dayBlock')).get(SalesValLightAry[v].day).isDisplayed()){
                                    lighElem.getText().then(function(Txt){
                                        switch(SalesValLightAry[v].day){
                                            case 0:
                                                    //console.log('Monday has the Light sales value');
                                                break;
                                            case 1:
                                                    //console.log('Tuesday has the Light sales value');
                                                break;
                                            case 2:
                                                    //console.log('Wednesday has the Light sales value');
                                                break;
                                            case 3:
                                                    //console.log('Thursday has Light sales value');
                                                break;
                                            case 4:
                                                    //console.log('Friday has Light sales value');
                                                break;
                                            case 5:
                                                    //console.log('Saturday has the Light sales value');
                                                break;
                                            case 6:
                                                    //console.log('Sunday has the Light sales value');
                                                break;
                                            default:
                                                //console.log("Default");
                                        }
                                    })   
                                }
                                
                            })
                        })
                    }
                });
            });
        });
    });

    this.Then('the "Sales volume" section is displayed in Trading Days widget', function () {
       return expect(element(by.css('.sb-tradingDays .valueByDay')).isDisplayed()).to.eventually.equal(true);
    });

    this.Then('the Sales volume icon is displayed in Trading Days widget', function () {
       return expect(element(by.css('.sb-tradingDays .valueByDay .tradingDays-item__icon-wrapper')).isDisplayed()).to.eventually.equal(true);
    });

  this.Then('a colour bar representing week days is displayed in Sales volume section', function () {
        return expect(element.all(by.css('.sb-tradingDays .valueByDay  .tradingDays-item__row .tradingDays-item__content__dayBlock')).count()).to.eventually.equal(7); 
    });

    this.Then('the week starts from "Monday" for Sales volume', function () {
       return expect(element.all(by.css('.sb-tradingDays .valueByDay  .tradingDays-item__row .tradingDays-item__content__dayBlock')).first().getText()).to.eventually.equal('M'); 
    });
     
    this.Then('the box has "Dark" colour for "Highest" sales volume', function() {
        var SalesValAry =[]
       
        return element.all(by.css('.sb-tradingDays .valueByDay  .tradingDays-item__row .tradingDays-item__content__dayBlock')).then(function(days){
            days.forEach(function(days,i){
                days.click();
                 element.all(by.css('.sb-tradingDays  .sb-tradingDays__tooltip__text')).last().getText().then(function(SalesVol){
                    var str = SalesVol.slice(1);
                    var str1 = str.replace(/,/g,"");

                    SalesValAry.push({day: i,val: str1})
                    SalesValAry.sort(function(a,b){return a.val-b.val});
                    if(SalesValAry.length == 7){
                      return  element.all(by.css('.sb-tradingDays .valueByDay  .tradingDays-item__content__dayBlock')).get(SalesValAry[SalesValAry.length-1].day).isDisplayed().then(function(){
                            element(by.css('.sb-tradingDays .valueByDay  .blockDark')).isDisplayed().then(function(){
                                switch(SalesValAry[SalesValAry.length-1].day){
                                    case 0:
                                        //console.log('Monday has the highest sales volume');
                                        break;
                                    case 1:
                                        //console.log('Tuesday has the highest sales volume');
                                        break;
                                    case 2:
                                        //console.log('Wednesday has the highest sales volume');
                                        break;
                                    case 3:
                                        //console.log('Thursday has the highest sales volume');
                                        break;
                                    case 4:
                                        //console.log('Friday has the highest sales volume');
                                        break;
                                    case 5:
                                        //console.log('Saturday has the highest sales volume');
                                        break;
                                    case 6:
                                        //console.log('Sunday has the highest sales volume');
                                        break;
                                    default:
                                        //console.log("Default");
                                }
                            })
                        });
                    }
                });
            });
        });

    });

    this.Then('the box is in "Medium" colour for the next 3 values of sales volume', function () {
        var SalesValAryMed =[]
                    
        return element.all(by.css('.sb-tradingDays .valueByDay  .tradingDays-item__row .tradingDays-item__content__dayBlock')).then(function(days){
            days.forEach(function(days,i){
                days.click();
                 element.all(by.css('.sb-tradingDays  .sb-tradingDays__tooltip__text')).last().getText().then(function(SalesVal){
                    var str = SalesVal.slice(1);
                    var str1 = str.replace(/,/g,"");

                    SalesValAryMed.push({day: i,val: str1})
                    SalesValAryMed.sort(function(a,b){return a.val-b.val});
        
                    var MedSaleDay = "";
                    var medBlocks = 0  ;
                    
                    if(SalesValAryMed.length == 7){
                        //console.log(SalesValAryMed.valueOf());
                        return element.all(by.css('.sb-tradingDays .valueByDay  .blockMedium')).then(function(medEle){
                            medEle.forEach(function(medElem,v){
                                if (element.all(by.css('.sb-tradingDays .valueByDay  .tradingDays-item__content__dayBlock')).get(SalesValAryMed[v+3].day).isDisplayed()){
                                    medElem.getText().then(function(Txt){
                                        switch(SalesValAryMed[v+3].day){
                                            case 0:
                                                    //console.log('Monday has the Medium sales value');
                                                break;
                                            case 1:
                                                    //console.log('Tuesday has the Medium sales value');
                                                break;
                                            case 2:
                                                    //console.log('Wednesday has the Medium sales value');
                                                break;
                                            case 3:
                                                    //console.log('Thursday has Medium sales value');
                                                break;
                                            case 4:
                                                    //console.log('Friday has Medium sales value');
                                                break;
                                            case 5:
                                                    //console.log('Saturday has the Medium sales value');
                                                break;
                                            case 6:
                                                    //console.log('Sunday has the Medium sales value');
                                                break;
                                            default:
                                                //console.log("Default");
                                        }
                                    })   
                                }
                                
                            })
                        })
                    }
                });
            });
        });
    });

    this.Then('for the last 3 values of sales volume the box has "Light" colour', function () {
        var SalesValLightAry =[]
                    
        return element.all(by.css('.sb-tradingDays .valueByDay  .tradingDays-item__row .tradingDays-item__content__dayBlock')).then(function(days){
            days.forEach(function(days,i){
                days.click();
                 element.all(by.css('.sb-tradingDays  .sb-tradingDays__tooltip__text')).last().getText().then(function(SalesVal){
                    var str = SalesVal.slice(1);
                    var str1 = str.replace(/,/g,"");

                    SalesValLightAry.push({day: i,val: str1})
                    SalesValLightAry.sort(function(a,b){return a.val-b.val});
                    var count  = 0;
                    var u = 4;

                    if(SalesValLightAry.length == 7){
                        //console.log(SalesValLightAry.valueOf());
                        return element.all(by.css('.sb-tradingDays .valueByDay  .blockLight')).then(function(lighEle){
                            lighEle.forEach(function(lighElem,v){
                                if (element.all(by.css('.sb-tradingDays .valueByDay  .tradingDays-item__content__dayBlock')).get(SalesValLightAry[v].day).isDisplayed()){
                                    lighElem.getText().then(function(Txt){
                                        switch(SalesValLightAry[v].day){
                                            case 0:
                                                    //console.log('Monday has the Light sales volume');
                                                break;
                                            case 1:
                                                    //console.log('Tuesday has the Light sales volume');
                                                break;
                                            case 2:
                                                    //console.log('Wednesday has the Light sales volume');
                                                break;
                                            case 3:
                                                    //console.log('Thursday has Light sales volume');
                                                break;
                                            case 4:
                                                    //console.log('Friday has Light sales volume');
                                                break;
                                            case 5:
                                                    //console.log('Saturday has the Light sales volume');
                                                break;
                                            case 6:
                                                    //console.log('Sunday has the Light sales volume');
                                                break;
                                            default:
                                                //console.log("Default");
                                        }
                                    })   
                                }
                                
                            })
                        })
                    }
                });
            });
        });
    });


    this.Then('the Trading Hours widget is visible', function () {
        return expect(element(by.css('.sb-tradingHours')).isDisplayed()).to.eventually.equal(true);
    });
    
    
    this.Then('the Trading Hours widget title is "$title"', function(title) {
        return expect(element(by.css('.sb-tradingHours .widget__title')).getText()).to.eventually.equal(title);
    });

    this.Then('the footer title for Trading Hours widget is "$Footer"',function(Footer){
        return expect(element(by.css('.sb-tradingHours .widget__footer__copy')).getText()).to.eventually.equal(Footer);
    });


    this.Then('the legend for trading hours is displayed representing "Highest", "Lowest" and "No activity" values', function () {
        return element.all(by.css('.tradingHours__label__title')).then(function(labelCount){
            labelCount.forEach(function(labelObj){
                labelObj.getText().then(function(labelValue){
                    switch(labelValue){
                        case 'Highest':
                            //console.log('Highest is displayed');
                            break;
                        case 'Lowest':
                            //console.log('Lowest is displayed');
                            break;
                        case 'No activity':
                            //console.log('No activity is displayed');
                            break;
                        default:
                            //console.log('Unknown is displayed');
                    }
                })
            })

       })

        return element(by.css('.tradingHours__label__activity--light')).isDisplayed().then(function(){
            return element(by.css('.tradingHours__label__activity--medium')).isDisplayed().then(function(){
                return expect(element(by.css('.tradingHours__label__activity--light')).isDisplayed()).to.eventually.equal(true);
            });
        });
    });
    
    this.Then('the "Sales value" section is displayed in Trading hours widget', function () {
        return element.all(by.css('.sb-tradingHours ')).first().isDisplayed().then(function(){
            return expect(element.all(by.css('.tradingHours__chart .tradingHours__chart__title')).first().getText()).to.eventually.equal("Sales value");
        });
    });


    this.Then('the Sales value icon is displayed in Trading hours widget', function () {
        return expect(element.all(by.css('.sb-tradingHours .tradingHours__chart__icon--wrapper__icon')).first().isDisplayed()).to.eventually.equal(true);
    });
    
    
    this.Then('semi-circle bar graph has 24 slices representing "$Sales" from 0:00 hrs to 24:00 hrs', function(Sales) {
        return element.all(by.css('.sb-tradingHours ')).first().isDisplayed().then(function(){
            return expect(element.all(by.css('.sb-halfPie g.valueSlice')).count()).to.eventually.equal(24); 
        });
    });

    this.Then('the hours "8:00", "13:00" and "17:00" are specified on axis for "$Sales" section', function(Sales) {
        return element.all(by.css('.sb-tradingHours ')).first().isDisplayed().then(function(){
            return element.all(by.css('.sb-halfPie g.valueSlice')).get(8).getText().then(function(timeVal_start){
                return element.all(by.css('.sb-halfPie g.valueSlice')).get(13).getText().then(function(timeVal_break){
                    return element.all(by.css('.sb-halfPie g.valueSlice')).get(17).getText().then(function(timeVal_end){
                        //console.log(timeVal_start, '-', timeVal_break , '-', timeVal_end);
                        return expect((timeVal_start === "08:00") && (timeVal_break === "13:00") && (timeVal_end === "17:00")).to.equal(true);
                    })
                })
            })
       })
    });

    this.Then('the slices having "0" sales value is in "#cccccc" colour',function(){
        return element.all(by.css('.tradingHours__chart .valueSlice')).each(function(singleSliceObj, i) {
            browser.actions().mouseMove(singleSliceObj).perform();
            singleSliceObj.click();
            var timeRange = element(by.css('div[data="valueData"] .sb-halfPie__tooltip__timeRange')).getText();
            var value = element(by.css('div[data="valueData"] .sb-halfPie__tooltip__value')).getText();
            value.then(function(val) {
                timeRange.then(function(tr) {
                    ////console.log(tr + " = " + val);
                    var color = element.all(by.css('.tradingHours__chart g.valueSlice .blockStyle')).get(i);
                        color.getAttribute('fill').then(function(fillAttrib){
                        if(val=='R 0.00'){
                            return expect(fillAttrib).to.equal('#cccccc');
                        }
                        else{
                            return expect(fillAttrib).not.to.equal('#cccccc');
                        }
                    });
                });
            });
        });

    });


    this.Then('one third of total slices with low sales value have "Light" colour',function(){
        var ValLightAry = [];
        return element.all(by.css('.tradingHours__chart .valueSlice')).each(function(singleSliceObj, i) {
            browser.actions().mouseMove(singleSliceObj).perform();
            singleSliceObj.click();
            var timeRange = element(by.css('div[data="valueData"] .sb-halfPie__tooltip__timeRange')).getText();
            var value = element(by.css('div[data="valueData"] .sb-halfPie__tooltip__value')).getText();
            value.then(function(val) {
                timeRange.then(function(tr) {
                    var str = val.slice(2);                             //removing the symbol('R') and space
                    var str1 = str.replace(/,/g,"");                    //removing ','(Comma) in the amount (Sales value)
                    ValLightAry.push({key1: i,key2: parseFloat(str1)})  //Creating array with time-sales Value as key-value pair 
                    
                    if(ValLightAry.length == 24){
                        // //console.log(ValLightAry.valueOf());                     //Printing array once all the values are updated
                        ValLightAry.sort(function(a,b){return b.key2-a.key2});      //Sorting array in Descending order
                        // //console.log(ValLightAry.valueOf());                     //Printing array aftr sorting
                        var count = 0;
                        for(var itr =0;itr<ValLightAry.length;itr++){           //Getting count of 'R 0.00' sales value
                            if(ValLightAry[itr].key2 == 0){
                                count = count+1
                            }
                        }

                        for(var j=0;j<count;j++){                               //Removing the elements with 'R 0.00' sales value from array
                            ValLightAry.pop();
                        }

                        totlight = ValLightAry.length/3;                        //Getting 1/3rd of sales value (Which is non-zero)

                        for(var k=ValLightAry.length-1; k>=(ValLightAry.length-parseInt(totlight)); k--){                //Getting Colour attribute for the slices which has one third of low sales value      
                            var color = element.all(by.css('.tradingHours__chart g.valueSlice .blockStyle')).get(ValLightAry[k].key1);
                            color.getAttribute('fill').then(function(fillAttrib){
                                //console.log(fillAttrib);
                                return expect(fillAttrib).to.equal('#adc2eb');
                            });
                        }
                    }
                });
            });
        });
    });

    this.Then('the slices with medium sales value have "Medium" colour',function(){
        var ValMedAry = [];
        return element.all(by.css('.tradingHours__chart .valueSlice')).each(function(singleSliceObj, i) {
            browser.actions().mouseMove(singleSliceObj).perform();
            singleSliceObj.click();
            var timeRange = element(by.css('div[data="valueData"] .sb-halfPie__tooltip__timeRange')).getText();
            var value = element(by.css('div[data="valueData"] .sb-halfPie__tooltip__value')).getText();
            value.then(function(val) {
                timeRange.then(function(tr) {
                    var str = val.slice(2);                             //removing the symbol('R') and space
                    var str1 = str.replace(/,/g,"");                    //removing ','(Comma) in the amount (Sales value)
                    ValMedAry.push({key1: i,key2: parseFloat(str1)})  //Creating array with time-sales Value as key-value pair 
                    
                    if(ValMedAry.length == 24){
                        // //console.log(ValMedAry.valueOf());                     //Printing array once all the values are updated
                        ValMedAry.sort(function(a,b){return b.key2-a.key2});  //Sorting array in Descending order
                        // //console.log(ValMedAry.valueOf());                     //Printing array aftr sorting
                        var count = 0;
                        for(var itr =0;itr<ValMedAry.length;itr++){           //Getting count of 'R 0.00' sales value
                            if(ValMedAry[itr].key2 == 0){
                                count = count+1
                            }
                        }

                        for(var j=0;j<count;j++){                               //Removing the elements with 'R 0.00' sales value from array
                            ValMedAry.pop();
                        }

                        totMed = ValMedAry.length/3;                        //Getting 1/3rd of medium sales value (Which is non-zero)

                        for(var k=(ValMedAry.length-parseInt(totMed)-1); k>=(ValMedAry.length-(2*parseInt(totMed))); k--){                //Getting Colour attribute for the slices which has one third of medium sales value      
                            var color = element.all(by.css('.tradingHours__chart g.valueSlice .blockStyle')).get(ValMedAry[k].key1);
                            color.getAttribute('fill').then(function(fillAttrib){
                                //console.log(fillAttrib);
                                return expect(fillAttrib).to.equal('#5c85d6');
                            });
                        }
                    }
                });
            });
        });
    });
    
    this.Then('the rest of slices with high sales value have "Dark" colour',function(){
       var ValHighAry = [];
        return element.all(by.css('.tradingHours__chart .valueSlice')).each(function(singleSliceObj, i) {
            browser.actions().mouseMove(singleSliceObj).perform();
            singleSliceObj.click();
            var timeRange = element(by.css('div[data="valueData"] .sb-halfPie__tooltip__timeRange')).getText();
            var value = element(by.css('div[data="valueData"] .sb-halfPie__tooltip__value')).getText();
            value.then(function(val) {
                timeRange.then(function(tr) {
                    var str = val.slice(2);                             //removing the symbol('R') and space
                    var str1 = str.replace(/,/g,"");                    //removing ','(Comma) in the amount (Sales value)
                    ValHighAry.push({key1: i,key2: parseFloat(str1)})  //Creating array with time-sales Value as key-value pair 
                    
                    if(ValHighAry.length == 24){
                        // //console.log(ValHighAry.valueOf());                     //Printing array once all the values are updated
                        ValHighAry.sort(function(a,b){return b.key2-a.key2});  //Sorting array in Descending order
                        // //console.log(ValHighAry.valueOf());                     //Printing array aftr sorting
                        var count = 0;
                        for(var itr =0;itr<ValHighAry.length;itr++){           //Getting count of 'R 0.00' sales value
                            if(ValHighAry[itr].key2 == 0){
                                count = count+1
                            }
                        }

                        for(var j=0;j<count;j++){                               //Removing the elements with 'R 0.00' sales value from array
                            ValHighAry.pop();
                        }

                        totHigh = ValHighAry.length/3;                        //Getting highest sales value (Which is non-zero)

                        for(var k=(ValHighAry.length-(2*parseInt(totHigh))-1); k>=0; k--){                //Getting Colour attribute for the slices which has high sales value      
                            var color = element.all(by.css('.tradingHours__chart g.valueSlice .blockStyle')).get(ValHighAry[k].key1);
                            color.getAttribute('fill').then(function(fillAttrib){
                                //console.log(fillAttrib);
                                return expect(fillAttrib).to.equal('#3366cc');
                            });
                        }
                    }
                });
            });
        });
    });
    

    
    this.When('I hover over on a slice in sales value section', function () {
        return element.all(by.css('.tradingHours__chart .valueSlice')).each(function(singleSliceObj, i) {
            browser.actions().mouseMove(singleSliceObj).perform();
            singleSliceObj.click();
            return element(by.css('div[data="valueData"] .sb-halfPie__tooltip__timeRange')).isDisplayed().then(function(){
                 return expect(element(by.css('div[data="valueData"] .sb-halfPie__tooltip__value')).isDisplayed()).to.eventually.equal(true);
            })
        });
    });

    this.Then('the Sales value and the time period is displayed', function () {
        return element.all(by.css('.tradingHours__chart .valueSlice')).each(function(singleSliceObj, i) {
            browser.actions().mouseMove(singleSliceObj).perform();
            singleSliceObj.click();
            return element(by.css('div[data="valueData"] .sb-halfPie__tooltip__value')).isDisplayed().then(function(){
                 return expect(element(by.css('div[data="valueData"] .sb-halfPie__tooltip__timeRange')).isDisplayed()).to.eventually.equal(true);
            })
        });
    });

    this.Then('the "Sales volume" section is displayed in Trading hours widget', function () {
        return element.all(by.css('.sb-tradingHours ')).last().isDisplayed().then(function(){
            return expect(element.all(by.css('.tradingHours__chart .tradingHours__chart__title')).last().getText()).to.eventually.equal("Sales volume");
        });
    });


    this.Then('the Sales volume icon is displayed in Trading hours widget', function () {
       return expect (element.all(by.css('.sb-tradingHours .tradingHours__chart__icon--wrapper__icon')).last().isDisplayed()).to.eventually.equal(true);
    });
    
   
    this.Then('the hours "8:00", "13:00" and "17:00" are specified on axis', function () {
        return element.all(by.css('.sb-tradingHours ')).last().isDisplayed().then(function(){
            return element.all(by.css('.sb-halfPie g.volumeSlice')).get(8).getText().then(function(timeVal_start){
                return element.all(by.css('.sb-halfPie g.volumeSlice')).get(13).getText().then(function(timeVal_break){
                    return element.all(by.css('.sb-halfPie g.volumeSlice')).get(17).getText().then(function(timeVal_end){
                        //console.log(timeVal_start, '-', timeVal_break , '-', timeVal_end);
                        return expect((timeVal_start === "08:00") && (timeVal_break === "13:00") && (timeVal_end === "17:00")).to.equal(true);
                    })
                })
            })
       })
    });

    this.Then('the slices having "0" sales volume is in "#cccccc" colour',function(){
        return element.all(by.css('.tradingHours__chart .volumeSlice')).each(function(singleSliceObj, i) {
            browser.actions().mouseMove(singleSliceObj).perform();
            singleSliceObj.click();
            var timeRange = element(by.css('div[data="volumeData"] .sb-halfPie__tooltip__timeRange')).getText();
            var volume = element(by.css('div[data="volumeData"] .sb-halfPie__tooltip__value')).getText();
            volume.then(function(vol) {
                timeRange.then(function(tr) {
                    // //console.log(tr + " = " + vol);
                    var color = element.all(by.css('.tradingHours__chart g.volumeSlice .blockStyle')).get(i);
                        color.getAttribute('fill').then(function(fillAttrib){
                        if(vol=='0'){
                            return expect(fillAttrib).to.equal('#cccccc');
                        }
                        else{
                            return expect(fillAttrib).not.to.equal('#cccccc');
                        }
                    });
                });
            });
        });
    });


    this.Then('one third of total slices with low sales volume have "Light" colour',function(){
         var ValLightAry = [];
        return element.all(by.css('.tradingHours__chart .volumeSlice')).each(function(singleSliceObj, i) {
            browser.actions().mouseMove(singleSliceObj).perform();
            singleSliceObj.click();
            var timeRange = element(by.css('div[data="volumeData"] .sb-halfPie__tooltip__timeRange')).getText();
            var value = element(by.css('div[data="volumeData"] .sb-halfPie__tooltip__value')).getText();
            value.then(function(val) {
                timeRange.then(function(tr) {
                    ValLightAry.push({key1: i,key2: parseInt(val)})  //Creating array with time-sales volume as key-value pair 
                    
                    if(ValLightAry.length == 24){
                        // //console.log(ValLightAry.valueOf());                     //Printing array once all the values are updated
                        ValLightAry.sort(function(a,b){return b.key2-a.key2});  //Sorting array in Descending order
                        // //console.log(ValLightAry.valueOf());                     //Printing array aftr sorting
                        var count = 0;
                        for(var itr =0;itr<ValLightAry.length;itr++){           //Getting count of '0' sales volume
                            if(ValLightAry[itr].key2 == 0){
                                count = count+1
                            }
                        }

                        for(var j=0;j<count;j++){                               //Removing the elements with '0' sales volume from array
                            ValLightAry.pop();
                        }
                        // //console.log(ValLightAry.valueOf());
                        totlight = ValLightAry.length/3;                        //Getting 1/3rd of sales volume (Which is non-zero)
                        // //console.log(totlight);
                        var temp =0;

                        for(var k=(ValLightAry.length-parseInt(totlight)); k<=(ValLightAry.length-1); k++){                //Getting Colour attribute for the slices which has one third of low sales volume      
                            if(ValLightAry[k].key2 == ValLightAry[k-1].key2){
                                if((k==(ValLightAry.length-parseInt(totlight))) || (temp != 0)){ 
                                    temp = ValLightAry[k].key2
                                    var color = element.all(by.css('.tradingHours__chart g.volumeSlice .blockStyle')).get(ValLightAry[k].key1);
                                    color.getAttribute('fill').then(function(fillAttrib){
                                        ////console.log(fillAttrib);
                                        return expect(fillAttrib).to.equal('#5c85d6');
                                    });
                                }
                                else{
                                    var color = element.all(by.css('.tradingHours__chart g.volumeSlice .blockStyle')).get(ValLightAry[k].key1);
                                    temp = 0;
                                    color.getAttribute('fill').then(function(fillAttrib){
                                        //console.log(fillAttrib);
                                        return expect(fillAttrib).to.equal('#adc2eb');
                                    });
                                }
                            }
                            else{
                                var color = element.all(by.css('.tradingHours__chart g.volumeSlice .blockStyle')).get(ValLightAry[k].key1);
                                temp = 0;
                                color.getAttribute('fill').then(function(fillAttrib){
                                    //console.log(fillAttrib);
                                    return expect(fillAttrib).to.equal('#adc2eb');
                                });
                            }
                        }
                    }
                });
            })
        });
    });

    this.Then('the slices with medium sales volume have "Medium" colour',function(){
         var ValMedAry = [];
        return element.all(by.css('.tradingHours__chart .volumeSlice')).each(function(singleSliceObj, i) {
            browser.actions().mouseMove(singleSliceObj).perform();
            singleSliceObj.click();
            var timeRange = element(by.css('div[data="volumeData"] .sb-halfPie__tooltip__timeRange')).getText();
            var value = element(by.css('div[data="volumeData"] .sb-halfPie__tooltip__value')).getText();
            value.then(function(val) {
                timeRange.then(function(tr) {
                    ValMedAry.push({key1: i,key2: parseInt(val)})  //Creating array with time-sales volume as key-value pair 
                    if(ValMedAry.length == 24){
                        // //console.log(ValMedAry.valueOf());                     //Printing array once all the values are updated
                        ValMedAry.sort(function(a,b){return b.key2-a.key2});  //Sorting array in Descending order
                        // //console.log(ValMedAry.valueOf());                     //Printing array aftr sorting
                        var count = 0;
                        for(var itr =0;itr<ValMedAry.length;itr++){           //Getting count of '0' sales volume
                            if(ValMedAry[itr].key2 == 0){
                                count = count+1
                            }
                        }

                        for(var j=0;j<count;j++){                               //Removing the elements with '0' sales volume from array
                            ValMedAry.pop();
                        }

                        totMed = ValMedAry.length/3;                        //Getting 1/3rd of medium sales volume (Which is non-zero)

                        var temp =0;

                        for(var k=(ValMedAry.length-(2*parseInt(totMed))); k<=(ValMedAry.length-parseInt(totMed)-1); k++){                //Getting Colour attribute for the slices which has one third of low sales volume      
                            if(ValMedAry[k].key2 == ValMedAry[k-1].key2){
                                if((k==(ValMedAry.length-parseInt(totlight))) || (temp != 0)){ 
                                    temp = ValMedAry[k].key2
                                    var color = element.all(by.css('.tradingHours__chart g.volumeSlice .blockStyle')).get(ValMedAry[k].key1);
                                    color.getAttribute('fill').then(function(fillAttrib){
                                        ////console.log(fillAttrib);
                                        return expect(fillAttrib).to.equal('##3366cc');
                                    });
                                }
                                else{
                                    var color = element.all(by.css('.tradingHours__chart g.volumeSlice .blockStyle')).get(ValMedAry[k].key1);
                                    temp = 0;
                                    color.getAttribute('fill').then(function(fillAttrib){
                                        //console.log(fillAttrib);
                                        return expect(fillAttrib).to.equal('#5c85d6');
                                    });
                                }
                            }
                            else{
                                var color = element.all(by.css('.tradingHours__chart g.volumeSlice .blockStyle')).get(ValMedAry[k].key1);
                                temp = 0;
                                color.getAttribute('fill').then(function(fillAttrib){
                                    //console.log(fillAttrib);
                                    return expect(fillAttrib).to.equal('#5c85d6');
                                });
                            }
                        }
                    }
                });
            });
        });
    });
    
    this.Then('the rest of slices with high sales volume have "Dark" colour',function(){
        var ValHighAry = [];
        return element.all(by.css('.tradingHours__chart .volumeSlice')).each(function(singleSliceObj, i) {
            browser.actions().mouseMove(singleSliceObj).perform();
            singleSliceObj.click();
            var timeRange = element(by.css('div[data="volumeData"] .sb-halfPie__tooltip__timeRange')).getText();
            var value = element(by.css('div[data="volumeData"] .sb-halfPie__tooltip__value')).getText();
            value.then(function(val) {
                timeRange.then(function(tr) {
                    ValHighAry.push({key1: i,key2: parseInt(val)})  //Creating array with time-sales Value as key-value pair 
                    
                    if(ValHighAry.length == 24){
                        // //console.log(ValHighAry.valueOf());                     //Printing array once all the values are updated
                        ValHighAry.sort(function(a,b){return b.key2-a.key2});  //Sorting array in Descending order
                        // //console.log(ValHighAry.valueOf());                     //Printing array aftr sorting
                        var count = 0;
                        for(var itr =0;itr<ValHighAry.length;itr++){           //Getting count of '0' sales volume
                            if(ValHighAry[itr].key2 == 0){
                                count = count+1
                            }
                        }

                        for(var j=0;j<count;j++){                               //Removing the elements with '0' sales volume from array
                            ValHighAry.pop();
                        }

                        totHigh = ValHighAry.length/3;                        //Getting highest sales volume (Which is non-zero)

                        for(var k=0; k<=(ValHighAry.length-(2*parseInt(totMed))-1); k++){                //Getting Colour attribute for the slices which has high sales volume      
                            var color = element.all(by.css('.tradingHours__chart g.volumeSlice .blockStyle')).get(ValHighAry[k].key1);
                            color.getAttribute('fill').then(function(fillAttrib){
                                //console.log(fillAttrib);
                                return expect(fillAttrib).to.equal('#3366cc');
                            });
                        }
                    }
                });
            });
        });
    });
    
  
    this.When('I hover over on a slice in sales volume section', function () {
        return element.all(by.css('.tradingHours__chart .volumeSlice')).each(function(singleSliceObj, i) {
            browser.actions().mouseMove(singleSliceObj).perform();
            singleSliceObj.click();
            return element(by.css('div[data="volumeData"] .sb-halfPie__tooltip__timeRange')).isDisplayed().then(function(){
                 return expect(element(by.css('div[data="volumeData"] .sb-halfPie__tooltip__value')).isDisplayed()).to.eventually.equal(true);
            })
        }); 
    });

    this.Then('the Sales volume and the time period is displayed', function () {
        return element.all(by.css('.tradingHours__chart .volumeSlice')).each(function(singleSliceObj, i) {
            browser.actions().mouseMove(singleSliceObj).perform();
            singleSliceObj.click();
            return element(by.css('div[data="volumeData"] .sb-halfPie__tooltip__value')).isDisplayed().then(function(){
                 return expect(element(by.css('div[data="volumeData"] .sb-halfPie__tooltip__timeRange')).isDisplayed()).to.eventually.equal(true);
            })
        });
    });







};

module.exports = Trading_Time;
