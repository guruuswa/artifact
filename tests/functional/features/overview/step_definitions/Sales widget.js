module.exports = function () {

	var expect = require('../../../helpers/expect');
    var tableHelper = require('../../../helpers/table_helper');

    this.setDefaultTimeout(30000);

    this.Then('The sales widget is visible', function () {
        return expect(element(by.css('.widget--spotlight')).isDisplayed()).to.eventually.equal(true);
    });

    this.Then('The sales widget title is "$title"', function (title) {
        return expect(element(by.css('.widget--spotlight .widget__title')).getText()).to.eventually.equal(title); 
    });

    this.Then('The sales widget footer text is "$title"', function (title) {
        return expect(element(by.css('.widget--spotlight .widget__footer__copy')).getText()).to.eventually.equal(title); 
    });
    
    this.Then('The first section title is "$title"',function(title){
    	return expect(element(by.css('li.spotlight-item:nth-child(1) .spotlight-item__content__title')).getText()).to.eventually.equal(title); 
    });

	this.Then('The second section title is "$title"',function(title){
    	return expect(element(by.css('li.spotlight-item:nth-child(2) .spotlight-item__content__title')).getText()).to.eventually.equal(title); 
	});

 	this.Then('The third section title is "$title"',function(title){
    	return expect(element(by.css('li.spotlight-item:nth-child(3) .spotlight-item__content__title')).getText()).to.eventually.equal(title); 
    });


    var arrowDirection = function(index) {
        return element(by.css('li.spotlight-item:nth-child('+ index +') .spotlight-item__content__wrapper__column__change--item.ng-binding')).getText().then(function(percentage) {
            
            var direction = parseInt(percentage) > 0 ? '.icon-arrow-up' : '.icon-arrow-down';
            
            return expect(element(by.css('li.spotlight-item:nth-child('+ index +') ' + direction)).isDisplayed()).to.eventually.equal(true); 
        });
    };

    // TODO: double check the calculation
    var calcPercentage = function(index) {

        var thisWeek    = element(by.css('li.spotlight-item:nth-child('+ index +') .spotlight-item__content__value--item:nth-child(1)')).getText();
        var lastWeek    = element(by.css('li.spotlight-item:nth-child('+ index +') .spotlight-item__content__value--item:nth-child(2)')).getText();
        var percentage  = element(by.css('li.spotlight-item:nth-child('+ index +') .spotlight-item__content__wrapper__column__change--item.ng-binding')).getText();

        return percentage.then(function(percent) {
            thisWeek.then(function(tw) {
                lastWeek.then(function(lw) {
                    tw = tableHelper.percentFormat(tw);
                    lw = tableHelper.percentFormat(lw);
                    var final = ( (tw - lw) / lw ) * 100;

                    if(lw == 0) final = -100;
                    if(tw == 0) final = 100;
                    if(tw == 0 && lw == 0) final = 0;
                    
                    return expect( Math.round(final) ).to.equal( parseInt(percent) );
                });
            });
        });
    };

	this.Then('The arrow direction must be correct for sales value changes',function(){
        return arrowDirection(1);
    });

    this.Then('The arrow direction must be correct for sales volume changes',function(){
        return arrowDirection(2);
    });

    this.Then('The arrow direction must be correct for average transaction size changes',function(){
        return arrowDirection(3);
    });

    this.Then('The percentage change must be correct for sales value changes',function(){
        return calcPercentage(1);
    });

    this.Then('The percentage change must be correct for sales volume changes',function(){
        return calcPercentage(2);
    });

    this.Then('The percentage change must be correct for average transaction size changes',function(){
        return calcPercentage(3);
    });
};

