module.exports = function () {

    var expect = require('../../../helpers/expect');
    var tableHelper = require('../../../helpers/table_helper');


    this.setDefaultTimeout(30000);
    
    this.Then('The client demographic widget is visible',function(){    
        return expect(element(by.css('.sb-clientDemographic')).isDisplayed()).to.eventually.equal(true);
    });

    this.Then('The client demographic widget title is "$WidgetName"',function(WidgetName){
        return expect(element(by.css('.sb-clientDemographic .widget__title')).getText()).to.eventually.equal(WidgetName);
    });

    this.Then('The section title is "Race"', function() {
        return expect(element.all(by.css('.clientDemographic__title--label')).first().getText()).to.eventually.equal("Race");
    });

    this.Then('The section title is "Gender"', function() {
        return expect(element.all(by.css('.clientDemographic__title--label')).last().getText()).to.eventually.equal("Gender");
    });

    this.Then('The donut chart has "$count" slices',function(count){
        return expect(element.all(by.css('.clientDemographic__group__chart--race g.slice')).count()).to.eventually.equal(parseInt(count));
    });

    this.Then('The total of the male and female graphs must add up to "$total"',function(total){

        var elems = element.all(by.css('.clientDemographic__group__chart__container__title--value'));
      
        return elems.last().getText().then(function(male) {

            elems.first().getText().then(function(female) {
                male    = tableHelper.percentFormat(male);
                female  = tableHelper.percentFormat(female);
                return expect( male + female ).to.equal(100);
            });

        });
    });

};