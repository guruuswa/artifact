@TradingHours_Days_Widget
Feature: Overview Page

Scenario: Trading Days widget
	When I open the merchant portal
	And the Trading Days widget is visible
	And the Trading Days widget title is "Trading days"
	And the footer title for Trading Days widget is "What is this?"
	And the legend for trading days is displayed representing "Highest" and "Lowest" values

	And the "Sales value" section is displayed in Trading Days widget
	#And the Sales value icon is displayed in Trading Days widget
	And a colour bar representing week days is displayed in Sales value section
	And the week starts from "Monday" for Sales value
	And the box has "Dark" colour for "Highest" sales value
	And the box is in "Medium" colour for the next 3 values of sales value
	And for the last 3 values of sales value the box has "Light" colour

	And the "Sales volume" section is displayed in Trading Days widget
	#And the Sales volume icon is displayed in Trading Days widget
	And a colour bar representing week days is displayed in Sales volume section
	And the week starts from "Monday" for Sales volume
	And the box has "Dark" colour for "Highest" sales volume
	And the box is in "Medium" colour for the next 3 values of sales volume
	And for the last 3 values of sales volume the box has "Light" colour

	
Scenario: Trading Hours widget
	And the Trading Hours widget is visible
	And the Trading Hours widget title is "Trading hours"
	And the footer title for Trading Hours widget is "What is this?"
	And the legend for trading hours is displayed representing "Highest", "Lowest" and "No activity" values

	And the "Sales value" section is displayed in Trading hours widget
	#And the Sales value icon is displayed in Trading hours widget
	And semi-circle bar graph has 24 slices representing "Sales value" from 0:00 hrs to 24:00 hrs
	And the hours "8:00", "13:00" and "17:00" are specified on axis for "Sales value" section
	#And the slices having "0" sales value is in "#cccccc" colour
	#And one third of total slices with low sales value have "Light" colour
	#And the slices with medium sales value have "Medium" colour
	#And the rest of slices with high sales value have "Dark" colour



	And the "Sales volume" section is displayed in Trading hours widget
	#And the Sales volume icon is displayed in Trading hours widget 
	And semi-circle bar graph has 24 slices representing "Sales volume" from 0:00 hrs to 24:00 hrs
	And the hours "8:00", "13:00" and "17:00" are specified on axis for "Sales volume" section
	#And the slices having "0" sales volume is in "#cccccc" colour
	#And one third of total slices with low sales volume have "Light" colour
	#And the slices with medium sales volume have "Medium" colour
	#And the rest of slices with high sales volume have "Dark" colour