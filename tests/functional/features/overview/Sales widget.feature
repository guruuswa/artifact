@Sales_Widget
Feature: Overview Page

Scenario: Sales widget
	When I open the merchant portal
	
	Then The sales widget is visible
	And The sales widget title is "Sales"
	And The sales widget footer text is "What is this?"

	And The first section title is "Sales value"
	And The second section title is "Sales volume"
	And The third section title is "Average transaction size"
	
	And The arrow direction must be correct for sales value changes
	And The arrow direction must be correct for sales volume changes
	And The arrow direction must be correct for average transaction size changes

	And The percentage change must be correct for sales value changes
	And The percentage change must be correct for sales volume changes
	And The percentage change must be correct for average transaction size changes