exports.config = {
    
    directConnect: true, 

    framework: 'custom',
    
    allScriptsTimeout: 30000,

    specs: ['tests/functional/features/**/*.feature'],

    resultJsonOutputFile: 'reports/report.json',

    frameworkPath: 'node_modules/protractor-cucumber-framework',

    chromeDriver: 'node_modules/protractor/selenium/chromedriver',
    
    seleniumServerJar: 'node_modules/protractor/selenium/selenium-server-standalone-2.48.2.jar',

    multiCapabilities: [{
        browserName: 'chrome',
        count: 1
    }],  
    
    cucumberOpts: {
        format: 'pretty',
        require: ['tests/functional/features/**/*.js']
    },

    onPrepare: function () { 
        browser.baseUrl = 'http://localhost:3000';
        browser.manage().window().setSize(1920, 1080);
        browser.manage().window().maximize();
    }
};