#### Facts
	Charge number: 42121
	Team number:   D-16-000100
	On-boarding number: 246




#### Links

	https://dsbgchop11.standardbank.co.za/#/

	https://testmobilebanking.standardbank.co.za:5084/SBSA/MerchantInsights/#/
	
	http://staging-sbgmp.globalkinetic.com:8080/#/

	http://bamboo.standardbank.co.za:8096/bamboo/browse/MP
	
	http://stash.standardbank.co.za:7990/projects/SBMP/

	http://stash.standardbank.co.za:7990/projects/CC/repos/merchantweb/browse

	http://dchop65.standardbank.co.za:(8101,9000) (karaf / karaf) (mcdc / mcdc123)
	
	http://dchop66.standardbank.co.za:9200/_search?= (kibana / kibana)
	
	http://confluence.standardbank.co.za:8060/display/MC/to+be+Architecture




#### Running cucumber tests on Windows
The selenium server must run in its own command window

    cd sbg-merchant-portal    
    node node_modules/protractor/bin/webdriver-manager update --standalone
    node node_modules/protractor/bin/webdriver-manager start

Edit sbg-merchant-portal\tests\protractor.config and replace the seleniumServerJar: line with 

    seleniumAddress: 'http://localhost:4444/wd/hub'

Run the tests 

    gulp cucumber
    gulp cucumber --tags=@tag1,@tag2




#### Importing demo data
	
	npm install elasticdump -g

Extract logstash-cards.json.gz

	elasticdump --input=logstash-cards-mapping.json --output=http://localhost:9200/logstash-cards --type=mapping
	
	elasticdump --input=logstash-cards.json --output=http://localhost:9200/logstash-cards --bulk=true --limit=25000




#### Enabling CORS on Elastic

config/elasticsearch.yml

	http.cors.enabled : true
	http.cors.allow-origin : "*"
	http.cors.allow-methods : OPTIONS, HEAD, GET, POST, PUT, DELETE
	http.cors.allow-headers : X-Requested-With,X-Auth-Token,Content-Type,Content-Length,JWT-Token,Namespace,ProcessResponse
	
to allow access to your elastic from across the network 
	
	network.bind_host: "0.0.0.0"