describe('salesTrendsFactory', () => {
    let factory, $httpBackend, requestArguments, requestHandler;

    beforeEach(() => {
        inject((_salesTrendsFactory_, _$httpBackend_) => {
            factory = _salesTrendsFactory_;
            $httpBackend = _$httpBackend_;
        });
        requestArguments = ['GET', 'mocks/salesTrends.json', undefined, (headers) => headers.Namespace === 'salesTrends'];
        requestHandler = $httpBackend.when.apply(null, requestArguments);
        requestHandler.respond(MOCK_SALES_TRENDS);
    });

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('salesTrendsFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });

    describe('get api data', () => {
        it('returns and maps api data', () => {
            $httpBackend.expect.apply(null, requestArguments);
            let getResponse = factory.get().result;
            $httpBackend.flush();
            expect(getResponse.length).toBe(7);
            expect(getResponse[0].salesDate).toBe('Monday');
            expect(getResponse[0].salesValue).toBe(0);
        });
    });
});
