(function () {
    angular
        .module('appInterfaceSalesTrends')
        .service('salesTrendsModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                index: -1,
                id: 0,
                salesDate: undefined,
                salesValue: 0,
                salesValueCompare: 0,
                shouldCompare: false
            });
            return model.set(data);
        };
    }
})();
