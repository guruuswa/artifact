(function () {
    angular
        .module('appInterfaceSalesTrends')
        .factory('salesTrendsFactory', factory);

    function factory(elasticRequestHelper, eventHelper, salesTrendsModel) {
        const ELASTIC_REQUEST = elasticRequestHelper('salesTrends', mapToModel, {
            method: 'GET',
            endpoint: 'mocks/salesTrends.json',
            isArray: true
        });

        return {
            get: (storesArray, dates, filtersArray) => ELASTIC_REQUEST.query(getQuery(storesArray, dates, filtersArray)),
            onUpdate: new eventHelper()
        };

        function mapToModel(data) {
            return data.aggregations.salesTrends.buckets.map((x, i) => {
                const VALUE_BUCKET = x.ranges.buckets.find(x => x.key === 'filter_by');
                const VALUE_COMPARE_BUCKET = x.ranges.buckets.find(x => x.key === 'compare_to');

                let salesTrendsModelObject = salesTrendsModel({
                    index: i,
                    salesDate: x.key_as_string,
                    salesValue: VALUE_BUCKET.doc_count
                });

                if (!!VALUE_COMPARE_BUCKET) {
                    salesTrendsModelObject.set({
                        salesValueCompare: VALUE_COMPARE_BUCKET.doc_count,
                        shouldCompare: true
                    });
                }

                return salesTrendsModelObject;
            });
        }

        function getQuery() {//storesArray, dates, filtersArray) {
            return {

            };
        }
    }
})();
