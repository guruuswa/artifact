(function () {
    angular
        .module('appInterfaceTradingDays')
        .factory('tradingDaysFactory', factory);

    function factory($q, $http, configApiEndpoint, tradingDaysModel) {
        let service = {
            getData: getData,
            mapDataToModel: (data) => {
                let week = mapToModel(data);
                let sortedWeek = sortWeekData(week);
                return tradingDaysModel().set(sortedWeek);
            }
        };

        function getData(merchant) {
            let merchantFilter = {};

            if (merchant && merchant.name !== 'All stores') {
                merchantFilter = {
                    term: {
                        'Merchant name.raw': merchant.name
                    }
                };
            }

            let deferred = $q.defer();

            $http({
                method: 'POST',
                url: configApiEndpoint,
                headers: {'ProcessResponse': 'tradingDays'},
                data: {
                    size: 0,
                    query: {
                        filtered: {
                            query: {
                                query_string: {
                                    query: '*',
                                    analyze_wildcard: true
                                }
                            },
                            filter: {
                                bool: {
                                    must: [
                                        {
                                            range: {
                                                '@timestamp': {
                                                    gte: '01/01/2015',
                                                    lte: '07/01/2017',
                                                    format: 'dd/MM/yyyy||dd/MM/yyyy'
                                                }
                                            }
                                        }
                                    ],
                                    should: merchantFilter
                                }
                            }
                        }
                    },
                    aggs: {
                        days: {
                            date_histogram: {
                                field: '@timestamp',
                                interval: '1d',
                                time_zone: 'Africa/Johannesburg',
                                format: 'E',
                                min_doc_count: 10
                            },
                            aggs: {
                                valueForDay: {
                                    sum: {
                                        field: 'PIRZ_AMT'
                                    }
                                }
                            }
                        }
                    }
                }
            }).then(response => {
                let data = service.mapDataToModel(response.data);
                deferred.resolve(data);
            }).catch(response => {
                deferred.reject(response.data);
            });

            return deferred.promise;
        }

        function mapToModel(data) {
            let week = tradingDaysModel().set({});

            for (let x = 0; x < data.length; x++) {
                switch (data[x].key_as_string) {
                    case 'Mon' :
                    {
                        week.monday.volume = data[x].volume;
                        week.monday.value = data[x].value;
                        break;
                    }
                    case 'Tue' :
                    {
                        week.tuesday.volume = data[x].volume;
                        week.tuesday.value = data[x].value;
                        break;
                    }
                    case 'Wed' :
                    {
                        week.wednesday.volume = data[x].volume;
                        week.wednesday.value = data[x].value;
                        break;
                    }
                    case 'Thu' :
                    {
                        week.thursday.volume = data[x].volume;
                        week.thursday.value = data[x].value;
                        break;
                    }
                    case 'Fri' :
                    {
                        week.friday.volume = data[x].volume;
                        week.friday.value = data[x].value;
                        break;
                    }
                    case 'Sat' :
                    {
                        week.saturday.volume = data[x].volume;
                        week.saturday.value = data[x].value;
                        break;
                    }
                    case 'Sun' :
                    {
                        week.sunday.volume = data[x].volume;
                        week.sunday.value = data[x].value;
                        break;
                    }
                }
            }
            return week;
        }

        function sort(array, sortOn) {
            let j;
            let i;
            var tmp;

            for (j = 1; j < array.length; j++) {
                let value1 = array[j][sortOn];
                tmp = array[j];
                for (i = j - 1; (i >= 0) && (array[i][sortOn] < value1); i--) {
                    array[i+1] = array[i];
                }
                array[i+1] = tmp;
            }
            return array;
        }

        function applyColourClasses(array, week, typeOf) {
            for (let x = 0; x < array.length; x++) {
                if (x === 0) {
                    setColour(array[x], 'blockDark', week, typeOf);
                }
                else if (x > 0 && x <= 3) {
                    setColour(array[x], 'blockMedium', week, typeOf);
                }
                else if (x > 3) {
                    setColour(array[x], 'blockLight', week, typeOf);
                }
            }
        }

        function getArrayForSorting(week) {
            let array = [
                week.monday, week.tuesday, week.wednesday, week.thursday, week.friday, week.saturday, week.sunday
            ];
            return array;
        }

        function sortWeekData(week) {
            let weekValueArray = getArrayForSorting(week);
            let weekVolumeArray = getArrayForSorting(week);
            weekValueArray = sort(weekValueArray, 'value');
            weekVolumeArray = sort(weekVolumeArray, 'volume');
            applyColourClasses(weekValueArray, week, 'valueColour');
            applyColourClasses(weekVolumeArray, week, 'volumeColour');
            return week;
        }

        function setColour(dayObject, colour, week, setOn) {
            week[dayObject.day][setOn] = colour;
        }

        return service;
    }
})();
