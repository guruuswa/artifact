describe('tradingDaysFactory', () => {
    const ELASTIC_REQUEST = '/_search';
    const ELASTIC_REQUEST_TEST = RegExp(ELASTIC_REQUEST, 'i');
    let factory, $httpBackend, requestHandler;

    beforeEach(inject((_tradingDaysFactory_, _$httpBackend_) => {
        factory = _tradingDaysFactory_;
        $httpBackend = _$httpBackend_;
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('tradingDaysFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });

    describe('get trading days value', () => {

        beforeEach(inject(() => {
            requestHandler = $httpBackend.when('POST', ELASTIC_REQUEST_TEST).respond(postResponse());
        }));

        it('returns an error', () => {
            const ERROR_MESSAGE = '400 error';
            let response;
            requestHandler.respond(400, ERROR_MESSAGE);
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            factory.getData({id: null, name: 'All stores'}).catch(message => response = message);
            $httpBackend.flush();
            expect(response).toBe(ERROR_MESSAGE);
        });

        it('returns a promise', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getData({id: null, name: 'All stores'});
            $httpBackend.flush();
            expect(typeof getResponse.then).toBe('function');
            expect(typeof getResponse.catch).toBe('function');
        });

        it('returns data for week values', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getData({id: null, name: 'All stores'});
            $httpBackend.flush();
            getResponse.then(function (result) {
                expect(result.monday.value).toBe(276123.43);
                expect(result.monday.volume).toBe(240);
                expect(result.monday.valueColour).toBe('blockMedium');
                expect(result.monday.volumeColour).toBe('blockMedium');
            });
        });
    });

    function postResponse() {
        return [
            {
                "key_as_string": "Thu",
                "value": 155575.66,
                "volume": 195
            },
            {
                "key_as_string": "Wed",
                "value": 187630.90999999997,
                "volume": 192
            },
            {
                "key_as_string": "Sun",
                "value": 356147.07999999996,
                "volume": 401
            },
            {
                "key_as_string": "Sat",
                "value": 263192.11,
                "volume": 375
            },
            {
                "key_as_string": "Fri",
                "value": 295888.8,
                "volume": 322
            },
            {
                "key_as_string": "Tue",
                "value": 185921.94000000003,
                "volume": 181
            },
            {
                "key_as_string": "Mon",
                "value": 276123.43,
                "volume": 240
            }
        ];
    }
});
