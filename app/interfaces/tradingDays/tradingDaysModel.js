(function () {
    angular
        .module('appInterfaceTradingDays')
        .service('tradingDaysModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                monday: {
                    value: 0,
                    volume: 0,
                    valueColour: 'blockNoData',
                    volumeColour: 'blockNoData',
                    day: 'monday'
                },
                tuesday: {
                    value: 0,
                    volume: 0,
                    valueColour: 'blockNoData',
                    volumeColour: 'blockNoData',
                    day: 'tuesday'
                },
                wednesday: {
                    value: 0,
                    volume: 0,
                    valueColour: 'blockNoData',
                    volumeColour: 'blockNoData',
                    day: 'wednesday'
                },
                thursday: {
                    value: 0,
                    volume: 0,
                    valueColour: 'blockNoData',
                    volumeColour: 'blockNoData',
                    day: 'thursday'
                },
                friday: {
                    value: 0,
                    volume: 0,
                    valueColour: 'blockNoData',
                    volumeColour: 'blockNoData',
                    day: 'friday'
                },
                saturday: {
                    value: 0,
                    volume: 0,
                    valueColour: 'blockNoData',
                    volumeColour: 'blockNoData',
                    day: 'saturday'
                },
                sunday: {
                    value: 0,
                    volume: 0,
                    valueColour: 'blockNoData',
                    volumeColour: 'blockNoData',
                    day: 'sunday'
                }
            });
            return model.set(data);
        };
    }
})();
