describe('merchantSalesFactory', () => {
    let factory, $httpBackend, configApiEndpoint, requestHandler, requestArguments;

    beforeEach(() => {
        inject((_merchantSalesFactory_, _$httpBackend_, _configApiEndpoint_) => {
            factory = _merchantSalesFactory_;
            $httpBackend = _$httpBackend_;
            configApiEndpoint = _configApiEndpoint_;
        });
        requestArguments = ['POST', configApiEndpoint, undefined, (headers) => headers.Namespace === 'merchantSales'];
        requestHandler = $httpBackend.when.apply(null, requestArguments);
        requestHandler.respond(postResponse());
    });

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    describe('merchantSalesFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });

    describe('get api data', () => {
        it('returns api data', () => {
            let getResponse;
            $httpBackend.expect.apply(null, requestArguments);
            getResponse = factory.get(filterInput().storeId, filterInput().date.filter((x, i) => i < 1));
            $httpBackend.flush();
            expect(getResponse.result[0].merchant).toBe('123');
            expect(getResponse.result[0].value).toBe(456789.123);
            expect(getResponse.result[0].valueCompare).toBe(0);
            expect(getResponse.result[0].volume).toBe(456);
            expect(getResponse.result[0].volumeCompare).toBe(0);
        });

        it('returns api data with compare', () => {
            let getResponse;
            requestHandler.respond(postResponseCompareTo());
            getResponse = factory.get(filterInput().storeId, filterInput().date);
            $httpBackend.flush();
            expect(getResponse.result[0].merchant).toBe('123');
            expect(getResponse.result[0].value).toBe(456789.123);
            expect(getResponse.result[0].valueCompare).toBe(123456.789);
            expect(getResponse.result[0].volume).toBe(456);
            expect(getResponse.result[0].volumeCompare).toBe(123);
        });
    });

    describe('get api data by filters', () => {
        it('returns api data for an array of merchants', () => {
            let getResponse;
            requestHandler.respond(postResponseForMultiple());
            getResponse = factory.get(filterInput(true).storeId, filterInput().date);
            $httpBackend.flush();
            expect(getResponse.result.length).toBe(2);
            expect(getResponse.result[1].merchant).toBe('456');
            expect(getResponse.result[1].value).toBe(4444.44);
            expect(getResponse.result[1].volume).toBe(444);
        });
    });


    function filterInput(useArray) {
        let storeId;

        if(useArray){
            storeId = ['Company Name', 'Company Name'];
        } else {
            storeId = 'Company Name';
        }

        return {
            storeId: storeId,
            date: [{
                dateFrom: 'now-5M',
                dateTo: 'now',
                dateDescription: ''
            }, {
                dateFrom: 'now-10M',
                dateTo: 'now-4M',
                dateDescription: ''
            }]
        };
    }

    function postResponse() {
        return {
            took: 1,
            timed_out: false,
            _shards: {
                total: 20,
                successful: 20,
                failed: 0
            },
            hits: {
                total: 1,
                max_score: 0.0,
                hits: []
            },
            aggregations: {
                group_by_merchant_id: {
                    doc_count_error_upper_bound: 0,
                    sum_other_doc_count: 0,
                    buckets: [{
                        key: '123',
                        doc_count: 789,
                        ranges: {
                            buckets: [{
                                key: 'filter_by',
                                from: 1438253389385,
                                from_as_string: '2015-07-30',
                                to: 1451472589385,
                                to_as_string: '2015-12-30',
                                doc_count: 456,
                                value: {
                                    value: 456789.123
                                }
                            }]
                        },
                        merchant_name: {
                            doc_count_error_upper_bound: 0,
                            sum_other_doc_count: 0,
                            buckets: [
                                {
                                    key: 'Company Name',
                                    doc_count: 789
                                }
                            ]
                        }
                    }]
                }
            }
        };
    }

    function postResponseCompareTo() {
        return {
            took: 1,
            timed_out: false,
            _shards: {
                total: 20,
                successful: 20,
                failed: 0
            },
            hits: {
                total: 1,
                max_score: 0.0,
                hits: []
            },
            aggregations: {
                group_by_merchant_id: {
                    doc_count_error_upper_bound: 0,
                    sum_other_doc_count: 0,
                    buckets: [{
                        key: '123',
                        doc_count: 789,
                        ranges: {
                            buckets: [{
                                key: 'compare_to',
                                from: 1425120589385,
                                from_as_string: '2015-02-28',
                                to: 1440931789385,
                                to_as_string: '2015-08-30',
                                doc_count: 123,
                                value: {
                                    value: 123456.789
                                }
                            }, {
                                key: 'filter_by',
                                from: 1438253389385,
                                from_as_string: '2015-07-30',
                                to: 1451472589385,
                                to_as_string: '2015-12-30',
                                doc_count: 456,
                                value: {
                                    value: 456789.123
                                }
                            }]
                        },
                        merchant_name: {
                            doc_count_error_upper_bound: 0,
                            sum_other_doc_count: 0,
                            buckets: [
                                {
                                    key: 'Company Name',
                                    doc_count: 789
                                }
                            ]
                        }
                    }]
                }
            }
        };
    }

    function postResponseForMultiple() {
        return {
            took: 1,
            timed_out: false,
            _shards: {
                total: 20,
                successful: 20,
                failed: 0
            },
            hits: {
                total: 1,
                max_score: 0.0,
                hits: []
            },
            aggregations: {
                group_by_merchant_id: {
                    doc_count_error_upper_bound: 0,
                    sum_other_doc_count: 0,
                    buckets: [{
                        key: '123',
                        doc_count: 789,
                        ranges: {
                            buckets: [{
                                key: 'compare_to',
                                from: 1425120589385,
                                from_as_string: '2015-02-28',
                                to: 1440931789385,
                                to_as_string: '2015-08-30',
                                doc_count: 123,
                                value: {
                                    value: 123456.789
                                }
                            }, {
                                key: 'filter_by',
                                from: 1438253389385,
                                from_as_string: '2015-07-30',
                                to: 1451472589385,
                                to_as_string: '2015-12-30',
                                doc_count: 456,
                                value: {
                                    value: 456789.123
                                }
                            }]
                        },
                        merchant_name: {
                            doc_count_error_upper_bound: 0,
                            sum_other_doc_count: 0,
                            buckets: [
                                {
                                    key: 'Company Name',
                                    doc_count: 789
                                }
                            ]
                        }
                    }, {
                        key: '456',
                        doc_count: 777,
                        ranges: {
                            buckets: [{
                                key: 'compare_to',
                                from: 1425120589385,
                                from_as_string: '2015-02-28',
                                to: 1440931789385,
                                to_as_string: '2015-08-30',
                                doc_count: 111,
                                value: {
                                    value: 1111.11
                                }
                            }, {
                                key: 'filter_by',
                                from: 1438253389385,
                                from_as_string: '2015-07-30',
                                to: 1451472589385,
                                to_as_string: '2015-12-30',
                                doc_count: 444,
                                value: {
                                    value: 4444.44
                                }
                            }]
                        },
                        merchant_name: {
                            doc_count_error_upper_bound: 0,
                            sum_other_doc_count: 0,
                            buckets: [
                                {
                                    key: 'Company Name 2',
                                    doc_count: 777
                                }
                            ]
                        }
                    }]
                }
            }
        };
    }

    function postResponseForFilter() {
        return {
            took: 1,
            timed_out: false,
            _shards: {
                total: 20,
                successful: 20,
                failed: 0
            },
            hits: {
                total: 1,
                max_score: 0.0,
                hits: []
            },
            aggregations: {
                group_by_merchant_id: {
                    doc_count_error_upper_bound: 0,
                    sum_other_doc_count: 0,
                    buckets: [{
                        key: '123',
                        doc_count: 10,
                        ranges: {
                            buckets: [{
                                key: 'filter_by',
                                from: 1438253389385,
                                from_as_string: '2015-07-30',
                                to: 1451472589385,
                                to_as_string: '2015-12-30',
                                doc_count: 8,
                                value: {
                                    value: 20.99
                                }
                            }]
                        },
                        merchant_name: {
                            doc_count_error_upper_bound: 0,
                            sum_other_doc_count: 0,
                            buckets: [
                                {
                                    key: 'Company Name',
                                    doc_count: 10
                                }
                            ]
                        }
                    }]
                }
            }
        };
    }
});
