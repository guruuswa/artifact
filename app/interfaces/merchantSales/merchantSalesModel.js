(function () {
    angular
        .module('appInterfaceMerchantSales')
        .service('merchantSalesModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                index: -1,
                id: 0,
                merchant: '',
                value: 0,
                volume: 0,
                valueCompare: 0,
                volumeCompare: 0,
                shouldCompare: false
            });
            return model.set(data);
        };
    }
})();
