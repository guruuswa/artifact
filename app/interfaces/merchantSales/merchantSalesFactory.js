(function () {
    angular
        .module('appInterfaceMerchantSales')
        .factory('merchantSalesFactory', factory);

    function factory(elasticRequestHelper, eventHelper, merchantSalesModel) {
        const ELASTIC_REQUEST = elasticRequestHelper('merchantSales', mapToModel, {
            isArray: true
        });

        return {
            get: (storesArray, dates, filtersArray) => ELASTIC_REQUEST.query(getQuery(storesArray, dates, filtersArray)),
            onUpdate: new eventHelper()
        };

        function mapToModel(data) {
            return data.aggregations.group_by_merchant_id.buckets.map((x, i) => {
                const VALUE_BUCKET = x.ranges.buckets.find(x => x.key === 'filter_by');
                const VALUE_COMPARE_BUCKET = x.ranges.buckets.find(x => x.key === 'compare_to');

                let merchantSalesModelObj = merchantSalesModel({
                    index: i,
                    merchant: x.key,
                    id: x.merchant_name.buckets[0].key,
                    value: VALUE_BUCKET.value.value,
                    volume: VALUE_BUCKET.doc_count
                });

                if (!!VALUE_COMPARE_BUCKET) {
                    merchantSalesModelObj.set({
                        valueCompare: VALUE_COMPARE_BUCKET.value.value,
                        volumeCompare: VALUE_COMPARE_BUCKET.doc_count,
                        shouldCompare: true
                    });
                }

                return merchantSalesModelObj;
            });
        }

        function getQuery(storesArray, dates, filtersArray) {
            let filters = {
                bool: {
                    must: (filtersArray || []),
                    should: (storesArray || [])
                }
            };

            let dateRange = {
                field: '@timestamp',
                format: 'yyyy-MM-dd',
                ranges: [{
                    key: 'filter_by',
                    from: dates[0].dateFrom,
                    to: dates[0].dateTo
                }]
            };

            if(dates[1] && (dates[1].dateFrom && dates[1].dateTo)) {
                dateRange.ranges.push({key: 'compare_to', from: dates[1].dateFrom, to: dates[1].dateTo});
            }

            return {
                query: {
                    filtered: {
                        filter: filters
                    }
                },
                size: 0,
                aggs: {
                    group_by_merchant_id: {
                        terms: {
                            field: 'Merchant number.raw',
                            size: storesArray.length
                        },
                        aggs: {
                            ranges: {
                                date_range: dateRange,
                                aggs: {
                                    value: {
                                        sum: {
                                            field: 'PIRZ_AMT'
                                        }
                                    }
                                }
                            },
                            merchant_name: {
                                terms: {
                                    field: 'Merchant name.raw'
                                }
                            }
                        }
                    },
                    by_average: {
                        date_range: dateRange,
                        aggs: {
                            '00_average': {
                                avg: {
                                    field: 'PIRZ_AMT'
                                }
                            }
                        }
                    }
                }
            };
        }
    }
})();