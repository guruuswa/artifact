describe('merchantSalesModel', () => {
    let model;

    beforeEach(inject((_merchantSalesModel_) => {
        model = _merchantSalesModel_;
    }));


    describe('merchantSalesModel instance', () => {
        it('can create a new instance', () => {
            expect(typeof model()).toEqual('object');
        });
    });
});