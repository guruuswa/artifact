describe('customerSpendRangeModel', () => {
    let model, modelInstance;

    beforeEach(module('appInterfaceCustomerSpendRange'));

    beforeEach(() => {
        inject(['customerSpendRangeModel', (_customerSpendRangeModel_) => {
            model = _customerSpendRangeModel_;
        }]);
    });

    describe('customerSpendRangeModel instance', () => {
        it('can create a new instance', () => {
            modelInstance = new model();
            expect(typeof modelInstance).toEqual('object');
        });
    });
});