describe('customerSpendRangeFactory', () => {
    let factory, $httpBackend, configApiEndpoint, requestHandler, requestArguments;

    beforeEach(() => {
        inject((_customerSpendRangeFactory_, _$httpBackend_, _configApiEndpoint_) => {
            factory = _customerSpendRangeFactory_;
            $httpBackend = _$httpBackend_;
            configApiEndpoint = _configApiEndpoint_;
        });
        requestArguments = ['POST', configApiEndpoint, undefined, (headers) => headers.Namespace === 'spendRange'];
        requestHandler = $httpBackend.when.apply(null, requestArguments);
        requestHandler.respond(MOCK_SPEND_RANGE);
    });

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    describe('customerSpendRangeFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });

    describe('get api data for value', () => {
        it('returns api data for value', () => {
            let getResponse;
            $httpBackend.expect.apply(null, requestArguments);
            factory.get(filterInput().storeId, filterInput().date).then((response) => getResponse = response);
            $httpBackend.flush();
            expect(getResponse.length).toBe(6);
            expect(getResponse[0].volume).toBe(86740);
            expect(getResponse[0].value).toBe(123741);
            expect(getResponse[0].percentageValue).toBe('6%');
            expect(getResponse[0].averageValue).toBe(3000000);
        });

        it('returns api data when no store selected', () => {
            let getResponse;
            $httpBackend.expect.apply(null, requestArguments);
            factory.get(null, filterInput().date).then((response) => getResponse = response);
            $httpBackend.flush();
            expect(getResponse.length).toBe(6);
        });
    });


    function filterInput() {
        return {
            storeId: 'Company Name',
            date: [{
                dateFrom: 'now-5M',
                dateTo: 'now',
                dateDescription: ''
            }, {
                dateFrom: 'now-10M',
                dateTo: 'now-4M',
                dateDescription: ''
            }]
        };
    }
});
