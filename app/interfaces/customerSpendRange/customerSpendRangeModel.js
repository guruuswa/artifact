(function() {
    angular
        .module('appInterfaceCustomerSpendRange')
        .service('customerSpendRangeModel', service);

    function service(modelHelper) {
        return function(data) {
            var model = new modelHelper({
                rangeFrom: undefined,
                rangeTo: undefined,
                value: 0,
                volume: 0,
                percentageValue: '0%',
                averageValue: 0,
                totalValue: 0
            });
            return model.set(data);
        };
    }
})();