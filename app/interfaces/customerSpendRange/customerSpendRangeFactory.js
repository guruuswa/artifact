(function() {
    angular
        .module('appInterfaceCustomerSpendRange')
        .factory('customerSpendRangeFactory', factory);

    function factory(elasticRequestHelper, customerSpendRangeModel) {
        const RANGES = [{
            title: 'R50 and below',
            from: 0,
            to: 50
        }, {
            title: 'R50 - R100',
            from: 50,
            to: 100
        }, {
            title: 'R100 - R200',
            from: 100,
            to: 200
        }, {
            title: 'R200 - R500',
            from: 200,
            to: 500
        }, {
            title: 'R500 - R1000',
            from: 500,
            to: 1000
        }, {
            title: 'R1000 and more',
            from: 1000
        }];
        const ELASTIC_REQUEST = elasticRequestHelper('spendRange', mapToModel, {
            isArray: true
        });

        return {
            get: (storesArray, date, filtersArray) => ELASTIC_REQUEST.query(getQuery(storesArray, date, filtersArray))
        };

        function mapToModel(data) {
            let buckets = Object.keys(data.aggregations['2'].buckets).map((b) => data.aggregations['2'].buckets[b]);
            let average = data.aggregations['3'].value;
            let total = buckets.reduce((a, b) => a + b['1'].value, 0);
            return buckets.map((x) => {
                return customerSpendRangeModel({
                    rangeFrom: x.from,
                    rangeTo: x.to,
                    value: x['1'].value,
                    volume: x.doc_count,
                    percentageValue: Math.round(x['1'].value / total * 100) + '%',
                    averageValue: average,
                    totalValue: total
                });
            });
        }

        function getQuery (storesArray, date, filtersArray) {
            let filterInfo = {
                bool: {
                    must: (filtersArray || []).concat([{
                        range: {
                            '@timestamp': {
                                // Date ranges are as follows: [[start, end], [start, end]]. Presently, only one date range is supported by the query.
                                gte: date.dateFrom,
                                lte: date.dateTo
                            }
                        }
                    }]),
                    should: (storesArray || [])
                }
            };

            return {
                query: {
                    filtered: {
                        query: {
                            query_string: {
                                query: '*',
                                analyze_wildcard: true
                            }
                        },
                        filter: filterInfo
                    }
                },
                size: 0,
                aggs: {
                    '2': {
                        range: {
                            field: 'PIRZ_AMT',
                            ranges: RANGES,
                            keyed: true
                        },
                        aggs: {
                            '1': {
                                sum: {
                                    field: 'PIRZ_AMT'
                                }
                            }
                        }
                    },
                    '3': {
                        avg: {
                            field: 'PIRZ_AMT'
                        }
                    }
                }
            };
        }
    }
})();