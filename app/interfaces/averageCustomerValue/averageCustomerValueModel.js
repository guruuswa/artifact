(function () {
    angular
        .module('appInterfaceAverageCustomerValue')
        .service('averageCustomerValueModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                name: '',
                value: 0
            });
            return model.set(data);
        };
    }
})();
