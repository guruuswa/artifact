describe('averageCustomerValueModel', () => {
    let model;

    beforeEach(inject((_averageCustomerValueModel_) => {
        model = _averageCustomerValueModel_;
    }));


    describe('averageCustomerValueModel instance', () => {
        it('can create a new instance', () => {
            expect(typeof model()).toEqual('object');
        });
    });
});