describe('averageCustomerValueFactory', () => {
    let factory, $httpBackend, requestArguments, requestHandler;

    beforeEach(() => {
        inject((_averageCustomerValueFactory_, _$httpBackend_) => {
            factory = _averageCustomerValueFactory_;
            $httpBackend = _$httpBackend_;
        });
        requestArguments = ['GET', 'mocks/averageCustomerValue.json', undefined, (headers) => headers.Namespace === 'averageCustomerValue'];
        requestHandler = $httpBackend.when.apply(null, requestArguments);
        requestHandler.respond(MOCK_AVERAGE_CUSTOMER_VALUE);
    });


    describe('averageCustomerValueFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });

    describe('get api data', () => {
        it('returns and maps api data', () => {
            let getResponse;
            $httpBackend.expect.apply(null, requestArguments);
            getResponse = factory.get().result;
            $httpBackend.flush();
            expect(getResponse.length).toBe(2);
            expect(getResponse[0].name).toBe('filter_by');
            expect(getResponse[0].value).toBe(798.685836734694);
        });
    });
});
