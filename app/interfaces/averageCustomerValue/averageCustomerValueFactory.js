(function () {
    angular
        .module('appInterfaceAverageCustomerValue')
        .factory('averageCustomerValueFactory', factory);

    function factory(elasticRequestHelper, eventHelper, averageCustomerValueModel) {
        const ELASTIC_REQUEST = elasticRequestHelper('averageCustomerValue', mapToModel, {
            method: 'GET',
            endpoint: 'mocks/averageCustomerValue.json',
            isArray: true
        });

        return {
            get: () => ELASTIC_REQUEST.query(),
            onUpdate: new eventHelper()
        };

        function mapToModel(data) {
            return data.aggregations.by_average.buckets.map(x => {
                return averageCustomerValueModel({
                    name: x.key,
                    value: x.value
                });
            });
        }
    }
})();
