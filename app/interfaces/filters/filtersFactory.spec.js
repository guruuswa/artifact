describe('filtersFactory', () => {
    const FILTER_REQUEST = 'spotlightFilters.json';
    let factory, $httpBackend, requestHandler;

    beforeEach(() => {
        inject((_filtersFactory_, _$httpBackend_) => {
            factory = _filtersFactory_;
            $httpBackend = _$httpBackend_;
        });
        requestHandler = $httpBackend.when('GET', RegExp(FILTER_REQUEST, 'i'));
        requestHandler.respond(MOCK_SPOTLIGHT_FILTERS);
    });

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    describe('get filters method', () => {
        it('returns an error', () => {
            const ERROR_MESSAGE = '400 error';
            let response;
            requestHandler.respond(400, ERROR_MESSAGE);
            $httpBackend.expect('GET', RegExp(FILTER_REQUEST, 'i'));
            factory.get().catch(message => response = message);
            $httpBackend.flush();
            expect(response).toBe(ERROR_MESSAGE);
        });

        it('returns api data', () => {
            let getResponse;
            $httpBackend.expect('GET', RegExp(FILTER_REQUEST, 'i'));
            getResponse = factory.get();
            $httpBackend.flush();
            expect(typeof getResponse.then).toBe('function');
            expect(getResponse.result.length).toBe(3);
            expect(getResponse.result[0].id).toBe(0);
            getResponse = factory.get();
            expect(typeof getResponse.then).toBe('function');
            expect(getResponse.result.length).toBe(3);
            expect(getResponse.result[0].id).toBe(0);
        });
    });

    describe('active filters', () => {
        it('get active filters', () => {
            expect(typeof factory.getActive()).toBe('object');
        });

        it('set active filters', () => {
            let activeFilters = factory.setActive.apply(null, setActiveItemsArray());
            expect(activeFilters.dateRange.dateDescription).toBe('This week');
            expect(activeFilters.compareDateRange.dateDescription).toBe('Last week');
            expect(activeFilters.age.value).toBe('25,32');
            expect(activeFilters.race.value).toBe('W');
            expect(activeFilters.gender.value).toBe('M');
        });
    });

    describe('get query', () => {
        it('gets query with active set', () => {
            factory.setActive.apply(null, setActiveItemsArray());
            expect(factory.getQueryArray()[0].term['GENDER.raw']).toBe('M');
            expect(factory.getQueryArray()[1].term['Ethnicity_Cd.raw']).toBe('W');
            expect(factory.getQueryArray()[2].range.BIRTHDATE.lte).toBe('now-25y');
            expect(factory.getQueryArray()[2].range.BIRTHDATE.gte).toBe('now-32y');
        });

        it('gets query with active set (single birthdate value)', () => {
            let activeArray = setActiveItemsArray();
            activeArray[2].value = '60';
            factory.setActive.apply(null, activeArray);
            expect(factory.getQueryArray()[2].range.BIRTHDATE.lte).toBe('now-60y');
            expect(factory.getQueryArray()[2].range.BIRTHDATE.gte).toBeUndefined();
        });
    });

    describe('clears cache method', () => {
        it('clears cache', () => {
            expect(factory.clear()).toBeUndefined();
        });
    });


    function setActiveItemsArray() {
        return [
            {dateFrom: '2015-07-08', dateTo: '2015-08-11', dateDescription: 'This week'},
            {dateFrom: '2015-07-01', dateTo: '2015-08-07', dateDescription: 'Last week'},
            {value: '25,32', label: '25 - 32', isActive: true},
            {value: 'W', label: 'White', isActive: true},
            {value: 'M', label: 'Male', isActive: true}
        ];
    }
});
