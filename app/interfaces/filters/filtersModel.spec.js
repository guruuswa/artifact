describe('filtersModel', () => {
    let model, modelInstance;

    beforeEach(module('appInterfaceFilters'));

    beforeEach(() => {
        inject(['filtersModel',
            (_filtersModel_) => {
                model = _filtersModel_;
            }
        ]);
    });

    describe('filtersModel instance', () => {
        it('can create a new instance', () => {
            modelInstance = new model();
            expect(typeof modelInstance).toEqual('object');
        });
    });
});
