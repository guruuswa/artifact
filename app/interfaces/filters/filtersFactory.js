(function() {
    angular
        .module('appInterfaceFilters')
        .factory('filtersFactory', factory);

    function factory(elasticRequestHelper, filtersModel) {
        const ELASTIC_REQUEST = elasticRequestHelper('spendingTrends', mapToModel, {
            method: 'GET',
            endpoint: 'mocks/spotlightFilters.json',
            cache: true,
            isArray: true
        });

        let activeFilters = {
            dateRange: {},
            compareDateRange: {},
            age: {},
            race: {},
            gender: {}
        };

        return {
            get: () => ELASTIC_REQUEST.query(),
            getActive: () => activeFilters,
            setActive: (dateRange, compareDateRange, age, race, gender) => {
                activeFilters = {
                    dateRange: dateRange,
                    compareDateRange: compareDateRange,
                    age: age,
                    race: race,
                    gender: gender
                };
                return activeFilters;
            },
            getQueryArray: getQueryArray,
            clear: () => ELASTIC_REQUEST.clearCache(),
        };

        function mapToModel(response) {
            return response.map(x => filtersModel(x));
        }

        function getQueryArray() {
            let filterArray = [];

            if (activeFilters.gender && activeFilters.gender.value) {
                filterArray.push({
                    term: {
                        'GENDER.raw': activeFilters.gender.value
                    }
                });
            }

            if (activeFilters.race && activeFilters.race.value) {
                filterArray.push({
                    term: {
                        'Ethnicity_Cd.raw': activeFilters.race.value
                    }
                });
            }

            if (activeFilters.age && activeFilters.age.value) {
                const DATES = activeFilters.age.value.split(',');
                let birthDate = {
                    lte: `now-${DATES[0]}y`
                };

                if (DATES[1]) {
                    birthDate.gte = `now-${DATES[1]}y`;
                }

                filterArray.push({
                    range: {
                        BIRTHDATE: birthDate
                    }
                });
            }

            return filterArray;
        }
    }
})();
