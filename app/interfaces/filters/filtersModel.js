(function() {
    angular
        .module('appInterfaceFilters')
        .service('filtersModel', service);

    function service(modelHelper) {
        return function(data) {
            var model = new modelHelper({
                id: '',
                title: '',
                defaultLabel: '',
                activeId: null,
                popupOpen: false,
                name: '',
                options: []
            });

            return model.set(data);
        };
    }
}());