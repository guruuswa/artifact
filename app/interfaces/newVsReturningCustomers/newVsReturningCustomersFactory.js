(function () {
    angular
        .module('appInterfaceNewVsReturningCustomers')
        .factory('newVsReturningCustomersFactory', factory);

    function factory($window, $q, $http, newVsReturningCustomersModel) {
        const REQUEST_URL = 'mocks/newVsReturningCustomers.json';
        const STORAGE_KEY = 'nvrcust';

        let sessionStorage = $window.sessionStorage,
            nvrCustData = JSON.parse(sessionStorage.getItem(STORAGE_KEY));

        return {
            storageKey: STORAGE_KEY,
            get: () => {
                let deferred = $q.defer(),
                    result = [];

                    if(!nvrCustData) {
                        $http.get(REQUEST_URL)
                            .then( response => {
                                nvrCustData = response.data.map(x => newVsReturningCustomersModel(x));
                                sessionStorage.setItem(STORAGE_KEY, JSON.stringify(nvrCustData));
                                deferred.resolve(nvrCustData);
                            })
                            .catch(err => {
                                deferred.reject(err.data);
                            });
                    }
                    else {
                        angular.copy(nvrCustData, result);
                        deferred.resolve(nvrCustData);
                    }

                    deferred.promise.result = result;

                    return deferred.promise;
            },
            getById: id => {
                id = id || 'All stores';
                return nvrCustData.find(x => x.id === id);
            }
        };
    }
})();
