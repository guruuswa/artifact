describe('newVsReturningCustomersModel', () => {
    let model;

    beforeEach(inject((_newVsReturningCustomersModel_) => {
        model = _newVsReturningCustomersModel_;
    }));


    describe('newVsReturningCustomersModel instance', () => {
        it('can create a new instance', () => {
            expect(typeof model()).toEqual('object');
        });
    });
});