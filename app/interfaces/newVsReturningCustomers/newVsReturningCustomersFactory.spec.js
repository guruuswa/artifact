describe('newVsReturningCustomersFactory', () => {
    let factory;

    beforeEach(inject((_newVsReturningCustomersFactory_) => {
        factory = _newVsReturningCustomersFactory_;
    }));


    describe('newVsReturningCustomersFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });
});
