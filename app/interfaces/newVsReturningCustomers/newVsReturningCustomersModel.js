(function () {
    angular
        .module('appInterfaceNewVsReturningCustomers')
        .service('newVsReturningCustomersModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                // newVsReturningCustomersModel model
                id: '',
                newCustPercent: ''
            });
            return model.set(data);
        };
    }
})();
