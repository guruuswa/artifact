(function () {
    angular
        .module('appInterfaceCustomerLocation')
        .service('customerLocationModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                merchantLocations: [],
                customerLocations: []
            });
            return model.set(data);
        };
    }
})();
