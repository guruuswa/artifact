describe('customerLocationFactory', () => {
    const ELASTIC_REQUEST = '/_search';
    const ELASTIC_REQUEST_TEST = RegExp(ELASTIC_REQUEST, 'i');
    let factory, $httpBackend, requestHandler;

    beforeEach(inject((_customerLocationFactory_, _$httpBackend_) => {
        factory = _customerLocationFactory_;
        $httpBackend = _$httpBackend_;
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('customerLocationFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });

    describe('get customer location', () => {

        beforeEach(inject(() => {
            requestHandler = $httpBackend.when('POST', ELASTIC_REQUEST_TEST).respond(postResponse());
        }));

        it('returns an error', () => {
            const ERROR_MESSAGE = '400 error';
            let response;
            requestHandler.respond(400, ERROR_MESSAGE);
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            factory.getData({id: null, name: 'All stores'}).catch(message => response = message);
            $httpBackend.flush();
            expect(response).toBe(ERROR_MESSAGE);
        });

        it('returns a promise', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getData();
            $httpBackend.flush();
            expect(typeof getResponse.then).toBe('function');
            expect(typeof getResponse.catch).toBe('function');
        });

        it('returns a result', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getData();
            $httpBackend.flush();
            expect(Array.isArray(getResponse.result)).toBeTruthy();
        });

        it('returns data for highest value', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getData();
            $httpBackend.flush();
            getResponse.then(function (result) {
                expect(result.customerLocations[0].lon).toBe(28.0833);
            });
        });
    });

    function postResponse() {
        return {
            "took": 1267,
            "timed_out": false,
            "_shards": {
                "total": 6,
                "successful": 6,
                "failed": 0
            },
            "hits": {
                "total": 11736566,
                "max_score": 0,
                "hits": []
            },
            "aggregations": {
                "merchantLocations": {
                    "doc_count_error_upper_bound": 0,
                    "sum_other_doc_count": 0,
                    "buckets": [
                        {
                            "key": 28.0833,
                            "doc_count": 1135069,
                            "merchLat": {
                                "doc_count_error_upper_bound": 0,
                                "sum_other_doc_count": 0,
                                "buckets": [
                                    {
                                        "key": -26.2,
                                        "doc_count": 607186
                                    },
                                    {
                                        "key": -26.05,
                                        "doc_count": 202353
                                    }
                                ]
                            }
                        }
                    ]
                },
                "clientLocations": {
                    "doc_count_error_upper_bound": 0,
                    "sum_other_doc_count": 0,
                    "buckets": [
                        {
                            "key": 28.0833,
                            "doc_count": 1135069,
                            "clientLat": {
                                "doc_count_error_upper_bound": 0,
                                "sum_other_doc_count": 0,
                                "buckets": [
                                    {
                                        "key": -26.2,
                                        "doc_count": 607186
                                    },
                                    {
                                        "key": -26.05,
                                        "doc_count": 202353
                                    }

                                ]
                            }
                        }
                    ]
                }
            }
        };
    }
});
