(function () {
    angular
        .module('appInterfaceCustomerLocation')
        .factory('customerLocationFactory', factory);

    function factory($q, $http, configApiEndpoint, customerLocationModel) {
        return {
            getData: getData
        };

        function getData(merchant) {
            let deferred = $q.defer();
            let result = [];

            $http({
                method: 'POST',
                url: configApiEndpoint,
                data: {
                    size: 0,
                    query: {
                        filtered: {
                            filter: {
                                bool: {
                                    must: [
                                        {
                                            range: {
                                                '@timestamp': {
                                                    gte: '1/12/2014',
                                                    lte: '1/12/2016',
                                                    format: 'dd/MM/yyyy||dd/MM/yyyy'
                                                }
                                            }
                                        }
                                    ],
                                    must_not: [],
                                    should: [getMerchantFilter(merchant)]
                                }
                            }
                        }
                    },
                    aggs: {
                        merchantLocations: {
                            terms: {
                                field: 'Merch_Lon.raw',
                                size: 1000
                            },
                            aggs: {
                                merchLat: {
                                    terms: {
                                        field: 'Merch_Lat.raw',
                                        size: 1000
                                    }
                                }
                            }
                        },
                        clientLocations: {
                            terms: {
                                field: 'Longitude.raw',
                                size: 10000,
                                order: {
                                    _count: 'desc'
                                }
                            },
                            aggs: {
                                clientLat: {
                                    terms: {
                                        field: 'Latitude.raw',
                                        size: 10000,
                                        order: {
                                            _count: 'desc'
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }).then(response => {
                let data = mapToModel(response.data);
                angular.copy(data, result);
                deferred.resolve(data);
            }).catch(response => {
                deferred.reject(response.data);
            });


            deferred.promise.result = result;
            return deferred.promise;
        }

        function mapToModel(data) {
            let merchantLocations = [];
            let merchantResponseList = data.aggregations.merchantLocations.buckets;
            for (let i = 0; i < merchantResponseList.length; i++) {
                let merchant = merchantResponseList[i];
                let merchantLatList = merchant.merchLat.buckets;
                if (merchant.key !== '_null_') {
                    for (let j = 0; j < merchantLatList.length; j++) {
                        let merchantLat = merchantLatList[j];
                        merchantLocations.push({
                            lon: merchant.key,
                            lat: merchantLat.key,
                            weight: merchantLat.doc_count
                        });
                    }
                }
            }

            let customerLocations = [];
            let customerResponseList = data.aggregations.clientLocations.buckets;
            let totalHits = data.hits.total;
            let maxIntensity = Math.round(totalHits/customerResponseList.length);
            for (let i = 0; i < customerResponseList.length; i++) {
                let customer = customerResponseList[i];
                let customerLatList = customer.clientLat.buckets;
                if (customer.key !== '_null_') {
                    for (let j = 0; j < customerLatList.length; j++) {
                        let customerLat = customerLatList[j];
                        customerLocations.push({
                            lon: customer.key,
                            lat: customerLat.key,
                            weight: customerLat.doc_count,
                            maxIntensity: maxIntensity
                        });
                    }
                }
            }

            return customerLocationModel().set({
                merchantLocations: merchantLocations,
                customerLocations: customerLocations
            });
        }

        function getMerchantFilter(merchant) {
            var merchantFilter = {};

            if (merchant && merchant.name !== 'All stores') {
                merchantFilter = {
                    'term': {
                        'Merchant name.raw': merchant.name
                    }
                };
            }

            return merchantFilter;
        }
    }
})();
