(function () {
    angular
        .module('appInterfaceSidebar')
        .factory('sidebarFactory', factory);

    function factory(sidebarModel, eventHelper) {
        let sidebar = sidebarModel();

        let service = {
            get: () => sidebar,
            set: (open) => {
                sidebar.set({
                    isOpen: open
                });
                service.onUpdate.broadcast();
                return service.get();
            },
            open: () => service.set(true),
            close: () => service.set(false),
            toggle: () => service.set(!service.get().isOpen),
            onUpdate: new eventHelper()
        };

        return service;
    }
})();