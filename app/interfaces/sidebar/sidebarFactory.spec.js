describe('sidebarFactory', () => {
    let factory;

    beforeEach(inject((_sidebarFactory_) => {
        factory = _sidebarFactory_;
    }));


    describe('sidebarFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });


    describe('get sidebar', () => {
        it('can get the default sidebar', () => {
            expect(factory.get().isOpen).toBeDefined();
        });
    });


    describe('set sidebar', () => {
        it('can set sidebar', () => {
            expect(factory.set(true).isOpen).toBeTruthy();
            expect(factory.set(false).isOpen).toBeFalsy();
        });
    });


    describe('open sidebar', () => {
        it('can set sidebar to open', () => {
            expect(factory.open().isOpen).toBeTruthy();
        });
    });


    describe('close sidebar', () => {
        it('can set sidebar to closed', () => {
            expect(factory.close().isOpen).toBeFalsy();
        });
    });


    describe('toggle sidebar', () => {
        it('can toggle sidebar state', () => {
            expect(factory.toggle().isOpen).toBeTruthy();
            expect(factory.toggle().isOpen).toBeFalsy();
        });
    });


    describe('onUpdate', () => {
        it('has an onUpdate event helper', () => {
            expect(typeof factory.onUpdate.broadcast).toBe('function');
            expect(typeof factory.onUpdate.subscribe).toBe('function');
        });
    });
});