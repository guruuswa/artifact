(function () {
    angular
        .module('appInterfaceSidebar')
        .service('sidebarModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                isOpen: false
            });
            return model.set(data);
        };
    }
})();
