describe('sidebarModel', () => {
    let model;

    beforeEach(inject((_sidebarModel_) => {
        model = _sidebarModel_;
    }));


    describe('sidebarModel instance', () => {
        it('can create a new instance', () => {
            expect(typeof model()).toEqual('object');
        });
    });
});