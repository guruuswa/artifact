(function () {
    angular
        .module('appInterfaceSingleTransaction')
        .factory('singleTransactionFactory', factory);

    function factory($q, $http, configApiEndpoint, singleTransactionModel) {
        return {
            getHighest: (merchant) => {
                return getDefault(getHighestQuery(merchant));
            },
            getLowest: (merchant) => {
                return getDefault(getLowestQuery(merchant));
            }
        };
        
        function getDefault(data){
            let deferred = $q.defer();
            let result = [];

            $http({
                method: 'POST',
                url: configApiEndpoint,
                data: data
            }).then(response => {
                let data = mapToModel(response.data);
                angular.copy(data, result);
                deferred.resolve(data);
            }).catch(response => {
                deferred.reject(response.data);
            });

            deferred.promise.result = result;

            return deferred.promise;
        }
        
        function mapToModel(data) {
            let infoBucket = data.aggregations['2'].buckets[0]['3'].buckets[0];
            
            return singleTransactionModel().set({
                merchant: infoBucket.key,
                value: infoBucket['1'].value
            });
        }

        function getMerchantFilter(merchant){
            var merchantFilter = {};

            if(merchant && merchant.name !== 'All stores'){
                merchantFilter = {
                    'term': {
                        'Merchant name.raw': merchant.name
                    }
                };
            }

            return merchantFilter;
        }
        
        function getHighestQuery(merchant) {
            var merchantFilter = getMerchantFilter(merchant);
            return {
                'size': 0,
                'query': {
                    'filtered': {
                        'query': {
                            'query_string': {
                                'query': '*',
                                'analyze_wildcard': true
                            }
                        },
                        'filter': {
                            'bool': {
                                'must': [
                                    {
                                        'range': {
                                            '@timestamp': {
                                                'gte': '1/12/2014',
                                                'lte': '1/12/2016',
                                                'format': 'dd/MM/yyyy||dd/MM/yyyy'
                                            }
                                        }
                                    }
                                ],
                                'must_not': [],
                                should: merchantFilter
                            }
                        }
                    }
                },
                'aggs': {
                    '2': {
                        'terms': {
                            'field': 'Merchant number.raw',
                            'size': 1,
                            'order': {
                                '1': 'desc'
                            }
                        },
                        'aggs': {
                            '1': {
                                'max': {
                                    'field': 'PIRZ_AMT'
                                }
                            },
                            '3': {
                                'terms': {
                                    'field': 'Merchant name.raw',
                                    'size': 5,
                                    'order': {
                                        '1': 'desc'
                                    }
                                },
                                'aggs': {
                                    '1': {
                                        'max': {
                                            'field': 'PIRZ_AMT'
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
        }
        
        function getLowestQuery(merchant) {
            var merchantFilter = getMerchantFilter(merchant);
            return {
                'size': 0,
                'query': {
                    'filtered': {
                        'query': {
                            'query_string': {
                                'query': '*',
                                'analyze_wildcard': true
                            }
                        },
                        'filter': {
                            'bool': {
                                'must': [
                                    {
                                        'range': {
                                            '@timestamp': {
                                                'gte': '1/12/2014',
                                                'lte': '1/12/2016',
                                                'format': 'dd/MM/yyyy||dd/MM/yyyy'
                                            }
                                        }
                                    }
                                ],
                                'must_not': [],
                                should: merchantFilter
                            }
                        }
                    }
                },
                'aggs': {
                    '2': {
                        'terms': {
                            'field': 'Merchant number.raw',
                            'size': 1,
                            'order': {
                                '1': 'asc'
                            }
                        },
                        'aggs': {
                            '1': {
                                'min': {
                                    'field': 'PIRZ_AMT'
                                }
                            },
                            '3': {
                                'terms': {
                                    'field': 'Merchant name.raw',
                                    'size': 5,
                                    'order': {
                                        '1': 'desc'
                                    }
                                },
                                'aggs': {
                                    '1': {
                                        'min': {
                                            'field': 'PIRZ_AMT'
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
        }
    }
})();
