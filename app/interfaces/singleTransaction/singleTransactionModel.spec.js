describe('singleTransactionModel', () => {
    let model;

    beforeEach(inject((_singleTransactionModel_) => {
        model = _singleTransactionModel_;
    }));


    describe('singleTransactionModel instance', () => {
        it('can create a new instance', () => {
            expect(typeof model()).toEqual('object');
        });
    });
});