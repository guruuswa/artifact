(function () {
    angular
        .module('appInterfaceSingleTransaction')
        .service('singleTransactionModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                merchant: '',
                value: 0
            });
            return model.set(data);
        };
    }
})();
