describe('singleTransactionFactory', () => {
    const ELASTIC_REQUEST = '/_search';
    const ELASTIC_REQUEST_TEST = RegExp(ELASTIC_REQUEST, 'i');
    let factory, $httpBackend, requestHandler;

    beforeEach(inject((_singleTransactionFactory_, _$httpBackend_) => {
        factory = _singleTransactionFactory_;
        $httpBackend = _$httpBackend_;
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('singleTransactionFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });
    
    describe('get highest single transaction', () => {
        
        beforeEach(inject(() => {
            requestHandler = $httpBackend.when('POST', ELASTIC_REQUEST_TEST).respond(postHighestResponse());
        }));
        
        it('returns an error', () => {
            const ERROR_MESSAGE = '400 error';
            let response;
            requestHandler.respond(400, ERROR_MESSAGE);
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            factory.getHighest({id: null, name: 'All stores'}).catch(message => response = message);
            $httpBackend.flush();
            expect(response).toBe(ERROR_MESSAGE);
        });
        
        it('returns a promise', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getHighest({id: null, name: 'All stores'});
            $httpBackend.flush();
            expect(typeof getResponse.then).toBe('function');
            expect(typeof getResponse.catch).toBe('function');
        });
        
        it('returns a result', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getHighest({id: null, name: 'All stores'});
            $httpBackend.flush();
            expect(Array.isArray(getResponse.result)).toBeTruthy();
        });
        
        it('returns data for highest value', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getHighest({id: null, name: 'All stores'});
            $httpBackend.flush();
            getResponse.then(function(result){
                expect(result.merchant).toBe('VKB LANDBOU VILLIERS');
                expect(result.value).toBe(3000000);
            });
        });
    });
    
    describe('get lowest single transaction', () => {
        
        beforeEach(inject(() => {
            requestHandler = $httpBackend.when('POST', ELASTIC_REQUEST_TEST).respond(postLowestResponse());
        }));
        
        it('returns an error', () => {
            const ERROR_MESSAGE = '400 error';
            let response;
            requestHandler.respond(400, ERROR_MESSAGE);
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            factory.getLowest({id: null, name: 'All stores'}).catch(message => response = message);
            $httpBackend.flush();
            expect(response).toBe(ERROR_MESSAGE);
        });
        
        it('returns a promise', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getLowest({id: null, name: 'All stores'});
            $httpBackend.flush();
            expect(typeof getResponse.then).toBe('function');
            expect(typeof getResponse.catch).toBe('function');
        });
        
        it('returns a result', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getLowest({id: null, name: 'All stores'});
            $httpBackend.flush();
            expect(Array.isArray(getResponse.result)).toBeTruthy();
        });
        
        it('returns data for lowest value', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getLowest({id: null, name: 'All stores'});
            $httpBackend.flush();
            getResponse.then(function(result){
                expect(result.merchant).toBe('CABLE CITY');
                expect(result.value).toBe(-237006.01);
            });
        });
    });
    
    function postHighestResponse() {
        return{
            'took': 2074,
            'timed_out': false,
            '_shards': {
                'total': 6,
                'successful': 6,
                'failed': 0
            },
            'hits': {
                'total': 12487063,
                'max_score': 0,
                'hits': []
            },
            'aggregations': {
                '2': {
                    'doc_count_error_upper_bound': -1,
                    'sum_other_doc_count': 2129297,
                    'buckets': [
                        {
                            '1': {
                                'value': 3000000
                            },
                            '3': {
                                'doc_count_error_upper_bound': 0,
                                'sum_other_doc_count': 0,
                                'buckets': [
                                    {
                                        '1': {
                                            'value': 3000000
                                        },
                                        'key': 'VKB LANDBOU VILLIERS',
                                        'doc_count': 101
                                    }
                                ]
                            },
                            'key': '3072363',
                            'doc_count': 101
                        }
                    ]
                }
            }
        };
    }
    
    function postLowestResponse() {
        return {
            "took": 5276,
            "timed_out": false,
            "_shards": {
                "total": 6,
                "successful": 6,
                "failed": 0
            },
            "hits": {
                "total": 12487063,
                "max_score": 0,
                "hits": []
            },
            "aggregations": {
                "2": {
                    "doc_count_error_upper_bound": -1,
                    "sum_other_doc_count": 2129378,
                    "buckets": [
                        {
                            "1": {
                                "value": -237006.01
                            },
                            "3": {
                                "doc_count_error_upper_bound": 0,
                                "sum_other_doc_count": 0,
                                "buckets": [
                                    {
                                        "1": {
                                            "value": -237006.01
                                        },
                                        "key": "CABLE CITY",
                                        "doc_count": 20
                                    }
                                ]
                            },
                            "key": "3236579",
                            "doc_count": 20
                        }
                    ]
                }
            }
        };
    }
});
