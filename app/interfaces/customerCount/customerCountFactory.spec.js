describe('customerCountFactory', () => {
    let factory, $httpBackend, requestArguments, requestHandler;

    beforeEach(() => {
        inject((_customerCountFactory_, _$httpBackend_) => {
            factory = _customerCountFactory_;
            $httpBackend = _$httpBackend_;
        });
        requestArguments = ['GET', 'mocks/customerCount.json', undefined, (headers) => headers.Namespace === 'customerCount'];
        requestHandler = $httpBackend.when.apply(null, requestArguments);
        requestHandler.respond(MOCK_CUSTOMER_COUNT);
    });

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    describe('customerCountFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });

    describe('get api data', () => {
        it('returns and maps api data', () => {
            let getResponse;
            $httpBackend.expect.apply(null, requestArguments);
            getResponse = factory.get().result;
            $httpBackend.flush();
            expect(getResponse.length).toBe(2);
            expect(getResponse[1].id).toBe(1);
            expect(getResponse[1].name).toBe('BELLEZZA');
            expect(getResponse[1].value).toBe(2741);
        });
    });
});
