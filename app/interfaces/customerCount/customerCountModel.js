(function () {
    angular
        .module('appInterfaceCustomerCount')
        .service('customerCountModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                id: '',
                name: '',
                value: 0
            });
            return model.set(data);
        };
    }
})();
