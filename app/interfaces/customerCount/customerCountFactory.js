(function () {
    angular
        .module('appInterfaceCustomerCount')
        .factory('customerCountFactory', factory);

    function factory(elasticRequestHelper, eventHelper, customerCountModel) {
        const ELASTIC_REQUEST = elasticRequestHelper('customerCount', mapToModel, {
            method: 'GET',
            endpoint: 'mocks/customerCount.json',
            isArray: true
        });

        return {
            get: () => ELASTIC_REQUEST.query(),
            onUpdate: new eventHelper()
        };

        function mapToModel(data) {
            return data.aggregations.customers.buckets.map((x, i) => {
                return customerCountModel({
                    id: i,
                    name: x.key,
                    value: x.doc_count
                });
            });
        }
    }
})();
