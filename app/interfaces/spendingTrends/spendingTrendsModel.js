(function () {
    angular
        .module('appInterfaceSpendingTrends')
        .service('spendingTrendsModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                index: -1,
                id: 0,
                averageBasketSize: 0,
                count: 0,
                countCompare: 0,
                shouldCompare: false
            });
            return model.set(data);
        };
    }
})();
