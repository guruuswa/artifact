(function () {
    angular
        .module('appInterfaceSpendingTrends')
        .factory('spendingTrendsFactory', factory);

    function factory(elasticRequestHelper, eventHelper, spendingTrendsModel) {
        const ELASTIC_REQUEST = elasticRequestHelper('spendingTrends', mapToModel, {
            method: 'GET',
            endpoint: 'mocks/spendingTrends.json',
            isArray: true
        });

        return {
            get: () => ELASTIC_REQUEST.query(),
            onUpdate: new eventHelper()
        };

        function mapToModel(data) {
            return data.aggregations.baskets.buckets.map((x, i) => {
                return spendingTrendsModel({
                    index: i,
                    averageBasketSize: x.key,
                    count: x.doc_count
                });
            });
        }
    }
})();
