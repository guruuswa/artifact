describe('spendingTrendsFactory', () => {
    let factory;

    beforeEach(inject((_spendingTrendsFactory_) => {
        factory = _spendingTrendsFactory_;
    }));


    describe('spendingTrendsFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });
});
