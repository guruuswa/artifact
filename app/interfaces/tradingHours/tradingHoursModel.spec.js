describe('tradingHoursModel', () => {
    let model;

    beforeEach(inject((_tradingHoursModel_) => {
        model = _tradingHoursModel_;
    }));

    describe('tradingHoursModel instance', () => {
        it('can create a new instance', () => {
            expect(typeof model()).toEqual('object');
        });
    });
});