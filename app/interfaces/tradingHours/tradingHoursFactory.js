(function () {
    angular
        .module('appInterfaceTradingHours')
        .factory('tradingHoursFactory', factory);

    function factory($q, $http, configApiEndpoint, tradingHoursModel) {

        return {
            getValue: (merchant) => {
                return getDefault(getQuery(merchant), getValueLabel);
            },
            getVolume: (merchant) => {
                return getDefault(getQuery(merchant), getVolumeLabel);
            }
        };

        function getDefault(data, modelFunction){
            let deferred = $q.defer();
            let result = [];

            $http({
                 method: 'POST',
                 url: configApiEndpoint,
                 data: data,
                 headers: {
                    'ProcessResponse': 'tradingHours'
                 }
            }).then(response => {
                 let data = mapToModel(response.data, modelFunction);
                 angular.copy(data, result);
                 deferred.resolve(data);
            }).catch(response => {
                deferred.reject(response.data);
            });

            deferred.promise.result = result;

            return deferred.promise;
        }

        function getValueLabel(data){
            return data.value;
        }

        function getVolumeLabel(data){
            return data.volume;
        }

        function mapToModel(data, modelFunction){
            let array = [];
            let labelArray = [];
            let colorArray = [];
            let finalArray = [];

            //sort according to highest value
            for (let i = 0; i < data.length; i++) {
                labelArray.push(modelFunction(data[i]));
            }

            labelArray.sort(function(a, b){
                return b - a;
            });

            //calculate color ranges
            let colorCount = Math.round((labelArray.length / 3) - 1);
            let value = labelArray.length - 1;
            let lightRange = [value, value - colorCount];
            value = (value - colorCount) - 1;
            let mediumRange = [value, value - colorCount];
            value = (value - colorCount) - 1;
            let darkRange = [value, 0];

            //set colors
            for (let i = 0; i < labelArray.length; i++) {
                if(i <= darkRange[0] && i >= darkRange[1]){
                    colorArray.push({label: labelArray[i], color: '#3366cc'});
                }

                if(i <= mediumRange[0] && i >= mediumRange[1]){
                    colorArray.push({label: labelArray[i], color: '#5c85d6'});
                }

                if(i <= lightRange[0] && i >= lightRange[1]){
                    colorArray.push({label: labelArray[i], color: '#adc2eb'});
                }
            }

            //push data values
            for (let i = 0; i < data.length; i++) {
                let range = setRange(parseInt(data[i].key_as_string));

                array.push({value: 1, label: modelFunction(data[i]), range: range, color: getColor(colorArray, modelFunction(data[i])), arcText: getArcText(range[0])});
            }

            //fill in rest of the hours
            for (let i = 0; i < 24; i++) {
                let arrayObj = {};
                let isInArray = false;
                let range = setRange(i);

                for (let j = 0; j < array.length; j++) {
                    if(array[j].range[0] === range[0]){
                        arrayObj = array[j];
                        isInArray = true;
                        break;
                    }
                }

                if(!isInArray){
                    finalArray.push({value: 1, label: 0, range: range, color: '#cccccc', arcText: getArcText(range[0])});
                }
                else{
                    finalArray.push(arrayObj);
                }
            }

            return tradingHoursModel().set({
                objArray: finalArray
            });
        }

        function getColor(array, label){
            let color = '';

            for (let i = 0; i < array.length; i++) {
                if(array[i].label === label) {
                    return array[i].color;
                }
            }

            return color;
        }

        function getArcText(rangeValue){
            let arcText = '';

            switch(rangeValue){
                case '08:00':
                case '13:00':
                case '17:00':   arcText = rangeValue;
                                break;
            }

            return arcText;
        }

        function setRange(value){
            let range = [];
            let range_0 = '';
            let range_1 = '';

            if(value < 10){
                range_0 = '0' + value + ':00';
                if((value + 1) < 10){
                    range_1 = '0' + (value + 1) + ':00';
                }
                else{
                    range_1 = (value + 1) + ':00';
                }
            }
            else{
                range_0 = value + ':00';
                range_1 = (value + 1) + ':00';
            }
            range.push(range_0);
            range.push(range_1);

            return range;
        }

        function getMerchantFilter(merchant){
            var merchantFilter = {};

            if(merchant && merchant.name !== 'All stores'){
                merchantFilter = {
                    'term': {
                        'Merchant name.raw': merchant.name
                    }
                };
            }

            return merchantFilter;
        }

        function getQuery(merchant){
            var merchantFilter = getMerchantFilter(merchant);
            return {
                'size': 0,
                'query': {
                    'filtered': {
                        'query': {
                            'query_string': {
                                'query': '*',
                                'analyze_wildcard': true
                            }
                        },
                        'filter': {
                            'bool': {
                                'must': [],
                                'must_not': [],
                                should: merchantFilter
                            }
                        }
                    }
                },
                'aggs': {
                    'hours': {
                        'date_histogram': {
                            'field': '@timestamp',
                            'interval': '1h',
                            'time_zone': 'Africa/Johannesburg',
                            'format': 'HH',
                            'min_doc_count': 10
                        },
                        'aggs': {
                            'valueForHour': {
                                'sum': {
                                    'field': 'PIRZ_AMT'
                                }
                            }
                        }
                    }
                }
            };
        }
    }
})();
