describe('tradingHoursFactory', () => {
    const ELASTIC_REQUEST = '/_search';
    const ELASTIC_REQUEST_TEST = RegExp(ELASTIC_REQUEST, 'i');
    let factory, $httpBackend, requestHandler;

    beforeEach(inject((_tradingHoursFactory_, _$httpBackend_) => {
        factory = _tradingHoursFactory_;
        $httpBackend = _$httpBackend_;
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('tradingHoursFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });

    describe('get trading hours', () => {

        beforeEach(inject(() => {
            requestHandler = $httpBackend.when('POST', ELASTIC_REQUEST_TEST).respond(postResponse());
        }));

        it('returns an error', () => {
            const ERROR_MESSAGE = '400 error';
            let response;
            requestHandler.respond(400, ERROR_MESSAGE);
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            factory.getValue({id: null, name: 'All stores'}).catch(message => response = message);
            $httpBackend.flush();
            expect(response).toBe(ERROR_MESSAGE);
        });

        it('returns a promise', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getValue({id: null, name: 'All stores'});
            $httpBackend.flush();
            expect(typeof getResponse.then).toBe('function');
            expect(typeof getResponse.catch).toBe('function');
        });

        it('returns a result', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getValue({id: null, name: 'All stores'});
            $httpBackend.flush();
            expect(Array.isArray(getResponse.result)).toBeTruthy();
        });

        it('returns value data', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getValue({id: null, name: 'All stores'});
            $httpBackend.flush();
            getResponse.then(function(result){
                expect(result.objArray).toEqual(
                    [
                        { value: 1, label: 400834434.0799935, range: [ '00:00', '01:00' ], color: '#3366cc', arcText: '' },
                        { value: 1, label: 0, range: [ '01:00', '02:00' ], color: '#cccccc', arcText: '' },
                        { value: 1, label: 570324.58, range: [ '02:00', '03:00' ], color: '#3366cc', arcText: '' },
                        { value: 1, label: 1986125.29, range: [ '03:00', '04:00' ], color: '#3366cc', arcText: '' },
                        { value: 1, label: 0, range: [ '04:00', '05:00' ], color: '#cccccc', arcText: '' },
                        { value: 1, label: 0, range: [ '05:00', '06:00' ], color: '#cccccc', arcText: '' },
                        { value: 1, label: 0, range:[ '06:00', '07:00' ], color: '#cccccc', arcText: '' },
                        { value: 1, label: 83178.79999999999, range: [ '07:00', '08:00' ], color: '#adc2eb', arcText: ''},
                        { value: 1, label: 0, range: [ '08:00', '09:00' ], color: '#cccccc',arcText: '08:00' },
                        { value: 1, label: 0, range: [ '09:00', '10:00' ], color: '#cccccc', arcText: '' },
                        { value: 1, label: 241008.30999999997, range: [ '10:00', '11:00' ], color: '#5c85d6', arcText: '' },
                        { value: 1, label: 351924.13, range: [ '11:00', '12:00' ], color: '#5c85d6', arcText: '' },
                        { value: 1, label: 458291.86000000004, range: [ '12:00', '13:00' ], color: '#3366cc', arcText: '' },
                        { value: 1, label: 416446.46, range: [ '13:00', '14:00' ], color: '#3366cc', arcText: '13:00' },
                        { value: 1, label: 433477.99, range: [ '14:00', '15:00' ], color: '#3366cc', arcText: '' },
                        { value: 1, label: 371507.82999999996, range: [ '15:00', '16:00' ], color: '#5c85d6', arcText: '' },
                        { value: 1, label: 356385.82, range: [ '16:00','17:00' ], color: '#5c85d6', arcText: '' },
                        { value: 1, label: 302882.8, range: [ '17:00', '18:00' ], color: '#5c85d6', arcText: '17:00' },
                        { value: 1, label: 225582.22, range: [ '18:00', '19:00' ], color: '#5c85d6', arcText: '' },
                        { value: 1, label: 46417.46, range: [ '19:00', '20:00' ], color: '#adc2eb', arcText: '' },
                        { value: 1, label: 11156.82, range: [ '20:00', '21:00' ], color: '#adc2eb', arcText: '' },
                        { value: 1, label: 0, range: [ '21:00', '22:00' ], color: '#cccccc', arcText: '' },
                        { value: 1, label: 0, range: [ '22:00', '23:00' ], color: '#cccccc', arcText: '' },
                        {value: 1, label: 23656.2, range: [ '23:00', '24:00' ], color: '#adc2eb', arcText: '' }
                    ]
                );
            });
        });

        it('returns value data', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getValue({id: null, name: 'All stores'});
            $httpBackend.flush();
            getResponse.then(function(result){
                expect(result.objArray.length).toBe(24);
            });
        });

        it('returns volume data', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getVolume({id: null, name: 'All stores'});
            $httpBackend.flush();
            getResponse.then(function(result){
                expect(result.objArray).toEqual(
                    [
                        { value: 1, label: 750548, range: [ '00:00', '01:00' ], color: '#3366cc', arcText: '' },
                        { value: 1, label: 0, range: [ '01:00', '02:00' ], color: '#cccccc', arcText: '' },
                        { value: 1, label: 1472,range: [ '02:00', '03:00' ], color: '#3366cc', arcText: '' },
                        { value: 1, label: 4807, range: [ '03:00', '04:00' ], color: '#3366cc', arcText: '' },
                        { value: 1, label: 0, range: [ '04:00', '05:00' ], color: '#cccccc', arcText: '' },
                        { value: 1, label: 0, range: [ '05:00', '06:00' ], color: '#cccccc', arcText: '' },
                        { value: 1, label: 0, range: [ '06:00', '07:00' ],color: '#cccccc', arcText: '' },
                        { value: 1, label: 186, range: [ '07:00', '08:00' ], color: '#adc2eb', arcText: '' },
                        { value: 1, label: 0, range: [ '08:00', '09:00' ], color: '#cccccc', arcText: '08:00' },
                        { value: 1, label: 0, range: [ '09:00', '10:00' ], color: '#cccccc', arcText: '' },
                        { value: 1, label: 304, range: [ '10:00', '11:00' ], color: '#adc2eb', arcText: '' },
                        { value: 1, label: 435, range: [ '11:00', '12:00' ], color: '#5c85d6', arcText: '' },
                        { value: 1, label: 661, range: [ '12:00', '13:00' ], color: '#3366cc', arcText: '' },
                        { value: 1, label: 657, range: [ '13:00', '14:00' ], color: '#3366cc', arcText: '13:00' },
                        { value: 1, label: 593, range: [ '14:00', '15:00' ], color: '#3366cc', arcText: '' },
                        { value: 1, label: 520, range: [ '15:00', '16:00' ], color: '#5c85d6', arcText: '' },
                        { value: 1, label: 503, range: [ '16:00', '17:00' ], color: '#5c85d6', arcText: '' },
                        { value: 1, label: 408, range: [ '17:00', '18:00' ],color: '#5c85d6', arcText: '17:00' },
                        { value: 1, label: 333, range: [ '18:00', '19:00' ], color: '#adc2eb', arcText: '' },
                        { value: 1, label: 41, range: [ '19:00', '20:00' ], color: '#adc2eb', arcText: '' },
                        { value: 1, label: 22, range: [ '20:00', '21:00' ], color: '#adc2eb', arcText: '' },
                        { value: 1, label: 0, range: [ '21:00', '22:00' ], color: '#cccccc', arcText: '' },
                        { value: 1, label: 0, range: [ '22:00', '23:00' ], color: '#cccccc', arcText: '' },
                        { value: 1, label: 42, range: [ '23:00', '24:00' ], color: '#adc2eb', arcText: '' }
                    ]
                );
            });
        });

        it('returns volume data', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getVolume({id: null, name: 'All stores'});
            $httpBackend.flush();
            getResponse.then(function(result){
                expect(result.objArray.length).toBe(24);
            });
        });
    });

    function postResponse(){
        return [{
            "key_as_string": "00",
            "value": 400834434.0799935,
            "volume": 750548
        },
        {
            "key_as_string": "02",
            "value": 570324.58,
            "volume": 1472
        },
        {
            "key_as_string": "03",
            "value": 1986125.29,
            "volume": 4807
        },
        {
            "key_as_string": "07",
            "value": 83178.79999999999,
            "volume": 186
        },
        {
            "key_as_string": "08",
            "value": 187720.39999999997,
            "volume": 463
        },
        {
            "key_as_string": "09",
            "value": 163810.13999999998,
            "volume": 338
        },
        {
            "key_as_string": "10",
            "value": 241008.30999999997,
            "volume": 304
        },
        {
            "key_as_string": "11",
            "value": 351924.13,
            "volume": 435
        },
        {
            "key_as_string": "12",
            "value": 458291.86000000004,
            "volume": 661
        },
        {
            "key_as_string": "13",
            "value": 416446.46,
            "volume": 657
        },
        {
            "key_as_string": "14",
            "value": 433477.99,
            "volume": 593
        },
        {
            "key_as_string": "15",
            "value": 371507.82999999996,
            "volume": 520
        },
        {
            "key_as_string": "16",
            "value": 356385.82,
            "volume": 503
        },
        {
            "key_as_string": "17",
            "value": 302882.8,
            "volume": 408
        },
        {
            "key_as_string": "18",
            "value": 225582.22,
            "volume": 333
        },
        {
            "key_as_string": "19",
            "value": 46417.46,
            "volume": 41
        },
        {
            "key_as_string": "20",
            "value": 11156.82,
            "volume": 22
        },
        {
            "key_as_string": "23",
            "value": 23656.2,
            "volume": 42
        }];
    }
});
