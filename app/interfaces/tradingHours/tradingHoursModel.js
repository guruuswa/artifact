(function () {
    angular
        .module('appInterfaceTradingHours')
        .service('tradingHoursModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                objArray: []
            });
            return model.set(data);
        };
    }
})();
