(function () {
    angular
        .module('appInterfaceClientDemographic')
        .factory('clientDemographicFactory', factory);

    function factory($q, $http, configApiEndpoint, clientDemographicModel) {
        return {
            getRace: (merchant) => {
                return getDefault(getRaceQuery(merchant), raceSpecific);
            },
            getGender: (merchant) => {
                return getDefault(getGenderQuery(merchant), genderSpecific);
            }
        };

        function getDefault(data, modelFunction){
            let deferred = $q.defer();
            let result = [];

            $http({
                method: 'POST',
                url: configApiEndpoint,
                data: data
            }).then(response => {
                let data = mapToModel(response.data, modelFunction);
                angular.copy(data, result);
                deferred.resolve(data);
            }).catch(response => {
                deferred.reject(response.data);
            });

            deferred.promise.result = result;

            return deferred.promise;
        }
        
        function mapToModel(data, modelFunction) {
            let preInfoBucket = data.aggregations['2'].buckets;
            let valueArray = [];
            let sumOfValues = 0;

            //filter out null values
            let infoBucket = [];
            for(let i = 0; i < preInfoBucket.length; i++){
                if(preInfoBucket[i].key !== '_null_'){
                    infoBucket.push(preInfoBucket[i]);
                }
            }

            //get sum of values
            for (let i = 0; i < infoBucket.length; i++) {
                sumOfValues += infoBucket[i].doc_count;
            }

            //work out percentages of values
            for (let i = 0; i < infoBucket.length; i++) {
                valueArray.push((infoBucket[i].doc_count/sumOfValues)*100);
            }

            valueArray = calculatePercentages(valueArray);

            let array = modelFunction(valueArray, infoBucket);

            return clientDemographicModel().set({
                objArray: array
            });
        }

        function genderSpecific(valueArray, infoBucket){
            let array = [];

            for (let i = 0; i < infoBucket.length; i++) {
                array.push({label: infoBucket[i].key, value: valueArray[i]});
            }

            if(array.length < 2){
                for(let i = 0; i < array.length; i++){
                    if(array[i].label === 'M'){
                        array.push({label: 'F', value: (100-array[i].value)});
                    }
                    else if(array[i].label === 'F'){
                        array.push({label: 'M', value: (100-array[i].value)});
                    }

                    if(array.length === 2){
                        break;
                    }
                }
            }

            return array;
        }

        function raceSpecific(valueArray, infoBucket){
            let array = [];
            let rangeOne = 0;
            let rangeTwo = valueArray[0];

            for (let i = 0; i < infoBucket.length; i++) {
                array.push({label: infoBucket[i].key, value: valueArray[i], range: [rangeOne, rangeTwo], color: getColor(infoBucket[i].key)});

                rangeOne = rangeTwo;
                rangeTwo = rangeTwo + valueArray[i+1];
            }

            return array;
        }

        function getColor(label){
            let color = '';
            switch(label){
                case 'WHITE':           color = '#3366cc';
                                        break;
                case 'COLOURED':        color = '#00aadd';
                                        break;
                case 'AFRICAN':         color = '#0033aa';
                                        break;
                case 'UNKNOWN':         color = '#ffb822';
                                        break;
                case 'ASIAN':           color = '#00aa77';
                                        break;
                case 'NOT APPLICABLE':  color = '#a90a50';
                                        break;
                default:                color = '#3366cc';
            }
            return color;
        }

        function calculatePercentages(valueArr){
            let splitArray = [];
            let decimalArray = [];
            let finalArray = [];
            let roundedDownSum = 0;

            //setup
            for (let i = 0; i < valueArr.length; i++) {
                let splitValue = valueArr[i].toString().split('.');
                let integer = Number(splitValue[0]);
                let decimal = Number(splitValue[1]);

                roundedDownSum += integer;
                splitArray.push({integer: integer, decimal: decimal});
                decimalArray.push(decimal);
            }

            //sort array according to highest decimal value
            decimalArray.sort(function(a, b){
                return b - a;
            });

            //add additional left over values
            let leftOver = 100 - roundedDownSum;
            for (let i = 0; i < leftOver; i++) {
                for (let j = 0; j < splitArray.length; j++) {
                    if(splitArray[j].decimal === decimalArray[i]){
                        splitArray[j].integer++;
                    }
                }
            }

            //return final array
            for (let i = 0; i < splitArray.length; i++) {
                finalArray.push(splitArray[i].integer);
            }

            return finalArray;
        }

        function getMerchantFilter(merchant){
            var merchantFilter = {};

            if(merchant && merchant.name !== 'All stores'){
                merchantFilter = {
                    'term': {
                        'Merchant name.raw': merchant.name
                    }
                };
            }

            return merchantFilter;
        }
        
        function getRaceQuery(merchant) {
            var merchantFilter = getMerchantFilter(merchant);
            return {
                'size': 0,
                'query': {
                    'filtered': {
                        'query': {
                            'query_string': {
                                'query': '*',
                                'analyze_wildcard': true
                            }
                        },
                        'filter': {
                            'bool': {
                                'must': [
                                    {
                                        'range': {
                                            '@timestamp': {
                                                'gte': '1/12/2014',
                                                'lte': '1/12/2016',
                                                'format': 'dd/MM/yyyy||dd/MM/yyyy'
                                            }
                                        }
                                    }
                                ],
                                'must_not': [],
                                should: merchantFilter
                            }
                        }
                    }
                },
                'aggs': {
                    '2': {
                        'terms': {
                            'field': 'Ethnicity_Desc.raw',
                            'size': 6,
                            'order': {
                                '_count': 'desc'
                            }
                        }
                    }
                }
            };
        }

        function getGenderQuery(merchant) {
            var merchantFilter = getMerchantFilter(merchant);
            return {
                'size': 0,
                'query': {
                    'filtered': {
                        'query': {
                            'query_string': {
                                'query': '*',
                                'analyze_wildcard': true
                            }
                        },
                        'filter': {
                            'bool': {
                                'must': [
                                    {
                                        'range': {
                                            '@timestamp': {
                                                'gte': '1/12/2014',
                                                'lte': '1/12/2016',
                                                'format': 'dd/MM/yyyy||dd/MM/yyyy'
                                            }
                                        }
                                    }
                                ],
                                'must_not': [],
                                should: merchantFilter
                            }
                        }
                    }
                },
                'aggs': {
                    '2': {
                        'terms': {
                            'field': 'GENDER.raw',
                            'size': 6,
                            'order': {
                                '_count': 'desc'
                            }
                        }
                    }
                }
            };
        }
    }
})();
