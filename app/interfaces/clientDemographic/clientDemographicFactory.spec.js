describe('clientDemographicFactory', () => {
    const ELASTIC_REQUEST = '/_search';
    const ELASTIC_REQUEST_TEST = RegExp(ELASTIC_REQUEST, 'i');
    let factory, $httpBackend, requestHandler;

    beforeEach(inject((_clientDemographicFactory_, _$httpBackend_) => {
        factory = _clientDemographicFactory_;
        $httpBackend = _$httpBackend_;
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('clientDemographicFactory object', () => {
        it('returns an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });
    
    describe('get client demographic race', () => {

        beforeEach(inject(() => {
            requestHandler = $httpBackend.when('POST', ELASTIC_REQUEST_TEST).respond(raceResponse());
        }));

        it('returns an error', () => {
            const ERROR_MESSAGE = '400 error';
            let response;
            requestHandler.respond(400, ERROR_MESSAGE);
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            factory.getRace({id: null, name: 'All stores'}).catch(message => response = message);
            $httpBackend.flush();
            expect(response).toBe(ERROR_MESSAGE);
        });
        
        it('returns a promise', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getRace({id: null, name: 'All stores'});
            $httpBackend.flush();
            expect(typeof getResponse.then).toBe('function');
            expect(typeof getResponse.catch).toBe('function');
        });
        
        it('returns a result', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getRace({id: null, name: 'All stores'});
            $httpBackend.flush();
            expect(Array.isArray(getResponse.result)).toBeTruthy();
        });
        
        it('returns data for race', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getRace({id: null, name: 'All stores'});
            $httpBackend.flush();
            getResponse.then(function(result){
                expect(result.objArray).toEqual(
                    [
                        {label: 'WHITE',value: 49,range: [0,49],color: '#3366cc'},
                        {label: 'COLOURED',value: 30,range: [49,79],color: '#00aadd'},
                        {label: 'AFRICAN',value: 11,range: [79,90],color: '#0033aa'},
                        {label: 'UNKNOWN',value: 7,range: [90,97],color: '#ffb822'},
                        {label: 'ASIAN',value: 2,range: [97,99],color: '#00aa77'},
                        {label: 'NOT APPLICABLE',value: 1,range: [99,100],color: '#a90a50'}
                    ]
                );
            });
        });

        it(', values must add up to 100 (percent)', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getRace({id: null, name: 'All stores'});
            $httpBackend.flush();
            getResponse.then(function(result){
                let sum = 0;
                for (let i = 0; i < result.objArray.length; i++) {
                    sum += result.objArray[i].value;
                }

                expect(sum).toBe(100);
            });
        });
    });

    describe('get client demographic gender', () => {

        beforeEach(inject(() => {
            requestHandler = $httpBackend.when('POST', ELASTIC_REQUEST_TEST).respond(genderResponse());
        }));

        it('returns an error', () => {
            const ERROR_MESSAGE = '400 error';
            let response;
            requestHandler.respond(400, ERROR_MESSAGE);
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            factory.getGender({id: null, name: 'All stores'}).catch(message => response = message);
            $httpBackend.flush();
            expect(response).toBe(ERROR_MESSAGE);
        });

        it('returns a promise', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getGender({id: null, name: 'All stores'});
            $httpBackend.flush();
            expect(typeof getResponse.then).toBe('function');
            expect(typeof getResponse.catch).toBe('function');
        });

        it('returns a result', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getGender({id: null, name: 'All stores'});
            $httpBackend.flush();
            expect(Array.isArray(getResponse.result)).toBeTruthy();
        });

        it('returns data for gender', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getGender({id: null, name: 'All stores'});
            $httpBackend.flush();
            getResponse.then(function(result){
                expect(result.objArray).toEqual(
                    [
                        {label: 'M',value: 71},
                        {label: 'F',value: 29}
                    ]
                );
            });
        });

        it(', values must add up to 100 (percent)', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getGender({id: null, name: 'All stores'});
            $httpBackend.flush();
            getResponse.then(function(result){
                let sum = 0;
                for (let i = 0; i < result.objArray.length; i++) {
                    sum += result.objArray[i].value;
                }

                expect(sum).toBe(100);
            });
        });
    });

    describe('client demographic gender array length', () => {

        beforeEach(inject(() => {
            requestHandler = $httpBackend.when('POST', ELASTIC_REQUEST_TEST).respond(genderLengthResponse());
        }));

        it(' must be equal to 2 (for male and female)', () => {
            let getResponse;
            $httpBackend.expect('POST', ELASTIC_REQUEST_TEST);
            getResponse = factory.getGender({id: null, name: 'All stores'});
            $httpBackend.flush();
            getResponse.then(function(result){
                expect(result.objArray.length).toBe(2);
            });
        });
    });
    
    function raceResponse() {
        return {
            'took': 944,
            'timed_out': false,
            '_shards': {
                'total': 6,
                'successful': 6,
                'failed': 0
            },
            'hits': {
                'total': 12487063,
                'max_score': 0,
                'hits': []
            },
            'aggregations': {
                '2': {
                    'doc_count_error_upper_bound': 0,
                    'sum_other_doc_count': 0,
                    'buckets': [
                        {
                            'key': 'WHITE',
                            'doc_count': 1384667
                        },
                        {
                            'key': 'COLOURED',
                            'doc_count': 823409
                        },
                        {
                            'key': 'AFRICAN',
                            'doc_count': 300751
                        },
                        {
                            'key': 'UNKNOWN',
                            'doc_count': 198996
                        },
                        {
                            'key': 'ASIAN',
                            'doc_count': 73569
                        },
                        {
                            'key': 'NOT APPLICABLE',
                            'doc_count': 3507
                        }
                    ]
                }
            }
        };
    }

    function genderResponse(){
        return {
            "took": 1280,
            "timed_out": false,
            "_shards": {
                "total": 6,
                "successful": 6,
                "failed": 0
            },
            "hits": {
                "total": 12487063,
                "max_score": 0,
                "hits": []
            },
            "aggregations": {
                "2": {
                    "doc_count_error_upper_bound": 0,
                    "sum_other_doc_count": 0,
                    "buckets": [
                        {
                            "key": "M",
                            "doc_count": 8650426
                        },
                        {
                            "key": "F",
                            "doc_count": 3487156
                        }
                    ]
                }
            }
        };
    }

    function genderLengthResponse(){
        return {
            "took": 1280,
            "timed_out": false,
            "_shards": {
                "total": 6,
                "successful": 6,
                "failed": 0
            },
            "hits": {
                "total": 12487063,
                "max_score": 0,
                "hits": []
            },
            "aggregations": {
                "2": {
                    "doc_count_error_upper_bound": 0,
                    "sum_other_doc_count": 0,
                    "buckets": [
                        {
                            "key": "M",
                            "doc_count": 8650426
                        }
                    ]
                }
            }
        };
    }
});
