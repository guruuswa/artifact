describe('clientDemographicModel', () => {
    let model;

    beforeEach(inject((_clientDemographicModel_) => {
        model = _clientDemographicModel_;
    }));

    describe('clientDemographicModel instance', () => {
        it('can create a new instance', () => {
            expect(typeof model()).toEqual('object');
        });
    });
});