(function () {
    angular
        .module('appInterfaceClientDemographic')
        .service('clientDemographicModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                objArray: []
            });
            return model.set(data);
        };
    }
})();
