(function() {
    angular
        .module('appInterfaceUser')
        .factory('userFactory', factory);

    function factory(userModel, $http, $q) {
        var model = userModel();

        return {
            //TODO: Waiting for clarification from client
            login: (username /*, password*/ ) => {
                var newModel = new userModel();
                newModel.name = username;
                newModel.token = '1234567890#';
                newModel.pathToProfileLogo = 'assets/images/logo_profile_kfc.png';

                model.set(newModel);

                return $q(
                    function(resolve /*, reject*/ ) {
                        resolve(model);
                    });
            },

            getUser: () => {
                return model.get();
            },

            logout: () => model.set(new userModel()),
            isLoggedIn: () => !!model.token,

            setTcAccepted: () => model.get().termsConditionsAcceptedDateTime = new Date().getTime(),

            hasTCAccepted: () => !!model.termsConditionsAcceptedDateTime,
        };
    }
})();