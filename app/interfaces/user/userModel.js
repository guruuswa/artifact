(function() {
    angular
        .module('appInterfaceUser')
        .service('userModel', service);

    function service(modelHelper) {
        return function(data) {

            var model = new modelHelper({
                name: 'Bob Pedersen',
                id: '10',
                pathToProfileLogo: 'assets/images/logo_profile_kfc.png',
                token: null,
                expireDateTime: null,
                termsConditionsAcceptedDateTime: null,
            });

            return model.set(data);
        };
    }
})();