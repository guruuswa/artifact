describe('userModel', () => {
    let model;

    beforeEach(inject((_userModel_) => {
        model = _userModel_;
    }));


    describe('userModel instance', () => {
        it('can create a new instance', () => {
            expect(typeof model()).toEqual('object');
        });
    });
});