(function () {
    angular
        .module('appInterfaceMerchants')
        .service('merchantsModel', service);

    function service(modelHelper) {
        return function (data) {
            var model = new modelHelper({
                id: '',
                name: ''
            });
            return model.set(data);
        };
    }
})();
