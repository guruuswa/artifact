describe('merchantsModel', () => {
    let model;

    beforeEach(inject((_merchantsModel_) => {
        model = _merchantsModel_;
    }));

    describe('merchantsModel instance', () => {
        it('can create a new instance', () => {
            expect(typeof model()).toEqual('object');
        });
    });
});
