(function () {
    angular
        .module('appInterfaceMerchants')
        .factory('merchantsFactory', factory);

    function factory($q, merchantsModel, elasticRequestHelper, eventHelper) {
        const ALL_STORES_MERCHANT = merchantsModel({
            id: null,
            name: 'All stores'
        });
        const ELASTIC_REQUEST = elasticRequestHelper('merchants', mapToModel, {
            cache: true,
            isArray: true
        });

        let activeMerchant;

        let service = {
            get: () => ELASTIC_REQUEST.query(getQuery()),
            getWithAllStores: () => {
                let deferred = $q.defer();
                let result = [];

                service.get().then(data => {
                    let merchantList = angular.copy(data);
                    merchantList.unshift(ALL_STORES_MERCHANT);
                    angular.copy(merchantList, result);
                    deferred.resolve(merchantList);
                });

                deferred.promise.result = result;

                return deferred.promise;
            },
            getById: (id) => {
                let deferred = $q.defer();
                let result = {};

                service.get().then(data => {
                    let merchant = angular.copy(data.find(x => x.id === id));
                    angular.copy(merchant, result);
                    deferred.resolve(merchant);
                });

                deferred.promise.result = result;

                return deferred.promise;
            },
            getActive: () => {
                return activeMerchant || ALL_STORES_MERCHANT;
            },
            setActive: (id) => {
                activeMerchant = service.get().result.find(x => x.id === id);
                service.activeUpdate.broadcast();
                return service.getActive();
            },
            getQueryArray: getQueryArray,
            clear: () => {
                return ELASTIC_REQUEST.clearCache();
            },
            activeUpdate: new eventHelper()
        };

        return service;

        function mapToModel(response) {
            return response.aggregations.merchants.buckets.map(x => {
                return merchantsModel({
                    id: x.merchantNumberAndValue.buckets[0].key,
                    name: x.key,
                    averageValue: x.merchantNumberAndValue.buckets[0].averageValueForMerchant.value
                });
            });
        }

        function getQuery() {
            return {
                size: 0,
                query: {
                    filtered: {
                    }
                },
                aggs: {
                    merchants: {
                        terms: {
                            field: 'Merchant name.raw',
                            size: 100,
                            order: {
                                _term: 'asc'
                            }
                        },
                        aggs: {
                            merchantNumberAndValue: {
                                terms: {
                                    field: 'Merchant number.raw',
                                    size: 5,
                                    order: {
                                        _term: 'desc'
                                    }
                                },
                                aggs: {
                                    averageValueForMerchant: {
                                        avg: {
                                            field: 'PIRZ_AMT'
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
        }

        function getQueryArray() {
            let args = (arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments));
            return args.map(x => {
                return {
                    term: {
                        'Merchant number.raw': angular.isObject(x) ? x.id : x
                    }
                };
            });
        }
    }
})();
