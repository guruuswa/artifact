describe('merchantsFactory', () => {
    let factory, $httpBackend, configApiEndpoint, requestHandler, requestArguments;

    beforeEach(() => {
        inject((_$httpBackend_, _configApiEndpoint_, _merchantsFactory_) => {
            $httpBackend = _$httpBackend_;
            configApiEndpoint = _configApiEndpoint_;
            factory = _merchantsFactory_;
        });
        requestArguments = ['POST', configApiEndpoint, undefined, (headers) => headers.Namespace === 'merchants'];
        requestHandler = $httpBackend.when.apply(null, requestArguments);
        requestHandler.respond(MOCK_MERCHANTS);
    });

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    describe('merchantsFactory object', () => {
        it('is an object', () => {
            expect(typeof factory).toEqual('object');
        });
    });

    describe('get merchants method', () => {
        it('returns api data', () => {
            let getResponse;
            $httpBackend.expect.apply(null, requestArguments);
            getResponse = factory.get();
            $httpBackend.flush();
            expect(typeof getResponse.then).toBe('function');
            expect(getResponse.result.length).toBe(2);
            expect(getResponse.result[0].id).toBe('3480828');
            getResponse = factory.get();
            expect(typeof getResponse.then).toBe('function');
            expect(getResponse.result.length).toBe(2);
            expect(getResponse.result[0].id).toBe('3480828');
        });
    });

    describe('clear cache method', () => {
        it('clears cache', () => {
            expect(factory.clear()).toBeUndefined();
        });
    });

    describe('get merchants incl all stores', () => {
        it('returns merchants incl all stores', () => {
            let getResponse;
            $httpBackend.expect.apply(null, requestArguments);
            getResponse = factory.getWithAllStores();
            $httpBackend.flush();
            expect(getResponse.result[0].name).toEqual('All stores');
            expect(getResponse.result[1].id).toEqual('3480828');
        });
    });

    describe('get merchant by id', () => {
        it('gets merchant by id', () => {
            let id = '3311554';
            let getByIdResponse = factory.getById(id);
            $httpBackend.flush();
            expect(getByIdResponse.result.id).toBe(id);
        });
    });

    describe('active merchant', () => {
        it('returns the default merchant', () => {
            expect(factory.getActive().id).toEqual(null);
            expect(factory.getActive().name).toEqual('All stores');
        });

        it('sets active by id', () => {
            let newActiveId = '3311554';
            factory.get();
            $httpBackend.flush();
            expect(factory.setActive(newActiveId).id).toBe(newActiveId);
            expect(factory.getActive().id).toBe(newActiveId);
        });

        it('gets all stores if invalid id set', () => {
            factory.setActive();
            $httpBackend.flush();
            expect(factory.getActive().id).toEqual(null);
            expect(factory.getActive().name).toEqual('All stores');
        });
    });

    describe('active update', () => {
        it('has an activeUpdate property', () => {
            expect(typeof factory.activeUpdate.broadcast).toBe('function');
            expect(typeof factory.activeUpdate.subscribe).toBe('function');
        });
    });

    describe('get query array', () => {
        it('accepts no arguments', () => {
            let merchantQueryArray = factory.getQueryArray();
            expect(Array.isArray(merchantQueryArray)).toBeTruthy();
            expect(merchantQueryArray.length).toBe(0);
        });

        it('accepts one argument', () => {
            let merchantQueryArray;
            let merchants = factory.get().result;
            $httpBackend.flush();
            merchantQueryArray = factory.getQueryArray(merchants[0]);
            expect(merchantQueryArray.length).toBe(1);
            expect(merchantQueryArray[0].term['Merchant number.raw']).toBe('3480828');
        });

        it('accepts multiple arguments', () => {
            let merchantQueryArray;
            let merchants = factory.get().result;
            $httpBackend.flush();
            merchantQueryArray = factory.getQueryArray.apply(null, merchants);
            expect(merchantQueryArray.length).toBe(2);
            expect(merchantQueryArray[0].term['Merchant number.raw']).toBe('3480828');
        });

        it('accepts merchant ids instead of merchant model', () => {
            let merchantQueryArray;
            let merchants = factory.get().result;
            $httpBackend.flush();
            merchantQueryArray = factory.getQueryArray.apply(null, merchants.map(x => x.id));
            expect(merchantQueryArray.length).toBe(2);
            expect(merchantQueryArray[0].term['Merchant number.raw']).toBe('3480828');
        });
    });
});
