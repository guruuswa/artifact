(function () {
    angular
        .module('appInterfaceTopPerformingStore')
        .factory('topPerformingStoresFactory', factory);

    function factory($q, $http, configApiEndpoint) {

        let service = {
            getData: getData,
            mapDataToModel: (data) => {
                let topStoreName = data.aggregations['2'].buckets[0]['3'].buckets[0].key;
                let value = data.aggregations['2'].buckets[0]['3'].buckets[0]['1'].value;
                let volume = data.aggregations['2'].buckets[0]['3'].buckets[0].doc_count;
                let storeId = data.aggregations['2'].buckets[0].key;
                return {
                    topStoreName: topStoreName,
                    value: value,
                    volume: volume,
                    storeId: storeId
                };
            }
        };

        function getData(merchant) {
            let merchantFilter = {};

            if (merchant && merchant.name !== 'All stores') {
                merchantFilter = {
                    term: {
                        'Merchant name.raw': merchant.name
                    }
                };
            }

            let deferred = $q.defer();

            $http({
                method: 'POST',
                url: configApiEndpoint,
                data: {
                    size: 0,
                    query: {
                        filtered: {
                            query: {
                                query_string: {
                                    analyze_wildcard: true,
                                    query: '*'
                                }
                            },
                            filter: {
                                bool: {
                                    must: [
                                        {
                                            range: {
                                                '@timestamp': {
                                                    gte: '1/12/2014',
                                                    lte: '1/12/2016',
                                                    format: 'dd/MM/yyyy||dd/MM/yyyy'
                                                }
                                            }
                                        }
                                    ],
                                    must_not: [],
                                    should: merchantFilter
                                }
                            }
                        }
                    },
                    aggs: {
                        2: {
                            terms: {
                                field: 'Merchant number.raw',
                                size: 1,
                                order: {
                                    1: 'desc'
                                }
                            },
                            aggs: {
                                1: {
                                    sum: {
                                        field: 'PIRZ_AMT'
                                    }
                                },
                                3: {
                                    terms: {
                                        field: 'Merchant name.raw',
                                        size: 1,
                                        order: {
                                            1: 'desc'
                                        }
                                    },
                                    aggs: {
                                        1: {
                                            sum: {
                                                field: 'PIRZ_AMT'
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }).then(response => {
                let data = service.mapDataToModel(response.data);
                deferred.resolve(data);
            }).catch(response => {
                deferred.reject(response.data);
            });

            return deferred.promise;
        }

        return service;
    }
})();
