describe('sbTopPerformingStore', () => {
    const TOP_PERFORMIN_STORE_REQUEST = '/_search';
    const TOP_PERFORMIN_STORE_REQUEST_TEST = RegExp(TOP_PERFORMIN_STORE_REQUEST, 'i');
    let factory, $httpBackend;
    let requestHandler;

    beforeEach(inject((_topPerformingStoresFactory_) => {
        factory = _topPerformingStoresFactory_;
    }));

    it('should map the response object to the correct model object format', () => {
        var mockDataObject = getMockDataObject();
        var modelObject = factory.mapDataToModel(mockDataObject);

        expect(modelObject).toEqual({
            topStoreName: 'SOUTH AFRICAN AIRWAYS FLYSAA.C',
            value: 24174856.849999994,
            volume: 4996,
            storeId: '2716550'
        });
    });

    describe('topPerformingStore Async Calls', () => {
        beforeEach(inject((_$httpBackend_) => {
            $httpBackend = _$httpBackend_;

            requestHandler = $httpBackend.when('POST', TOP_PERFORMIN_STORE_REQUEST_TEST).respond(mockReturnObject());
        }));

        afterEach(() => {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        describe('topPerformingStoresFactory object', () => {
            it('is an object', () => {
                expect(typeof factory).toEqual('object');
            });
        });

        describe('get data from server', () => {
            it('should return the data to display', () => {
                spyOn(factory, 'mapDataToModel').and.returnValue({
                    id: 'Test',
                    name: 'Mock'
                });

                let getResponse;
                $httpBackend.expect('POST', TOP_PERFORMIN_STORE_REQUEST_TEST);
                getResponse = factory.getData();
                $httpBackend.flush();
                getResponse.then(function (result) {
                    expect(result.name).toEqual('Mock');
                    expect(result.id).toEqual('Test');
                });

            });
        });

        function mockReturnObject() {
            return {
                id: 'Test',
                name: 'Mock'
            };
        }
    });

    function getMockDataObject() {
        return {
            "took": 1876,
            "timed_out": false,
            "_shards": {
                "total": 6,
                "successful": 6,
                "failed": 0
            },
            "hits": {
                "total": 11736559,
                "max_score": 0,
                "hits": []
            },
            "aggregations": {
                "2": {
                    "doc_count_error_upper_bound": -1,
                    "sum_other_doc_count": 1909246,
                    "buckets": [
                        {
                            "1": {
                                "value": 24174856.849999994
                            },
                            "3": {
                                "doc_count_error_upper_bound": 0,
                                "sum_other_doc_count": 0,
                                "buckets": [
                                    {
                                        "1": {
                                            "value": 24174856.849999994
                                        },
                                        "key": "SOUTH AFRICAN AIRWAYS FLYSAA.C",
                                        "doc_count": 4996
                                    }
                                ]
                            },
                            "key": "2716550",
                            "doc_count": 4996
                        }
                    ]
                }
            }
        }
    };
});
