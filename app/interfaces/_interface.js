(function() {
    angular.module('appInterface', [
        'appInterfaceUser',
        'appInterfaceFilters',
        'appInterfaceMerchants',
        'appInterfaceSidebar',
        'appInterfaceSpotlight',
        'appInterfaceMerchantSales',
        'appInterfaceNewVsReturningCustomers',
        'appInterfaceCustomerSpendRange',
        'appInterfaceSingleTransaction',
        'appInterfaceTopPerformingStore',
        'appInterfaceClientDemographic',
        'appInterfaceTradingDays',
        'appInterfaceTradingHours',
        'appInterfaceCustomerLocation',
        'appInterfaceAverageCustomerValue',
        'appInterfaceCustomerCount',
        'appInterfaceSpendingTrends',
        'appInterfaceSalesTrends'
    ]);
})();