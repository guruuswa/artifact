describe('spotlightFactory', () => {
  const SPOTLIGHT_REQUEST = '/_search';
  const SPOTLIGHT_REQUEST_TEST = RegExp(SPOTLIGHT_REQUEST, 'i');
  let factory, $httpBackend;
  let requestHandler;

  beforeEach(inject((_spotlightFactory_, _$httpBackend_) => {
    factory = _spotlightFactory_;
    $httpBackend = _$httpBackend_;

    requestHandler = $httpBackend.when('POST', SPOTLIGHT_REQUEST_TEST).respond(mockSpotlight());
  }));

  afterEach(() => {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('spotlightFactory object', () => {
    it('returns an object', () => {
      expect(typeof factory).toEqual('object');
    });
  });

  describe('get storage key', () => {
    it('returns storage key string', () => {
      expect(typeof factory.storageKey).toBe('string');
    });
  });

  describe('get spotlight method', () => {
    it('returns an error', () => {
      const ERROR_MESSAGE = '400 error';
      let response;
      requestHandler.respond(400, ERROR_MESSAGE);
      $httpBackend.expect('POST', SPOTLIGHT_REQUEST_TEST);
      factory.get({
        id: null,
        name: 'All stores'
      }).catch(message => response = message);
      $httpBackend.flush();
      expect(response).toBe(ERROR_MESSAGE);
    });

    it('returns api data', () => {
      let getResponse;
      $httpBackend.expect('POST', SPOTLIGHT_REQUEST_TEST);
      getResponse = factory.get({
        id: null,
        name: 'All stores'
      });
      $httpBackend.flush();
      expect(typeof getResponse.then).toBe('function');
      expect(getResponse.result.length).toBe(3);
      expect(getResponse.result[0].title).toBe('Sales value');
      expect(typeof getResponse.result[0].value === 'number');
    });

    it('returns the correct percentage', () => {
      let getResponse = factory.get({
        id: null,
        name: 'All stores'
      });
      let result = getResponse.result;
      for (let i = 0; i < result.length; i++) {
        if (result[i].title === 'Sales value') {
          expect(Math.round(result[i].percentage)).toBe(87);
        } else if (result[i].title === 'Sales volume') {
          expect(Math.round(result[i].percentage)).toBe(90);
        } else if (result[i].title === 'Average transaction size') {
          expect(Math.round(result[i].percentage)).toBe(-2);
        }
      }
    });

    it('returns cached api data', () => {
      let getResponse = factory.get({
        id: null,
        name: 'All stores'
      });
      expect(typeof getResponse.then).toBe('function');
      expect(getResponse.result.length).toBe(3);
      expect(getResponse.result[0].title).toBe('Sales value');
      expect(typeof getResponse.result[0].value === 'number');
    });
  });

  describe('clear cache method', () => {
    it('clears cache', () => {
      expect(factory.clear()).toBeUndefined();
      expect(window.sessionStorage.getItem(factory.storageKey)).toBeNull();
    });
  });

  describe('active update', () => {
    it('has an activeUpdate property', () => {
      expect(typeof factory.activeUpdate.broadcast).toBe('function');
      expect(typeof factory.activeUpdate.subscribe).toBe('function');
    });
  });


  function mockSpotlight() {
    return {
      "took": 579,
      "timed_out": false,
      "_shards": {
        "total": 35,
        "successful": 35,
        "failed": 0
      },
      "hits": {
        "total": 17123567,
        "max_score": 0,
        "hits": []
      },
      "aggregations": {
        "spotlight_sum": {
          "buckets": [{
            "key": "filter_by",
            "from": 1436867934874,
            "from_as_string": "2015-07-14",
            "to": 1439546334874,
            "to_as_string": "2015-08-14",
            "doc_count": 1717956,
            "by_sum": {
              "value": 1218036975.5701869
            }
          }, {
            "key": "compare_to",
            "from": 1434275934874,
            "from_as_string": "2015-06-14",
            "to": 1436867934874,
            "to_as_string": "2015-07-14",
            "doc_count": 904404,
            "by_sum": {
              "value": 652411279.1000458
            }
          }]
        },
        "spotlight_avg": {
          "buckets": [{
            "key": "filter_by",
            "from": 1436867934874,
            "from_as_string": "2015-07-14",
            "to": 1439546334874,
            "to_as_string": "2015-08-14",
            "doc_count": 1717956,
            "by_average": {
              "value": 709.0035923912992
            }
          }, {
            "key": "compare_to",
            "from": 1434275934874,
            "from_as_string": "2015-06-14",
            "to": 1436867934874,
            "to_as_string": "2015-07-14",
            "doc_count": 904404,
            "by_average": {
              "value": 721.3715099668354
            }
          }]
        }
      }
    };
  }

});
