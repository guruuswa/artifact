(function () {
    angular
        .module('appInterfaceSpotlight')
        .factory('spotlightFactory', factory);

    function factory($window, $q, $http, spotlightModel, eventHelper, configApiEndpoint) {
        const STORAGE_KEY = 'spotlight';

        let spotlight = JSON.parse($window.sessionStorage.getItem(STORAGE_KEY));

        let service = {
            storageKey: STORAGE_KEY,
            get: (merchant, dateStart = '2016-01-11', dateEnd = '2016-01-25', dateCompareStart = '2015-12-28', dateCompareEnd = '2016-01-10') => {
                let deferred = $q.defer();
                let result = [];

                if (!spotlight) {
                    $http({
                        method: 'POST',
                        url: configApiEndpoint,
                        data: getQuery(merchant, dateStart, dateEnd, dateCompareStart, dateCompareEnd)
                    }).then(response => {
                        spotlight = mapToModel(response.data.aggregations);
                        $window.sessionStorage.setItem(STORAGE_KEY, JSON.stringify(spotlight));
                        angular.copy(spotlight, result);
                        deferred.resolve(spotlight);
                    }).catch(response => {
                        deferred.reject(response.data);
                    });
                } else {
                    angular.copy(spotlight, result);
                    deferred.resolve(spotlight);
                }

                deferred.promise.result = result;

                return deferred.promise;
            },
            clear: () => {
                spotlight = undefined;
                $window.sessionStorage.removeItem(STORAGE_KEY);
                return spotlight;
            },
            onUpdate: new eventHelper(),
            activeUpdate: new eventHelper()
        };

        return service;

        function mapToModel(data) {
            let salesValueBucket = data.spotlight_sum.buckets.find(x => x.key === 'filter_by');
            let salesValueCompareBucket = data.spotlight_sum.buckets.find(x => x.key === 'compare_to');
            let transactionValueBucket = data.spotlight_avg.buckets.find(x => x.key === 'filter_by');
            let transactionValueCompareBucket = data.spotlight_avg.buckets.find(x => x.key === 'compare_to');

            let salesValuePercentage = calculatePercentage(salesValueBucket.by_sum.value, salesValueCompareBucket.by_sum.value);
            let salesVolumePercentage = calculatePercentage(salesValueBucket.doc_count, salesValueCompareBucket.doc_count);
            let transactionValuePercentage = calculatePercentage(transactionValueBucket.by_average.value, transactionValueCompareBucket.by_average.value);

            let spotlightValues = [];

            let spotlightSalesValues = spotlightModel().set({
                title: 'Sales value',
                value: salesValueBucket.by_sum.value,
                compareValue: salesValueCompareBucket.by_sum.value,
                percentage: salesValuePercentage.percentage,
                isIncrease: salesValuePercentage.increase,
                icon: 'icn_graph_36x36.png',
                currency: 'R'
            });
            spotlightValues.push(spotlightSalesValues);

            let spotlightSalesVolume = spotlightModel().set({
                title: 'Sales volume',
                value: salesValueBucket.doc_count,
                compareValue: salesValueCompareBucket.doc_count,
                percentage: salesVolumePercentage.percentage,
                isIncrease: salesVolumePercentage.increase,
                icon: 'icn_cart_36x36.png'
            });
            spotlightValues.push(spotlightSalesVolume);

            let spotlightAverageTransactionValue = spotlightModel().set({
                title: 'Average transaction size',
                value: transactionValueBucket.by_average.value,
                compareValue: transactionValueCompareBucket.by_average.value,
                percentage: transactionValuePercentage.percentage,
                isIncrease: transactionValuePercentage.increase,
                icon: 'icn_card_36x36.png',
                currency: 'R'
            });
            spotlightValues.push(spotlightAverageTransactionValue);

            return spotlightValues;
        }

        function calculatePercentage(value, compareValue) {
            let percentage = 0;
            let increase = true;
            let result = value - compareValue;

            if (result === 0) {
                increase = false;
                percentage = 0;
            } else if (result >= compareValue) {
                increase = true;
                percentage = 100;
            } else if (result < 0) {
                increase = false;
                let pos = Math.abs(result);

                if (pos > compareValue) {
                    percentage = 100;
                } else {
                    percentage = (pos / compareValue) * 100;
                }
            } else if (result > 0 && result < compareValue) {
                increase = true;
                percentage = (result / compareValue) * 100;

            }

            if (!increase) {
                percentage = -Math.abs(percentage);
            }

            return {
                percentage: percentage,
                increase: increase
            };
        }

        function getMerchantFilter(merchant) {
            var merchantFilter = {};

            if (merchant && merchant.name !== 'All stores') {
                merchantFilter = {
                    'term': {
                        'Merchant name.raw': merchant.name
                    }
                };
            }

            return merchantFilter;
        }

        function getQuery(merchant, dateStart, dateEnd, dateCompareStart, dateCompareEnd) {
            var merchantFilter = getMerchantFilter(merchant);
            return {
                'size': 0,
                'query': {
                    'filtered': {
                        'query': {
                            'query_string': {
                                'query': '*',
                                'analyze_wildcard': true
                            }
                        },
                        'filter': {
                            'bool': {
                                'must': [],
                                'must_not': [],
                                should: merchantFilter
                            }
                        }
                    }
                },
                'aggs': {
                    'spotlight_avg': {
                        'date_range': {
                            'field': '@timestamp',
                            'format': 'yyyy-MM-dd',
                            'ranges': [{
                                'key': 'filter_by',
                                'from': dateStart,
                                'to': dateEnd
                            }, {
                                'key': 'compare_to',
                                'from': dateCompareStart,
                                'to': dateCompareEnd
                            }]
                        },
                        'aggs': {
                            'by_average': {
                                'avg': {
                                    'field': 'PIRZ_AMT'
                                }
                            }
                        }
                    },
                    'spotlight_sum': {
                        'date_range': {
                            'field': '@timestamp',
                            'format': 'yyyy-MM-dd',
                            'ranges': [{
                                'key': 'filter_by',
                                'from': dateStart,
                                'to': dateEnd
                            }, {
                                'key': 'compare_to',
                                'from': dateCompareStart,
                                'to': dateCompareEnd
                            }]
                        },
                        'aggs': {
                            'by_sum': {
                                'sum': {
                                    'field': 'PIRZ_AMT'
                                }
                            }
                        }
                    }
                }
            };
        }
    }
})();