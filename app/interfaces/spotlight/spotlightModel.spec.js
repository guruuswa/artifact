describe('spotlightModel', () => {
  let model;

  beforeEach(inject((_spotlightModel_) => {
    model = _spotlightModel_;
  }));


  describe('spotlightModel instance', () => {
    it('can create a new instance', () => {
      expect(typeof model()).toEqual('object');
    });
  });
});