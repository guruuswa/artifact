(function () {
  angular
    .module('appInterfaceSpotlight')
    .service('spotlightModel', service);

  function service(modelHelper) {
    return function (data) {
      var model = new modelHelper({
          title: '',
          value: 0,
          compareValue: 0,
          percentage: 0,
          isIncrease: false,
          icon: '',
          currency: ''
      });
      return model.set(data);
    };
  }
})();
