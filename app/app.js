(function() {
    angular
        .module('app', [
            // angular
            'ngAnimate',
            'ngRoute',
            'ngTouch',
            // compiled
            'config',
            'templates',
            // app
            'appHelper',
            'appInterface',
            'appView',
            'appComponent'
        ])
        .run(run)
        .config(config);

    //TODO: Waiting for clarification from client
    function run(loadingHelper, $rootScope) {
        loadingHelper.startFullPage();

        //TODO: Check the token status from SB - are we logged in?
        $rootScope.$on('$routeChangeSuccess', (event, current) => {
            if (current && current.$$route) {
                loadingHelper.startFullPage();
            }
        });
    }

    function config($httpProvider) {
        $httpProvider.interceptors.push('httpRequestInterceptor');
    }
})();