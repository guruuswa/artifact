(function () {
    angular
        .module('app')
        .factory('httpRequestInterceptor', function () {
            return {
                request: function (config) {
                    let token ='';
                    if(typeof document.getElementsByName('jwt-token')[0] !== 'undefined'){
                        token = document.getElementsByName('jwt-token')[0].value;
                    }

                    token = 'eyJhbGciOiJub25lIiwidHlwIjoiSldUIiwieDV0IjoiTkEifQ.eyJpc3MiOiJodHRwczovL21lcmNoYW50b25saW5lLmVjZW50cmljLmNvLnphL01lcmNoYW50UG9ydGFsIiwiYXVkIjoiaHR0cHM6Ly9TQlNBL01lcmNoYW50VmlldyIsInN1YiI6InRlc3RAZW1haWwuY29tIiwiaWF0IjoiMTQ1MTkxMDg5OCIsIm5iZiI6IjE0NTE5MTA4OTgiLCJleHAiOiIxNDUxOTExMDc4IiwianRpIjoiYzBkOWY1ZmEtMWU1Mi00YzZhLTg4NzYtMTI4NmI4MjdmYzM3In0.Rd66jW2xj9vokJGUzwr4TOD76sRwkMC6jelhGmjeKKs';

                    config.headers['JWT-Token'] = token;
                    return config;
                }
            };
        });
})();