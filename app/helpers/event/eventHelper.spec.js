describe('eventHelper', function () {
    let eventHelper;

    beforeEach(inject((_eventHelper_) => {
        eventHelper = _eventHelper_;
    }));

    describe('eventHelper instance', () => {
        it('can create a new instance', () => {
            expect(typeof (new eventHelper())).toEqual('object');
        });
    });
});