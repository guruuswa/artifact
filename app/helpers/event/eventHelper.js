(function () {
    angular
        .module('appHelperEvent')
        .service('eventHelper', service);

    function service($window) {
        function Event() {
            this.handlers = [];
        }

        Event.prototype = {
            broadcast: function (action, thisObj) {
                let scope = thisObj || $window;
                this.handlers.forEach((item) => {
                    item(action, scope);
                });
            },
            subscribe: function (scope, callBack) {
                let event = this;
                this.handlers.push(callBack);
                if (!scope) {
                    return;
                }
                scope.$on('$destroy', () => {
                    event.unsubscribe(callBack);
                });
            },
            unsubscribe: function (callBack) {
                this.handlers = this.handlers.filter((item) => {
                    if (item !== callBack) {
                        return item;
                    }
                });
            }
        };

        return Event;
    }
})();