(function() {
    angular
        .module('appHelperFilters')
        .filter('filterCurrency', filter);

    function filter($filter) {
        return function (amount, symbol, fractionSize) {
            amount = amount || 0;
            symbol = symbol || '';
            fractionSize = fractionSize || 0;
            let filterCurrency = $filter('currency')(amount, symbol + ' ', fractionSize);
            if (amount !== undefined && amount !== null) {
                filterCurrency = filterCurrency.replace(/,/g, ' ');
            }
            return filterCurrency;
        };
    }
})();