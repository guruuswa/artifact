describe('filterCurrency', () => {
    let $filter;

    beforeEach(inject((_$filter_) => {
        $filter = _$filter_;
    }));

    describe('filterCurrency instance', () => {
        it('can create a new instance', () => {
            expect(typeof $filter('filterCurrency')).toEqual('function');
        });
    });
});