describe('elasticRequestHelper', function () {
    const NAME = 'elasticRequestHelper';
    let $httpBackend, configApiEndpoint, elasticRequestHelper, requestArguments, requestHandler;

    beforeEach(() => {
        inject((_$httpBackend_, _configApiEndpoint_, _elasticRequestHelper_) => {
            $httpBackend = _$httpBackend_;
            configApiEndpoint = _configApiEndpoint_;
            elasticRequestHelper = _elasticRequestHelper_;
        });
        requestArguments = ['POST', configApiEndpoint, undefined, (headers) => headers.Namespace === NAME];
        requestHandler = $httpBackend.when.apply(null, requestArguments).respond(mockResponse());
    });

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    describe('elasticRequestHelper instance', function () {
        it('can create an instance', function () {
            let request = elasticRequestHelper(NAME);
            expect(typeof request).toEqual('object');
        });
    });

    describe('query', function () {
        it('catch errors', () => {
            const ERROR_MESSAGE = '400 error';
            let request, response;
            request = elasticRequestHelper(NAME);
            requestHandler.respond(400, ERROR_MESSAGE);
            $httpBackend.expect.apply(null, requestArguments);
            request.query().catch((message) => response = message);
            $httpBackend.flush();
            expect(response).toBe(ERROR_MESSAGE);
        });

        it('returns api data (no mapping)', () => {
            let response, result;
            let request = elasticRequestHelper(NAME).query();
            $httpBackend.expect.apply(null, requestArguments);
            request.then((responseData) => response = responseData);
            result = request.result;
            $httpBackend.flush();
            expect(response).toEqual(mockResponse());
            expect(result).toEqual(mockResponse());
        });

        it('returns api data (with mapping to object)', () => {
            let response, result;
            const EXPECTED_RESPONSE = {
                key: 'Key 1',
                value: 1
            };
            let request = elasticRequestHelper(NAME, mapToObject).query();
            $httpBackend.expect.apply(null, requestArguments);
            request.then((responseData) => response = responseData);
            result = request.result;
            $httpBackend.flush();
            expect(response).toEqual(EXPECTED_RESPONSE);
            expect(result).toEqual(EXPECTED_RESPONSE);
        });

        it('returns api data (with mapping to array)', () => {
            let response, result;
            const EXPECTED_RESPONSE = [{
                key: 'Key 1',
                value: 1
            }, {
                key: 'Key 2',
                value: 2
            }];
            let request = elasticRequestHelper(NAME, mapToArray, {isArray: true}).query();
            $httpBackend.expect.apply(null, requestArguments);
            request.then((responseData) => response = responseData);
            result = request.result;
            $httpBackend.flush();
            expect(response).toEqual(EXPECTED_RESPONSE);
            expect(result).toEqual(EXPECTED_RESPONSE);
        });
    });

    describe('cache management', () => {
        it('gets data from cache', () => {
            let result;
            let request = elasticRequestHelper(NAME, null, {cache: true});
            $httpBackend.expect.apply(null, requestArguments);
            request.query();
            $httpBackend.flush();
            result = request.query().result;
            expect(result).toEqual(mockResponse());
        });

        it('clear cache', () => {
            let result;
            let request = elasticRequestHelper(NAME, null, {cache: true});
            $httpBackend.expect.apply(null, requestArguments);
            request.query();
            $httpBackend.flush();
            result = request.clearCache();
            expect(result).toBeUndefined();
        });

        it('does not send multiple requests if already in progress', () => {
            let result;
            let request = elasticRequestHelper(NAME, null, {cache: true});
            $httpBackend.expect.apply(null, requestArguments);
            request.query();
            request.query();
            $httpBackend.flush(1);
            result = request.query().result;
            expect(result).toEqual(mockResponse());
        });
    });


    function mapToObject(data) {
        return {
            key: data.aggregations.data.buckets[0].key,
            value: data.aggregations.data.buckets[0].item.buckets[0].value
        };
    }

    function mapToArray(data) {
        return data.aggregations.data.buckets.map(x => {
            return {
                key: x.key,
                value: x.item.buckets.reduce((a, b) => a + b.value, 0)
            };
        });
    }

    function mockResponse() {
        return {
            'aggregations': {
                'data': {
                    'buckets': [{
                        'item': {
                            'buckets': [{
                                'value': 1,
                                'key': '123'
                            }]
                        },
                        'key': 'Key 1'
                    }, {
                        'item': {
                            'buckets': [{
                                'value': 2,
                                'key': '234'
                            }]
                        },
                        'key': 'Key 2'
                    }]
                }
            }
        };
    }
});
