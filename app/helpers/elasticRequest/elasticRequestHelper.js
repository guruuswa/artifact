(function () {
    angular
        .module('appHelperElasticRequest')
        .service('elasticRequestHelper', service);

    function service($q, $http, configApiEndpoint) {
        return function (namespace, mappingFn, options) {
            let dataCache, promise;

            options = angular.extend({
                method: 'POST',
                endpoint: configApiEndpoint,
                cache: false,
                isArray: false
            }, options);

            return {
                query: (data) => {
                    let deferred = $q.defer();
                    let result = options.isArray ? [] : {};

                    if (options.cache && promise) {
                        return promise;
                    }

                    if (!(options.cache && dataCache)) {
                        promise = deferred.promise;

                        $http({
                            method: options.method,
                            url: options.endpoint,
                            data: data,
                            headers: {
                                Namespace: namespace
                            }
                        }).then(response => {
                            dataCache = mappingFn ? mappingFn(response.data) : response.data;
                            angular.copy(dataCache, result);
                            deferred.resolve(dataCache);
                        }).catch(response => {
                            deferred.reject(response.data);
                        }).finally(() => {
                            promise = undefined;
                        });
                    } else {
                        angular.copy(dataCache, result);
                        deferred.resolve(dataCache);
                    }

                    deferred.promise.result = result;

                    return deferred.promise;
                },
                clearCache: () => {
                    dataCache = undefined;
                    return dataCache;
                }
            };
        };
    }
})();