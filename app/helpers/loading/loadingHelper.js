(function() {
    angular
        .module('appHelperLoading')
        .factory('loadingHelper', factory);

    //NOTE: This needs to be in a factory to retain the data between calls
    function factory() {
        let loadingTracker = {};

        /*
		We need to cater for the following scenario:
		Component 1: Start loading
		Component 2: Start loading
		Component 1: Stop loading

		The indicator should still show since Component 2 is still busy loading. Only when each of them have called "stop", should
		the loading indicator be removed
        */

        const GLOBAL = 'Global';
        const ALL = 'ALL';

        let helper = {
            get: (target) => {
                loadingTracker = angular.extend(loadingTracker);
                loadingTracker[target] = loadingTracker[target] || {};
                return loadingTracker[target];
            },
            start: (name, target) => {
                loadingTracker = angular.extend(loadingTracker);
                loadingTracker[target] = loadingTracker[target] || {};
                loadingTracker[target][name] = true;
            },
            startFullPage: () => {
                loadingTracker = angular.extend(loadingTracker);
                loadingTracker[ALL] = loadingTracker[ALL] || {};
                loadingTracker[ALL][GLOBAL] = true;
            },
            hasFullPage: () => {
                loadingTracker = angular.extend(loadingTracker);

                for (let name in loadingTracker[ALL]) {
                    if (loadingTracker[ALL][name] === true) {
                        return true;
                    }
                }

                return false;
            },
            stop: (name, target) => {
                delete loadingTracker[target][name];

                if (!helper.any() && !!loadingTracker && !!loadingTracker[ALL]) {
                    delete loadingTracker[ALL][GLOBAL];
                }
            },
            any: () => {
                loadingTracker = angular.extend(loadingTracker);
                for (let target in loadingTracker) {
                    if (target !== ALL) {
                        for (let name in loadingTracker[target]) {
                            if (loadingTracker[target][name] === true) {
                                return true;
                            }
                        }
                    }
                }

                return false;
            }
        };

        return helper;
    }
})();