describe('modelHelper', function() {
    var modelHelper;
    var modelHelperInstance;

    beforeEach(function() {
        inject(['modelHelper',
            function(_modelHelper_) {
                modelHelper = _modelHelper_;
            }
        ]);
    });


    describe('modelHelper instance', function() {
        var dataModel = {
            id: 0,
            name: 'model name',
            nested: {
                id: 1,
                name: 'nested name'
            }
        };

        it('can create a new instance', function() {
            modelHelperInstance = new modelHelper(dataModel);
            expect(typeof modelHelperInstance).toEqual('object');
        });
    });


    describe('modelHelper get', function() {
        it('returns data model', function() {
            expect(modelHelperInstance.get().id).toBe(0);
            expect(modelHelperInstance.get().name).toBe('model name');
            expect(modelHelperInstance.get().nested.id).toBe(1);
            expect(modelHelperInstance.get().nested.name).toBe('nested name');
        });
    });


    describe('modelHelper set', function() {
        var newData = {
            name: 'new model name',
            nested: {
                name: 'new nested name'
            }
        };

        it('can set data on the model', function() {
            modelHelperInstance.set(newData);
            expect(modelHelperInstance.get().id).toBe(0);
            expect(modelHelperInstance.get().name).toBe('new model name');
            expect(modelHelperInstance.get().nested.id).toBe(1);
            expect(modelHelperInstance.get().nested.name).toBe('new nested name');
        });
    });


    describe('modelHelper map', function() {
        var mapperName = 'test';
        var mapperMap = {
            name: 'a.nested.name.field',
            nested: {
                name: 'anotherName'
            }
        };
        var objectToMap = {
            a: {
                nested: {
                    name: {
                        field: 'mapped name'
                    }
                }
            },
            anotherName: 'mapped nested name'
        };

        it('can add a mapper', function() {
            modelHelperInstance.addMapper(mapperName, mapperMap);
            expect(typeof modelHelperInstance._mappers[mapperName]).toEqual('object');
        });

        it('can map the data to the new structure', function() {
            modelHelperInstance.map(mapperName, objectToMap);
            expect(modelHelperInstance.get().id).toBe(0);
            expect(modelHelperInstance.get().name).toBe('mapped name');
            expect(modelHelperInstance.get().nested.id).toBe(1);
            expect(modelHelperInstance.get().nested.name).toBe('mapped nested name');
        });
    });
});