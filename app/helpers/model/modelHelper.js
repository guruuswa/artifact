(function() {
    angular
        .module('appHelperModel')
        .service('modelHelper', service);

    function service() {
        var Model = function(modelData) {
            angular.forEach(modelData, function(value, key) {
                this[key] = value;
            }, this);
        };

        /* PUBLIC */

        Model.prototype.get = function() {
            return this;
        };

        Model.prototype.set = function(data) {
            var vm = this;
            if (angular.isDefined(data)) {
                vm = setNested(this, data);
            }
            return this.get();
        };

        Model.prototype._mappers = {};

        Model.prototype.addMapper = function(name, map) {
            this._mappers[name] = map;
        };

        Model.prototype.map = function(mapper, data) {
            var vm = this;
            if (angular.isDefined(data)) {
                vm = mapNested(this._mappers[mapper], this, data);
            }
            return this.get();
        };

        /* PRIVATE */

        function setNested(sourceObj, newObject) {
            if (angular.isDefined(newObject)) {
                for (let key in sourceObj) {
                    if (!sourceObj.hasOwnProperty(key)) {
                        continue;
                    }
                    if (!newObject.hasOwnProperty(key)) {
                        continue;
                    }
                    if (angular.isObject(sourceObj[key]) && !angular.isArray(sourceObj[key])) {
                        sourceObj[key] = setNested(sourceObj[key], newObject[key]);
                    } else {
                        sourceObj[key] = newObject[key];
                    }
                }
            }
            return sourceObj;
        }

        function mapNested(mapper, sourceObj, newObject) {
            if (angular.isDefined(newObject)) {
                for (let key in sourceObj) {
                    if (!sourceObj.hasOwnProperty(key)) {
                        continue;
                    }
                    if (!mapper.hasOwnProperty(key)) {
                        continue;
                    }
                    if (angular.isObject(sourceObj[key]) && !angular.isArray(sourceObj[key])) {
                        sourceObj[key] = mapNested(mapper[key], sourceObj[key], newObject);
                    } else {
                        sourceObj[key] = resolveObjectValue(newObject, mapper[key].split('.'));
                    }
                }
            }
            return sourceObj;
        }

        function resolveObjectValue(referenceObj, objStringArray, index) {
            if (angular.isUndefined(index)) {
                index = 0;
            }
            if (index === objStringArray.length) {
                return referenceObj;
            }
            return resolveObjectValue(referenceObj[objStringArray[index]], objStringArray, (index + 1));
        }

        return Model;
    }
})();