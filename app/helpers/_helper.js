(function () {
    angular.module('appHelper', [
        'appHelperModel',
        'appHelperLoading',
        'appHelperEvent',
        'appHelperFilters',
        'appHelperElasticRequest'
    ]);
})();