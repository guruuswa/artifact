(function() {
    angular.module('appView', [
        'appViewPortal'
    ])
    .config(config);

    function config($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/portal/overview.html'
            })
            .when('/your-sales', {
                templateUrl: 'views/portal/sales.html'
            })
            .when('/your-customers', {
                templateUrl: 'views/portal/customers.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    }
})();
