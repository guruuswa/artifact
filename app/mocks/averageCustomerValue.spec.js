const MOCK_AVERAGE_CUSTOMER_VALUE = {
    "aggregations": {
        "by_average": {
            "buckets": [
                {
                    "key": "filter_by",
                    "doc_count": 3185,
                    "value": 798.685836734694
                },
                {
                    "key": "compare_to",
                    "doc_count": 235,
                    "value": 688.9410212765958
                }
            ]
        }
    }
};