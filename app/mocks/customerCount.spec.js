const MOCK_CUSTOMER_COUNT = {
    aggregations: {
        customers: {
            buckets: [{
                key: 'ARCHITECTIVE PUBLICATIONS',
                doc_count: 1025
            }, {
                key: 'BELLEZZA',
                doc_count: 2741
            }]
        }
    }
};