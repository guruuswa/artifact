const MOCK_MERCHANTS = {
    'took': 1922,
    'timed_out': false,
    '_shards': {
        'total': 45,
        'successful': 45,
        'failed': 0
    },
    'hits': {
        'total': 20596258,
        'max_score': 0,
        'hits': []
    },
    'aggregations': {
        'merchants': {
            'doc_count_error_upper_bound': 0,
            'sum_other_doc_count': 1910935,
            'buckets': [{
                'key': ' ARCHITECTIVE PUBLICATIONS',
                'doc_count': 1,
                'merchantNumberAndValue': {
                    'doc_count_error_upper_bound': 0,
                    'sum_other_doc_count': 0,
                    'buckets': [{
                        'key': '3480828',
                        'doc_count': 1,
                        'averageValueForMerchant': {
                            'value': 993
                        }
                    }]
                }
            }, {
                'key': ' BELLEZZA',
                'doc_count': 2,
                'merchantNumberAndValue': {
                    'doc_count_error_upper_bound': 0,
                    'sum_other_doc_count': 0,
                    'buckets': [{
                        'key': '3311554',
                        'doc_count': 2,
                        'averageValueForMerchant': {
                            'value': 62.5
                        }
                    }]
                }
            }]
        }
    }
};