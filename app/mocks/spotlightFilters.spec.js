const MOCK_SPOTLIGHT_FILTERS = [{
    'id': 0,
    'title': 'Age:',
    'defaultLabel': 'Filter by age',
    'name': 'ageFilter',
    'options': [{
        'value': '',
        'label': 'Show all'
    }, {
        'value': '0,17',
        'label': '17 and below'
    }, {
        'value': '18,24',
        'label': '18 - 24'
    }, {
        'value': '25,32',
        'label': '25 - 32'
    }, {
        'value': '33,40',
        'label': '33 - 40'
    }, {
        'value': '41,60',
        'label': '41 - 60'
    }, {
        'value': '61',
        'label': '61 and above'
    }]
}, {
    'id': 1,
    'title': 'Race:',
    'defaultLabel': 'Filter by race',
    'name': 'raceFilter',
    'options': [{
        'value': '',
        'label': 'Show all'
    }, {
        'value': 'B',
        'label': 'African'
    }, {
        'value': 'A',
        'label': 'Asian'
    }, {
        'value': 'C',
        'label': 'Coloured'
    }, {
        'value': 'W',
        'label': 'White'
    }, {
        'value': 'U',
        'label': 'Unknown'
    }, {
        'value': 'N',
        'label': 'Not Applicable'
    }]
}, {
    'id': 2,
    'title': 'Gender:',
    'defaultLabel': 'Filter by gender',
    'name': 'genderFilter',
    'options': [{
        'value': '',
        'label': 'Show all'
    }, {
        'value': 'M',
        'label': 'Male'
    }, {
        'value': 'F',
        'label': 'Female'
    }]
}];
