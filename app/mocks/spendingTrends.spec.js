const MOCK_SPENDING_TRENDS = {
    'took': 1922,
    'timed_out': false,
    '_shards': {
        'total': 45,
        'successful': 45,
        'failed': 0
    },
    'hits': {
        'total': 20596258,
        'max_score': 0,
        'hits': [ ]
    },
    'aggregations': {
        'baskets': {
            'buckets': [
                {
                    'key': 225,
                    'doc_count': 6300
                },
                {
                    'key': 302,
                    'doc_count': 512
                },
                {
                    'key': 395,
                    'doc_count': 589
                },
                {
                    'key': 461,
                    'doc_count': 4042
                },
                {
                    'key': 508,
                    'doc_count': 448
                },
                {
                    'key': 575,
                    'doc_count': 3177
                },
                {
                    'key': 624,
                    'doc_count': 6173
                },
                {
                    'key': 722,
                    'doc_count': 4823
                }, 
                {
                    'key': 861,
                    'doc_count': 6713
                }
            ]
        }
    }
};