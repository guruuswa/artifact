const MOCK_SPEND_RANGE = {
    took: 59,
    timed_out: false,
    _shards: {
        total: 35,
        successful: 35,
        failed: 0
    },
    hits: {
        total: 904404,
        max_score: 0.0,
        hits: []
    },
    aggregations: {
        '2': {
            buckets: {
                '0.0-50.0': {
                    from: 0.0,
                    from_as_string: '0.0',
                    to: 50.0,
                    to_as_string: '50.0',
                    doc_count: 86740,
                    '1': {
                        value: 123741
                    }
                },
                '50.0-100.0': {
                    from: 50.0,
                    from_as_string: '50.0',
                    to: 100.0,
                    to_as_string: '100.0',
                    doc_count: 97356,
                    '1': {
                        value: 456258
                    }
                },
                '100.0-200.0': {
                    from: 100.0,
                    from_as_string: '100.0',
                    to: 200.0,
                    to_as_string: '200.0',
                    doc_count: 157597,
                    '1': {
                        value: 789963
                    }
                },
                '200.0-500.0': {
                    from: 200.0,
                    from_as_string: '200.0',
                    to: 500.0,
                    to_as_string: '500.0',
                    doc_count: 261829,
                    '1': {
                        value: 123456
                    }
                },
                '500.0-1000.0': {
                    from: 500.0,
                    from_as_string: '500.0',
                    to: 1000.0,
                    to_as_string: '1000.0',
                    doc_count: 184869,
                    '1': {
                        value: 123789
                    }
                },
                '1000.0-*': {
                    from: 1000.0,
                    from_as_string: '1000.0',
                    doc_count: 114372,
                    '1': {
                        value: 456321
                    }
                }
            }
        },
        '3': {
            'value': 3000000
        }
    }
};