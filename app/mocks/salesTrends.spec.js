const MOCK_SALES_TRENDS = {
    'took': 1922,
    'timed_out': false,
    '_shards': {
        'total': 45,
        'successful': 45,
        'failed': 0
    },
    'hits': {
        'total': 20596258,
        'max_score': 0,
        'hits': [ ]
    },
    'aggregations': {
        'salesTrends': {
            'buckets': [
                                {
                    'key_as_string': 'Monday',
                    'ranges': {
                        'buckets': [
                            {
                                'key': 'filter_by',
                                'doc_count': 0
                            },{
                                'key': 'compare_to',
                                'doc_count': 23904
                            }
                        ]
                    }
                },
                {
                    'key_as_string': 'Tuesday',
                    'ranges': {
                        'buckets': [
                            {
                                'key': 'filter_by',
                                'doc_count': 1600336
                            },{
                                'key': 'compare_to',
                                'doc_count': 345987
                            }
                        ]
                    }
                },
                {
                    'key_as_string': 'Wednesday',
                    'ranges': {
                        'buckets': [
                            {
                                'key': 'filter_by',
                                'doc_count': 629608
                            },{
                                'key': 'compare_to',
                                'doc_count': 901256
                            }
                        ]
                    }
                },
                {
                    'key_as_string': 'Thursday',
                    'ranges': {
                        'buckets': [
                            {
                                'key': 'filter_by',
                                'doc_count': 34824
                            },{
                                'key': 'compare_to',
                                'doc_count': 1484678
                            }
                        ]
                    }
                },
                {
                    'key_as_string': 'Friday',
                    'ranges': {
                        'buckets': [
                            {
                                'key': 'filter_by',
                                'doc_count': 1223278
                            },{
                                'key': 'compare_to',
                                'doc_count': 251896
                            }
                        ]
                    }
                },
                {
                    'key_as_string': 'Saturday',
                    'ranges': {
                        'buckets': [
                            {
                                'key': 'filter_by',
                                'doc_count': 419508
                            },{
                                'key': 'compare_to',
                                'doc_count': 190827
                            }
                        ]
                    }
                },
                {
                    'key_as_string': 'Sunday',
                    'ranges': {
                        'buckets': [
                            {
                                'key': 'filter_by',
                                'doc_count': 1897064
                            },{
                                'key': 'compare_to',
                                'doc_count': 1908765
                            }
                        ]
                    }
                }
            ]
        }
    }
};