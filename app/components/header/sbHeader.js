(function() {
    angular
        .module('appComponentHeader')
        .directive('sbHeader', directive);

    function directive($window, userFactory, sidebarFactory, loadingHelper) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            templateUrl: 'components/header/sbHeader.html',
            link: link,
        };

        function link(scope) {
            angular.extend(scope, {
                signoutOpen: false,
                openSignOut: openSignOut,
                signoutCancel: signoutCancel,
                signoutSignout: signoutSignout,
                toggleSidebar: toggleSidebar,
                redirect: redirect
            });

            function openSignOut() {
                scope.signoutOpen = true;
            }

            function signoutCancel() {
                scope.signoutOpen = false;
            }

            function signoutSignout() {
                scope.signoutOpen = false;
                redirect('https://merchantonline.standardbank.co.za/MerchantPortal/MerchantPortal/MerchAuthentication/Login');
                loadingHelper.start('logout'); // No stop: overlay will be shown until location.href goes through
            }

            function toggleSidebar() {
                sidebarFactory.toggle();
            }

            function redirect(url) {
                $window.location.href = url;
            }
        }
    }
})();