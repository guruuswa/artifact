describe('sbHeader', () => {
    let $compile, $scope, $window, element, elementScope, sidebarFactory;

    beforeEach(module(function($provide) {
        $window = {
            location: {},
            document: window.document
        };

        $provide.constant('$window', $window);
    }));


    beforeEach(inject((_$compile_, _$rootScope_, _sidebarFactory_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();
        sidebarFactory = _sidebarFactory_;

        element = angular.element('<sb-header></sb-header>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));


    describe('sbHeader created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-header')).toBeGreaterThan(-1);
        });

        it('displays modal on signout click', () => {
            elementScope.openSignOut();
            expect(elementScope.signoutOpen).toBe(true);
        });

        it('hides modal on cancel click', () => {
            elementScope.openSignOut();
            elementScope.signoutCancel();
            expect(elementScope.signoutOpen).toBe(false);
        });

        it('signs out on signout click', () => {
            elementScope.openSignOut();
            elementScope.signoutSignout();
            expect($window.location.href).toBe('https://merchantonline.standardbank.co.za/MerchantPortal/MerchantPortal/MerchAuthentication/Login');
        });

        it('toggles sidebar', () => {
            elementScope.toggleSidebar();
            expect(sidebarFactory.get().isOpen).toBeTruthy();
        });
    });
});