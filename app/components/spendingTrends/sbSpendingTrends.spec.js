describe('sbSpendingTrends', () => {
  let $compile, $scope, element, elementScope;

beforeEach(inject((_$compile_, _$rootScope_, _$httpBackend_) => {
    $compile = _$compile_;
    $scope = _$rootScope_.$new();

    element = angular.element('<sb-spending-trends></sb-spending-trends>');
    $compile(element)($scope);
    $scope.$digest();

    elementScope = element.isolateScope() || element.scope();
  }));


  describe('sbSpendingTrends created', () => {
    it('has element', () => {
      expect(element).toBeDefined();
      expect(element[0].className.indexOf('sb-spendingTrends')).toBeGreaterThan(-1);
    });
  });
});