(function () {
    angular
        .module('appComponentSpendingTrends')
        .directive('sbSpendingTrends', directive);

    function directive($filter, $location, merchantsFactory, loadingHelper) {//, filtersFactory) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            templateUrl: 'components/spendingTrends/sbSpendingTrends.html',
            controller: controller,
            controllerAs: 'sbSpendingTrendsCtrl',
            link: link
        };

        function controller() {
            // controller logic
        }

        function link() {//scope) {
            //angular.extend(scope, {
            //    merchantList: merchantsFactory.getWithAllStores().result,
            //    activeMerchant: merchantsFactory.getActive(),
            //    chartData: [],
            //    chartColors: {
            //        linePalette: [
            //            '#2b66cf',
            //            '#cccccc'
            //        ]
            //    },
            //    filterType: false,
            //    filterStores: [],
            //    showFilterStores: false,
            //    formatAmount: formatAmount,
            //    refreshFilters: refreshFilters,
            //    selectFilterStore: selectFilterStore,
            //    noneSelected: false
            //});

            reload();

            //merchantsFactory.getWithAllStores().then(data => {
            //    scope.filterStores = data.map(x => {
            //        return {
            //            value: x.id,
            //            label: x.name,
            //            isActive: true
            //        };
            //    });
            //    reload();
            //});

            // function formatAmount(amount) {
            //     let symbol = !scope.filterType ? 'R ' : '';
            //     return $filter('filterCurrency')(amount, symbol, 0);
            // }

            // function refreshFilters() {
            //     reload();
            // }

            //function selectFilterStore(index) {
                //let activeState = scope.filterStores[index].isActive;
                //if (index === 0) {
                //    scope.filterStores.map(x => {
                //        x.isActive = activeState || (activeState === false && x.value === scope.activeMerchant.id && scope.activeMerchant.id !== null);
                //        return x;
                //    });
                //} else {
                //    scope.filterStores[0].isActive = angular.copy(scope.filterStores).filter(x => x.value !== null).every(x => x.isActive);
                //}

                //scope.noneSelected = scope.filterStores.every(x => !x.isActive);
            //}

            function reload() {
                const LOADING_DATA = 'sbSpendingTrends';
                const LOADING_DATA_TARGET = 'spendingTrends';
                let selectedFilterStores = '';//scope.filterStores.filter(x => x.isActive && x.value !== null).map(x => x.value);

                loadingHelper.start(LOADING_DATA, LOADING_DATA_TARGET);

                //if (!selectedFilterStores.length) {
                //    getSpendingTrendsData(scope.activeMerchant.id);
                //} else {
                    getSpendingTrendsData(selectedFilterStores);
                //}

                function getSpendingTrendsData() {//selectedFilterStore) {
                    //let ageObject = {};
                    //if(!!filtersFactory.getActive().age)
                    //{
                    //    let ageArray = filtersFactory.getActive().age.value.split(',');
                    //    if(ageArray.length) {
                    //        ageObject.from = ageArray[0];
                    //        if(!!ageArray[1]) {
                    //            ageObject.to = ageArray[1];
                    //        }
                    //    }
                    //}

                    //let filter = merchantSalesFilterModel({
                    //    storeId: selectedFilterStore,
                    //    date: [filtersFactory.getActive().dateRange, filtersFactory.getActive().compareDateRange],
                    //    age: ageObject,
                    //    race: filtersFactory.getActive().race,
                    //    gender: filtersFactory.getActive().gender,
                    //    byVolume: scope.filterType
                    //});

                    //let promise = merchantSalesFactory.get(filter);
                    //let hasCompare = false;

                    //promise.then((data) => {
                    //    scope.chartData = data.map(x => {
                    //        let valueRange;
                    //        if(x.shouldCompare) {
                    //            valueRange = [x.value, x.valueCompare];
                    //            hasCompare = true;
                    //        } else {
                    //            valueRange = [x.value];
                    //        }
                    //        return {
                    //            value: valueRange,
                    //            label: x.id + '|' + x.merchant //pipe char splits the words into lines in sbChartBar.js
                    //        };
                    //    });
                    //    scope.chartData.IsData = scope.chartData.filter(e => e.value.filter(f => f > 0).length).length;
                    //    scope.chartData.currentDateFilter = filtersFactory.getActive().dateRange.dateDescription;
                    //    scope.chartData.compareDateFilter = filtersFactory.getActive().compareDateRange.dateDescription;
                    //    scope.chartData.hasCompare = hasCompare;
                    //}).catch((reason) => {
                    //    scope.messages = !!scope.messages ? scope.messages : [];
                    //    scope.messages.push('An error occurred during the operation: ' + reason);
                    //}).finally(() => {
                    //    loadingHelper.stop(LOADING_DATA, LOADING_DATA_TARGET);
                    //});
                }
            }
        }
    }
})();
