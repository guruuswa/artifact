(function () {
    angular
        .module('appComponentCustomerCount')
        .directive('sbCustomerCount', directive);

    function directive($q, loadingHelper, merchantsFactory, customerCountFactory, filtersFactory) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            templateUrl: 'components/customerCount/sbCustomerCount.html',
            link: link
        };

        function link(scope) {
            angular.extend(scope, {
                apiData: {
                    main: undefined,
                    compare: undefined
                },
                count: {
                    main: 0,
                    compare: 0
                },
                activeFilters: undefined
            });

            merchantsFactory.activeUpdate.subscribe(scope, reload);
            customerCountFactory.onUpdate.subscribe(scope, reload);

            reload();

            function reload() {
                const LOADING_DATA = 'sbCustomerCount';
                const LOADING_DATA_TARGET = 'customerCount';

                let promise, promiseCompare;

                loadingHelper.start(LOADING_DATA, LOADING_DATA_TARGET);

                scope.activeFilters = filtersFactory.getActive();

                promise = customerCountFactory.get();
                if (scope.activeFilters.compareDateRange.dateFrom && scope.activeFilters.compareDateRange.dateTo) {
                    promiseCompare = customerCountFactory.get();
                }

                $q.all([promise, promiseCompare]).then((data) => {
                    scope.apiData.main = data[0];
                    scope.apiData.compare = data[1];
                    scope.count.main = getCount(data[0]);
                    scope.count.compare = getCount(data[1]);
                }).finally(() => {
                    loadingHelper.stop(LOADING_DATA, LOADING_DATA_TARGET);
                });

                function getCount(array) {
                    if (!(array && array.length)) {
                        return undefined;
                    }
                    return array.reduce((a, b) => a + b.value, 0);
                }
            }
        }
    }
})();
