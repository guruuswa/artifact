describe('sbCustomerCount', () => {
    let $compile, $scope, element, elementScope, $httpBackend, customerCountArguments, filtersFactory, merchantsFactory;

    beforeEach(inject((_$compile_, _$rootScope_, _$httpBackend_, _filtersFactory_, _merchantsFactory_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();
        $httpBackend = _$httpBackend_;
        filtersFactory = _filtersFactory_;
        merchantsFactory = _merchantsFactory_;

        customerCountArguments = ['GET', 'mocks/customerCount.json', undefined, (headers) => headers.Namespace === 'customerCount'];
        $httpBackend.when.apply(null, customerCountArguments).respond(MOCK_CUSTOMER_COUNT);

        element = angular.element('<sb-customer-count></sb-customer-count>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    describe('sbCustomerCount created', () => {
        it('has element', () => {
            $httpBackend.flush();
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-customerCount')).toBeGreaterThan(-1);
        });
    });

    describe('set count values', () => {
        it('sets counts when no compare date', () => {
            $httpBackend.flush();
            expect(elementScope.apiData.main).toBeDefined();
            expect(elementScope.apiData.compare).toBeUndefined();
            expect(elementScope.count.main).toBe(3766);
            expect(elementScope.count.compare).toBeUndefined();
        });

        it('sets counts when compare date is set', () => {
            filtersFactory.setActive({dateFrom: '01-02-2016', dateTo: '01-03-2016'}, {dateFrom: '01-01-2016', dateTo: '01-02-2016'});
            merchantsFactory.activeUpdate.broadcast();
            $httpBackend.flush();
            expect(elementScope.apiData.main).toBeDefined();
            expect(elementScope.apiData.compare).toBeDefined();
            expect(elementScope.count.main).toBe(3766);
            expect(elementScope.count.compare).toBe(3766);
        });
    });
});
