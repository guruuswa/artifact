(function () {
    angular
        .module('appComponentGoogleMap')
        .factory('googleMapFactory', factory);

    function factory() {
        /* jshint ignore:start */

        let map;
        let markers = [];
        let latlngbounds;

        function createMap(elementId){
            map = new google.maps.Map(document.getElementById(elementId), {
                scrollwheel: true,
                zoomControl: true,
                center: {lat: -26, lng: 28},
                zoom: 4,
                maxZoom: 9
            });

            latlngbounds = new google.maps.LatLngBounds();
        }
        
        function addMarker(lat, lng, icon,  pinName) {
            let marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng (lat,lng),
                title: pinName,
                icon: icon
            });

            latlngbounds.extend(new google.maps.LatLng (lat,lng));
            map.setCenter(latlngbounds.getCenter());
            map.fitBounds(latlngbounds);

            markers.push(marker);

            return marker;
        }

        function addHeatMap(locations){

            let heatMapPoints = [];
            for (let i = 0; i < locations.length; i++) {
                heatMapPoints.push({
                    location: new google.maps.LatLng(locations[i].lat, locations[i].lon),
                    weight: locations[i].weight
                });
            }

            let heatmap = new google.maps.visualization.HeatmapLayer({
                data: heatMapPoints,
                map: map,
                maxIntensity: locations[0].maxIntensity/2,
            });

            map.setCenter(latlngbounds.getCenter());
            map.fitBounds(latlngbounds);

            return heatmap;
        }

        return {
            createMap: createMap,
            addMarker: addMarker,
            addHeatMap: addHeatMap
        };

        /* jshint ignore:end */
    }
})();
