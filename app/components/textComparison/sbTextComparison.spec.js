describe('sbTextComparison', () => {
    let $compile, $scope, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        element = angular.element('<sb-text-comparison></sb-text-comparison>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));


    describe('sbTextComparison created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-textComparison')).toBeGreaterThan(-1);
        });
    });
});