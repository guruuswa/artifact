(function () {
    angular
        .module('appComponentTextComparison')
        .directive('sbTextComparison', directive);

    function directive() {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                value: '=textComparisonValue',
                description: '=textComparisonDescription',
                valueCompare: '=textComparisonValueCompare',
                descriptionCompare: '=textComparisonDescriptionCompare'
            },
            templateUrl: 'components/textComparison/sbTextComparison.html'
        };
    }
})();
