(function () {
    angular
        .module('appComponentTopPerformingStore')
        .directive('sbTopPerformingStore', directive);

    function directive(topPerformingStoresFactory, merchantsFactory, loadingHelper) {
        return {
            scope: {},
            restrict: 'E',
            replace: true,
            templateUrl: 'components/topPerformingStore/sbTopPerformingStore.html',
            link: link
        };

        function link(scope, element) {
            scope.topPerformingStore = {};
            scope.getData = getData;
            scope.refresh = refresh;

            const LOADING_DATA = 'topPerformingStore';
            const LOADING_DATA_TARGET = 'topPerformingStore';

            element.on('load', scope.getData(merchantsFactory.getActive()));

            function getData(activeMerchant) {
                loadingHelper.start(LOADING_DATA, LOADING_DATA_TARGET);
                let promise = topPerformingStoresFactory.getData(activeMerchant);
                promise.then(function (result) {
                    scope.topPerformingStore = result;

                    //truncate store name if it is too long
                    let topStoreName = scope.topPerformingStore.topStoreName;
                    if(topStoreName.length > 19){
                        scope.topPerformingStore.topStoreName = topStoreName.substring(0, 17) + '...';
                    }
                }).finally(() => {
                    loadingHelper.stop(LOADING_DATA, LOADING_DATA_TARGET);
                });
                return promise;
            }

            function refresh() {
                scope.activeMerchant = merchantsFactory.getActive();
                getData(scope.activeMerchant);
            }

            merchantsFactory.activeUpdate.subscribe(scope, refresh);
        }
    }
})();