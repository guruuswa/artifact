describe('sbTopPerformingStore', () => {
    const TOP_PERFORMIN_STORE_REQUEST = '/_search';
    const TOP_PERFORMIN_STORE_REQUEST_TEST = RegExp(TOP_PERFORMIN_STORE_REQUEST, 'i');
    let $compile, $scope, element, elementScope, $httpBackend, factory, $q;

    beforeEach(inject((_$compile_, _$rootScope_, _$httpBackend_, _topPerformingStoresFactory_, _$q_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();
        $httpBackend = _$httpBackend_;
        factory = _topPerformingStoresFactory_;
        $q = _$q_;

        element = angular.element('<sb-top-performing-store></sb-top-performing-store>');
        $compile(element)($scope);
        $httpBackend.when('POST', TOP_PERFORMIN_STORE_REQUEST_TEST).respond();

        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    it('has element', () => {
        expect(element).toBeDefined();
        expect(element[0].className.indexOf('sb-topPerformingStore')).toBeGreaterThan(-1);
    });

    it('should set the topPerformingStoreObject when getData is called', () => {
        spyOn(factory, 'getData').and.returnValue($q.when({name: 'test'}));

        expect(elementScope.topPerformingStore).toEqual({});
        var promise = elementScope.getData();
        promise.then(function () {
            expect(elementScope.topPerformingStore).toEqual({name: 'test'});
        });
    });

});