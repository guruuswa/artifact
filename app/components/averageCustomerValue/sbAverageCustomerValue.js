(function () {
    angular
        .module('appComponentAverageCustomerValue')
        .directive('sbAverageCustomerValue', directive);

    function directive(loadingHelper, filtersFactory, averageCustomerValueFactory, merchantsFactory) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            templateUrl: 'components/averageCustomerValue/sbAverageCustomerValue.html',
            link: link
        };

        function link(scope) {
            angular.extend(scope, {
                apiData: undefined,
                activeFilters: undefined
            });

            merchantsFactory.activeUpdate.subscribe(scope, setData);
            averageCustomerValueFactory.onUpdate.subscribe(scope, setData);

            setData();

            function setData() {
                const LOADING_DATA = 'sbAverageCustomerValue';
                const LOADING_DATA_TARGET = 'averageCustomerValue';

                loadingHelper.start(LOADING_DATA, LOADING_DATA_TARGET);

                scope.activeFilters =  filtersFactory.getActive();
                averageCustomerValueFactory.get().then((data) => {
                    scope.apiData = data;
                }).finally(() => {
                    loadingHelper.stop(LOADING_DATA, LOADING_DATA_TARGET);
                });
            }
        }
    }
})();
