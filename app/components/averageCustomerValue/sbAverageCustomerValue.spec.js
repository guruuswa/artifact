describe('sbAverageCustomerValue', () => {
    let $compile, $scope, element, elementScope, $httpBackend, averageCustomerValueArguments, filtersFactory, merchantsFactory;

    beforeEach(inject((_$compile_, _$rootScope_, _$httpBackend_, _filtersFactory_, _merchantsFactory_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();
        $httpBackend = _$httpBackend_;
        filtersFactory = _filtersFactory_;
        merchantsFactory = _merchantsFactory_;

        averageCustomerValueArguments = ['GET', 'mocks/averageCustomerValue.json', undefined, (headers) => headers.Namespace === 'averageCustomerValue'];
        $httpBackend.when.apply(null, averageCustomerValueArguments).respond(MOCK_AVERAGE_CUSTOMER_VALUE);

        element = angular.element('<sb-average-customer-value></sb-average-customer-value>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    describe('sbAverageCustomerValue created', () => {
        it('has element', () => {
            $httpBackend.flush();
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-averageCustomerValue')).toBeGreaterThan(-1);
        });
    });

    describe('sets data', () => {
        it('sets data on load', () => {
            $httpBackend.flush();
            expect(elementScope.apiData.length).toBe(2);
            expect(elementScope.apiData[0].value).toBe(798.685836734694);
            expect(elementScope.apiData[1].value).toBe(688.9410212765958);
        });

        it('on filters update', () => {
            filtersFactory.setActive({dateFrom: '01-02-2016', dateTo: '01-03-2016'}, {dateFrom: '01-01-2016', dateTo: '01-02-2016'});
            merchantsFactory.activeUpdate.broadcast();
            $httpBackend.flush();
            expect(elementScope.apiData.length).toBe(2);
            expect(elementScope.apiData[0].value).toBe(798.685836734694);
            expect(elementScope.apiData[1].value).toBe(688.9410212765958);
        });
    });
});