(function() {
    angular
        .module('appComponentSpotlight')
        .directive('sbSpotlight', directive);

    function directive(merchantsFactory, filtersFactory, loadingHelper) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            templateUrl: 'components/spotlight/sbSpotlight.html',
            controller: controller,
            controllerAs: 'sbSpotlightCtrl'
        };

        function controller() {
            this.refresh = () => {
                loadingHelper.startFullPage();
                merchantsFactory.activeUpdate.broadcast();
            };
        }
    }
})();