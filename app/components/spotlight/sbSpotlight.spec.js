describe('sbSpotlight', () => {
    const MERCHANTS_REQUEST = '/_search';
    const MERCHANTS_REQUEST_TEST = RegExp(MERCHANTS_REQUEST, 'i');
    let factory, $httpBackend;
    let requestHandler;

    let $compile, $scope, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_, _merchantsFactory_, _$httpBackend_) => {
        factory = _merchantsFactory_;
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend.when('POST', MERCHANTS_REQUEST_TEST).respond(mockFilter());

        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        element = angular.element('<sb-spotlight></sb-spotlight>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    describe('sbSpotlight created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('widget--spotlight')).toBeGreaterThan(-1);
        });
    });

    function mockFilter() {
        return [{
            id: 0,
            title: 'From: DSDSDSDS',
            name: 'dateFromFilter',
            defaultLabel: 'Select date'
        }, {
            id: 1,
            title: 'To:',
            name: 'dateToFilter',
            defaultLabel: 'Select date'
        }, {
            id: 2,
            title: 'Age:',
            defaultLabel: 'Select age',
            name: 'ageFilter',
            options: [{
                name: 'ageFilter',
                from: null,
                to: null,
                title: 'Show all'
            }, {
                name: 'ageFilter',
                title: '17 and below',
                from: null,
                to: 17
            }, {
                name: 'ageFilter',
                title: '18 - 24',
                from: 18,
                to: 25
            }, {
                name: 'ageFilter',
                title: '25 - 32',
                from: 25,
                to: 32
            }, {
                name: 'ageFilter',
                title: '33 - 40',
                from: 33,
                to: 40
            }, {
                name: 'ageFilter',
                title: '41 - 60',
                from: 41,
                to: 60
            }, {
                name: 'ageFilter',
                title: '60 and above',
                from: 60,
                to: null
            }]
        }];
    }
});