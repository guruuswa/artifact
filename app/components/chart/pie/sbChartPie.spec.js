describe('sbPie', () => {
    let $compile, $scope, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        element = angular.element('<sb-chart-pie></sb-chart-pie>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));


    describe('sbPie created', () => {
        it('has element', inject(() => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-chartPie')).toBeGreaterThan(-1);
        }));
    });
});