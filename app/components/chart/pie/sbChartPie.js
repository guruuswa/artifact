(function(d3) {
    angular
        .module('appComponentChart')
        .directive('sbChartPie', directive);

    function directive($timeout) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                height: '@chartHeight',
                data: '=chartData',
                dataCompare: '=chartDataCompare',
                colors: '=chartColors',
                activeIndex: '=chartActiveIndex',
                sliceSelectedCallback: '=chartSliceSelectedCallback'
            },
            link: link,
            templateUrl: 'components/chart/pie/sbChartPie.html'
        };

        function link(scope, element) {
            const SIDEBAR_TRANSITION_DURATION = 300,
                DOUGHNUT_TRANSITION = 1000,
                SPACING = 20; // referenced in CSS

            let elem = element[0],
                svg,
                size,
                innerSize,
                ringWidthOuter,
                ringWidthInner = 18,
                ringActiveSpacing = 3,
                radius,
                colors,
                keys;

            let pie,
                pieInner,
                arc,
                arcInner,
                arcActive,
                arcInnerActive,
                chart,
                chartInner,
                slicesOuter,
                slicesInner,
                slicesVisible;

            d3.select(window).on('resize.' + elem.classList[0], () => {
                $timeout(() => {
                    renderChart();
                }, SIDEBAR_TRANSITION_DURATION);
            });

            scope.$watch('data', newVal => {
                if (newVal) {
                    renderChart();
                }
            });

            scope.$watch('dataCompare', newVal => {
                if (newVal) {
                    renderChart();
                }
            });

            function renderChart() {
                if (!scope.data) {
                    return;
                }

                d3.select(elem).selectAll('*').remove();

                scope.displayCompare = (scope.dataCompare && scope.dataCompare.length);

                if (scope.displayCompare) {
                    ringWidthOuter = 24;
                } else {
                    ringWidthOuter = 36; // referenced in CSS
                }

                size = Math.min(elem.getBoundingClientRect().width, scope.height) - SPACING;
                innerSize = size - ringWidthOuter;
                radius = size / 2;
                colors = d3.scale.ordinal().range(scope.colors);
                keys = scope.data.map(d => d.rangeFrom || 0);
                colors.domain(keys);

                svg = d3.select(elem)
                    .append('svg')
                    .attr({
                        width: size + SPACING,
                        height: size + SPACING
                    });

                pie = d3.layout.pie()
                    .sort(null)
                    .value((data) => data.value);

                pieInner = d3.layout.pie()
                    .sort(null)
                    .value((data) => data.value);

                arc = d3.svg.arc()
                    .outerRadius(radius)
                    .innerRadius(radius - ringWidthOuter);

                arcInner = d3.svg.arc()
                    .outerRadius(radius - ringWidthOuter)
                    .innerRadius(radius - ringWidthOuter - ringWidthInner);

                arcActive = d3.svg.arc()
                    .outerRadius(radius + ringActiveSpacing)
                    .innerRadius(radius - ringWidthOuter + ringActiveSpacing);

                arcInnerActive = d3.svg.arc()
                    .outerRadius(radius - ringWidthOuter - ringActiveSpacing)
                    .innerRadius((radius - ringWidthOuter - ringWidthInner) - ringActiveSpacing);

                chart = svg.append('g')
                    .attr({
                        width: size,
                        height: size
                    })
                    .append('g')
                    .attr('transform', 'translate(' + (size - radius + SPACING / 2) + ',' + (size - radius + SPACING / 2) + ')')
                    .selectAll('path')
                    .data(pie(scope.data))
                    .enter()
                    .append('g')
                    .attr('class', 'slice slice-outer');

                if (scope.displayCompare) {
                    chartInner = svg.append('g')
                        .attr({
                            width: innerSize,
                            height: innerSize
                        })
                        .append('g')
                        .attr('transform', 'translate(' + (size - radius + SPACING / 2) + ',' + (size - radius + SPACING / 2) + ')')
                        .selectAll('path')
                        .data(pie(scope.dataCompare))
                        .enter()
                        .append('g')
                        .attr('class', 'slice slice-inner');
                }

                slicesVisible = d3.selectAll('.slice').filter((x) => {
                    return x.data.value > 0;
                });

                for (let i = 0; i < slicesVisible[0].length; i++) {
                    d3.select(slicesVisible[0][i]).classed('slice-visible', true);
                }

                slicesOuter = svg.selectAll('g.slice-outer')
                    .append('svg:path')
                    .attr({
                        fill: (data) => colors(data.data.rangeFrom || 0),
                        d: arc
                    })
                    .on('click', (d, i) => {
                        d3.selectAll('.slice-outer path')
                            .transition()
                            .duration(DOUGHNUT_TRANSITION)
                            .attr({
                                d: arc
                            });

                        d3.select(d3.selectAll('.slice-outer path')[0][i])
                            .transition()
                            .duration(DOUGHNUT_TRANSITION)
                            .attr({
                                d: arcActive
                            });

                        scope.sliceSelectedCallback(i);
                    });

                slicesInner = d3.selectAll('g.slice-inner')
                    .append('path')
                    .attr({
                        fill: (data) => colors(data.data.rangeFrom || 0),
                        d: arcInner
                    })
                    .on('click', (d, i) => {
                        d3.selectAll('.slice-inner path')
                            .transition()
                            .duration(DOUGHNUT_TRANSITION)
                            .attr({
                                d: arcInner
                            });

                        d3.select(d3.selectAll('.slice-inner path')[0][i])
                            .transition()
                            .duration(DOUGHNUT_TRANSITION)
                            .attr({
                                d: arcInnerActive
                            });

                        scope.sliceSelectedCallback(i);
                    });


                scope.$watch('activeIndex', (newVal) => {

                    d3.selectAll('.slice path')
                        .transition()
                        .duration(DOUGHNUT_TRANSITION)
                        .attr({
                            d: arc
                        });

                    d3.selectAll('.slice-inner path')
                        .transition()
                        .duration(DOUGHNUT_TRANSITION)
                        .attr({
                            d: arcInner
                        });

                    d3.select(slicesOuter[0][newVal])
                        .transition()
                        .duration(DOUGHNUT_TRANSITION)
                        .attr({
                            d: arcActive
                        });

                    d3.select(slicesInner[0][newVal])
                        .transition()
                        .duration(DOUGHNUT_TRANSITION)
                        .attr({
                            d: arcInnerActive
                        });

                });
            }
        }
    }
})(window.d3);