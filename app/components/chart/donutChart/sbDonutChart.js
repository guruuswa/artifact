(function(d3) {
    angular
        .module('appComponentChart')
        .directive('sbDonutChart', directive);

    function directive($timeout) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                height: '@chartHeight',
                data: '=chartData',
                colors: '=chartColors'
            },
            link: link,
            templateUrl: 'components/chart/donutChart/sbDonutChart.html'
        };

        function link(scope, element) {
            const SIDEBAR_TRANSITION_DURATION = 300;
            const RINGWIDTH = 35;

            let elem = element[0],
                svg,
                width,
                height;

            scope.tooltip = {
                isActive: false,
                value: 0,
                label: '',
                x: 0,
                y: 0,
                circleColor: '#fff'
            };

            d3.select(window).on('resize.' + elem.classList[0], () => {
                scope.$apply(() => scope.tooltip.isActive = false);

                $timeout( () => {
                    renderChart();
                }, SIDEBAR_TRANSITION_DURATION);
            });

            scope.$watch('data', data => {
                if (data) {
                    renderChart();
                }
            });

            function renderChart() {
                width = Math.min(elem.getBoundingClientRect().width, scope.height);
                height = Math.min(elem.getBoundingClientRect().width, scope.height);

                d3.select(elem).selectAll('svg').remove();

                svg = d3.select(elem)
                    .append('svg')
                    .attr({
                        width: width,
                        height: height
                    });

                var radius = Math.min(width, height) / 2,
                    pie, arc, chart, slices,
                    colors = d3.scale.ordinal().range(scope.colors.slicePalette);

                if (!scope.data) {
                    return;
                }

                var keys = scope.data.map(function(d) {
                    return d.range[0] || 0;
                });

                colors.domain(keys);

                pie = d3.layout.pie()
                    .sort(null)
                    .value(function(data) {
                        return data.value;
                    });

                arc = d3.svg.arc()
                    .outerRadius(radius)
                    .innerRadius(radius - RINGWIDTH);

                chart = svg.append('g')
                    .attr('width', width)
                    .attr('height', height)
                    .append('g')
                    .attr('transform', 'translate(' + (width - radius) + ',' + (height - radius) + ')')
                    .selectAll('path')
                    .data(pie(scope.data))
                    .enter()
                    .append('g')
                    .attr('class', 'slice');


                slices = d3.selectAll('g.slice')
                    .append('path')
                    .attr('fill', function(data) {
                        return colors(data.data.range[0] || 0);
                    })
                    .attr('d', arc)
                    .on({
                        mouseover: (data) => {
                            scope.$apply(() => {
                                var c = arc.centroid(data);
                                scope.tooltip.isActive = true;
                                scope.tooltip.value = data.data.value;
                                scope.tooltip.label = data.data.label;
                                scope.tooltip.x = c[0] + (width - radius);
                                scope.tooltip.y = c[1] + (height - radius);
                                scope.tooltip.circleColor = data.data.color;
                            });
                        },
                        mouseout: () => {
                            scope.$apply(() => scope.tooltip.isActive = false);
                        }
                    });
            }
        }
    }
})(window.d3);