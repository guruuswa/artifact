describe('sbDonutChart', () => {
    let $compile, $scope, element, elementScope;

    describe('sbDonutChart created', () => {
        beforeEach(inject((_$compile_, _$rootScope_) => {
            $compile = _$compile_;
            $scope = _$rootScope_.$new();

            element = angular.element('<sb-donut-chart></sb-donut-chart>');
            $compile(element)($scope);
            $scope.$digest();

            elementScope = element.isolateScope() || element.scope();
        }));

        it('has element', inject(() => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-donutChart')).toBeGreaterThan(-1);
        }));
    });
});