(function(d3) {
    angular
        .module('appComponentChart')
        .directive('sbCustomGenderBar', directive);

    function directive($timeout) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                data: '=data',
                height: '=chartHeight',
                width: '=chartWidth'
            },
            templateUrl: 'components/chart/customGenderBar/sbCustomGenderBar.html',
            link: link
        };

        function link(scope, element) {
            const SIDEBAR_TRANSITION_DURATION = 300;

            let elem =  element[0],
                        svg,
                        width = scope.width || elem.getBoundingClientRect().width,
                        height = scope.height || elem.getBoundingClientRect().height;

            d3.select(window).on('resize.' + elem.classList[0], () => {
                $timeout( () => {
                    width = elem.getBoundingClientRect().width;
                    renderChart();
                }, SIDEBAR_TRANSITION_DURATION);
            });

            scope.$watch('data', data => {
                if (data) {
                    renderChart();
                }
            });

            function renderChart() {
                let xMax = 100,
                    xScale,
                    yScale;

                d3.select(elem).selectAll('*').remove();

                svg = d3.select(elem)
                    .append('svg')
                    .attr({
                        width: width,
                        height: height
                    })
                    .append('g')
                    .attr({
                        transform: `translate(0,0)`
                    });

                xScale = d3.scale.linear()
                    .domain([0, xMax])
                    .range([0, width]);

                yScale = d3.scale.ordinal()
                    .domain([0, xMax])
                    .rangeRoundBands([0, height], 0.1);

                let myHeight = (scope.data.value/100)*height;

                svg.append('rect')
                     .attr({
                         x: 0,
                         y: 0,
                         width: width,
                         height: height,
                         fill: '#3366cc',
                         opacity: 0.5
                    });

                svg.append('rect')
                    .attr({
                        x: 0,
                        y: height - myHeight,
                        width: width,
                        height: myHeight,
                        fill: '#3366cc'
                    });
            }
        }
    }
})(window.d3);