describe('sbCustomGenderBar', () => {
    let $compile, $scope, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        element = angular.element('<sb-custom-gender-bar></sb-custom-gender-bar>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));


    describe('sbCustomGenderBar created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-customGenderBar')).toBeGreaterThan(-1);
        });
    });
});