(function (d3) {
    angular
        .module('appComponentSlicedBarChart')
        .directive('sbSlicedBarChart', directive);

    function directive($timeout) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                height: '=chartHeight',
                colors: '=chartColors',
                data: '=data'
            },
            templateUrl: 'components/chart/slicedBarChart/sbSlicedBarChart.html',
            controllerAs: 'sbSlicedBarChartCtrl',
            link: link
        };

        function link(scope, element) {
            const BARS = 20,
                SIDEBAR_TRANSITION_DURATION = 300,
                GAP_SIZE = 2;

            // Width will snap to parent element; hence treated differently from height
            let elem = element[0], svg,
                width = elem.getBoundingClientRect().width,
                height = scope.height || elem.getBoundingClientRect().height || 150;

            d3.select(window).on('resize.' + elem.classList[0], () => {
                $timeout( () => {
                    width = elem.getBoundingClientRect().width;
                    renderChart();
                }, SIDEBAR_TRANSITION_DURATION);
            });

            scope.$watch('data', data => {
                if (data) {
                    renderChart();
                }
            });

            function renderChart() {
                let margins = {
                    left: 15,
                    right: 15,
                    top: 10,
                    bottom: 25
                },
                dataset = [ scope.data ],
                innerWidth = width - (margins.left + margins.right) + GAP_SIZE,
                innerheight = height - (margins.top + margins.bottom),
                dataStructure, xMax, xScale, yScale;

                dataStructure = dataset.map( d => {
                    return {
                        x: d.newCustPercent
                    };
                });

                xMax = d3.max( dataStructure, group => d3.max( group, d =>  d.x + d.x0 ) );

                d3.select(elem).selectAll('*').remove();

                svg = d3.select(elem)
                    .append('svg')
                    .attr({
                        width: innerWidth,
                        height: innerheight
                    })
                    .append('g')
                    .attr({
                        transform: `translate( ${margins.left}, 0 )`
                    });

                xScale = d3.scale.linear()
                    .domain([0, xMax])
                    .range([0, innerWidth]);

                yScale = d3.scale.ordinal()
                    .domain(dataStructure)
                    .rangeRoundBands([0, innerheight], 0.1);

                for( let i = 0; i < BARS; i++ ) {
                    svg.append('rect')
                        .attr({
                            x: (innerWidth / BARS) * i,
                            y: 0,
                            width: (innerWidth / BARS) - GAP_SIZE,
                            height: innerheight,
                            fill: fillFn(i, BARS)
                        });
                }

                function fillFn(i, BARS) {
                    let itemPercent = (i / BARS),
                        requiredPercent = (dataStructure[0].x) / 100;

                    return itemPercent >= requiredPercent ?
                        scope.colors.textPalette[1] : scope.colors.textPalette[0];
                }
            }
        }
    }
})(window.d3);