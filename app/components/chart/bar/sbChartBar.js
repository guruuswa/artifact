(function(d3) {
    angular
        .module('appComponentChart')
        .directive('sbChartBar', directive);

    function directive($timeout) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                height: '@chartHeight',
                data: '=chartData',
                colors: '=chartColors',
                formatAmount: '=chartFormat'
            },
            link: link,
            templateUrl: 'components/chart/bar/sbChartBar.html'
        };

        function link(scope, element) {
            const SIDEBAR_TRANSITION_DURATION = 300; // references the CSS transition
            const SLIDE_DURATION = 400;
            const TICKS = 6;
            const ITEMS_IN_VIEW = 5;
            const MARGIN = {
                left: 68,
                right: 18,
                top: 10,
                bottom: 40
            };

            let svg = d3.select(element[0].querySelector('.sb-chartBar__chart')).append('svg'),
                width = element[0].getBoundingClientRect().width,
                height = scope.height || element[0].getBoundingClientRect().height || 150,
                widthInner,
                heightInner,
                xRange,
                yRange,
                rangeSrc,
                pageStart,
                xNodeWrap,
                barsGroupWrap;

            angular.extend(scope, {
                tooltip: {
                    isActive: false,
                    value: 0,
                    x: 0,
                    y: 0,
                    circleColor: '#fff'
                },
                page: 0,
                pageMax: 0,
                pagePrevious: pagePrevious,
                pageNext: pageNext
            });

            d3.select(window).on('resize.' + element[0].classList[0], () => {
                scope.$apply(() => scope.tooltip.isActive = false);

                $timeout(() => {
                    width = element[0].getBoundingClientRect().width;
                    renderChart();
                }, SIDEBAR_TRANSITION_DURATION);
            });

            scope.$watch('data', initChart);

            function initChart() {
                let maxCurrent,
                    maxPrevious;

                if (!(scope.data && scope.data.length)) {
                    return;
                }

                scope.page = 0;
                scope.pageMax = Math.ceil(scope.data.length / ITEMS_IN_VIEW) - 1;
                pageStart = undefined;

                maxCurrent = scope.data.map(x => x.value).map(x => x[0])
                    .reduce((prev, cur) => Math.max(prev, cur), '');
                maxPrevious = scope.data.map(x => x.value).map(x => x[1])
                    .reduce((prev, cur) => Math.max(prev, cur), '');

                rangeSrc = [0, (Math.max(maxCurrent, maxPrevious) || maxCurrent)];

                renderChart();
            }

            function renderChart() {
                svg.selectAll('*').remove();

                if (!(scope.data && scope.data.length)) {
                    return;
                }

                widthInner = width - MARGIN.left - MARGIN.right;
                heightInner = height - MARGIN.top - MARGIN.bottom;

                svg.attr({
                    width: width,
                    height: height
                });

                renderAxes();
                renderBar();
                renderLine();
            }

            function animateChart() {
                pageStart = scope.page === scope.pageMax ? scope.data.length - ITEMS_IN_VIEW : scope.page * ITEMS_IN_VIEW;

                xNodeWrap.selectAll('text')
                    .each(formatLabel);

                xNodeWrap.transition()
                    .duration(SLIDE_DURATION)
                    .attr({
                        transform: `translate(${getPagePosition()}, 0)`
                    });

                barsGroupWrap.transition()
                    .duration(SLIDE_DURATION)
                    .attr({
                        transform: `translate(${getPagePosition()}, 0)`
                    });
            }

            function renderAxes() {
                let yAxis, xAxis, xNode;
                let rangeBandWidth = scope.data.length > ITEMS_IN_VIEW ? (widthInner / ITEMS_IN_VIEW * scope.data.length) : widthInner;

                xRange = d3.scale.ordinal().rangeBands([0, rangeBandWidth], 0.1).domain(scope.data.map(x => x.label));
                yRange = d3.scale.linear().domain(rangeSrc).range([heightInner, 0]).nice(TICKS);

                xAxis = d3.svg.axis()
                    .scale(xRange)
                    .orient('bottom');

                yAxis = d3.svg.axis()
                    .scale(yRange)
                    .orient('left')
                    .ticks(TICKS)
                    .tickFormat(d => scope.formatAmount(d));

                svg.append('g')
                    .selectAll('line')
                    .attr({
                        class: 'axis lines'
                    })
                    .data(yAxis.scale().ticks(TICKS))
                    .enter()
                    .append('line')
                    .attr({
                        x1: MARGIN.left,
                        y1: d => MARGIN.top + yRange(d),
                        x2: width - MARGIN.right,
                        y2: d => MARGIN.top + yRange(d),
                        stroke: '#e6e6e6'
                    });

                xNodeWrap = svg.append('svg')
                    .attr({
                        width: widthInner,
                        x: MARGIN.left,
                        overflow: 'hidden'
                    })
                    .append('g')
                    .attr({
                        class: 'axis-wrap',
                        transform: () => pageStart ? `translate(${getPagePosition()}, 0)` : null
                    });

                xNode = xNodeWrap.append('g')
                    .attr({
                        class: 'axis x',
                        transform: `translate(0, ${MARGIN.top + heightInner})`
                    })
                    .call(xAxis);

                svg.append('g')
                    .attr({
                        class: 'axis y',
                        transform: `translate(${MARGIN.left}, ${MARGIN.top})`
                    })
                    .call(yAxis);

                xNode.selectAll('text')
                    .each(formatLabel)
                    .attr('class', 'is-uppercase')
                    .insert('title', ':first-child')
                    .text(d => d.slice(0, d.indexOf('|')));
            }

            function renderLine() {
                let getYFromValue = val => MARGIN.top + yRange(val);
                let avgs = scope.data.map(x => x.value)
                    .reduce((a, b) => b.map((x, i) => x + (a[i] || 0)), [])
                    .map(x => x / scope.data.length);

                svg.append('g')
                    .selectAll('line')
                    .data(avgs)
                    .enter()
                    .append('line')
                    .attr({
                        x1: MARGIN.left,
                        y1: d => getYFromValue(d),
                        x2: width - MARGIN.right,
                        y2: d => getYFromValue(d),
                        'stroke-width': 2,
                        stroke: (d, i) => scope.colors.linePalette[i]
                    })
                    .on({
                        mouseover: (d, i) => {
                            scope.$apply(() => {
                                scope.tooltip.isActive = true;
                                scope.tooltip.value = scope.formatAmount(d);
                                scope.tooltip.x = width / 2;
                                scope.tooltip.y = yRange(d) + 5;
                                scope.tooltip.circleColor = scope.colors.linePalette[i];
                            });
                        },
                        mouseout: () => {
                            scope.$apply(() => {
                                scope.tooltip.isActive = false;
                            });
                        }
                    });
            }

            function renderBar() {
                barsGroupWrap = svg.append('svg')
                    .attr({
                        width: widthInner,
                        height: heightInner,
                        x: MARGIN.left,
                        y: MARGIN.top,
                        overflow: 'hidden'
                    })
                    .append('g')
                    .attr({
                        class: 'bar-group-wrap',
                        transform: () => pageStart ? `translate(${getPagePosition()}, 0)` : null
                    });

                let barGroup = barsGroupWrap.selectAll('.bar-group')
                    .data(() => scope.data)
                    .enter()
                    .append('g')
                    .attr({
                        transform: d => `translate(${xRange(d.label)}, 0)`,
                        class: 'bar-group'
                    });

                barGroup.selectAll('rect')
                    .data(d => d.value)
                    .enter()
                    .append('rect')
                    .attr({
                        x: (d, i, gi) => (xRange.rangeBand() / scope.data[gi].value.length) * i,
                        y: d => yRange(d),
                        width: (d, i, gi) => xRange.rangeBand() / scope.data[gi].value.length,
                        height: d => heightInner - yRange(d),
                        fill: (d, i) => scope.colors.barPalette[i]
                    })
                    .on({
                        mouseover: (d, i, gi) => {
                            let max = d3.max(scope.data.map(x => d3.max(x.value)));
                            // pixels to nudge the tooltip circle from the y axis
                            // if current bar's value is less than 16% of the max, then set nudge to 5px, otherwise set to 20
                            // change request 1 Feb 16 - arrow of tooltip must be on the graph line, not below.
                            // - changing offset from 20 to 18
                            let nudgeTooltipValue = (d < max * 0.16) ? 5 : 18;

                            scope.$apply(() => {
                                scope.tooltip.isActive = true;
                                scope.tooltip.value = scope.formatAmount(d);
                                scope.tooltip.x = getPagePosition() + xRange(xRange.domain()[gi]) + MARGIN.left + (xRange.rangeBand() / (scope.data[gi].value.length * 2)) + ((xRange.rangeBand() * i) / 2);
                                scope.tooltip.y = yRange(d) + nudgeTooltipValue;
                                scope.tooltip.circleColor = scope.colors.barPalette[i];
                            });
                        },
                        mouseout: () => {
                            scope.$apply(() => scope.tooltip.isActive = false);
                        }
                    });
            }

            function formatLabel(d, i) {
                let el = d3.select(this);
                let words = d.split('|');
                el.text('');

                if (i < (pageStart || scope.page) || i >= (pageStart || scope.page) + ITEMS_IN_VIEW) {
                    return;
                }

                for (let wordIndex = 0, l = words.length; wordIndex < l; wordIndex++) {
                    el.append('tspan')
                        .text(words[wordIndex])
                        .attr({
                            x: wordIndex !== 0 ? 0 : null,
                            dy: wordIndex !== 0 ? 12 * wordIndex : null
                        })
                        .each(ellipsis);
                }

                function ellipsis() {
                    let self = d3.select(this);
                    let text = self.text();
                    while (self.node().getComputedTextLength() > (xRange.rangeBand() - 10) && text.length > 0) {
                        text = text.slice(0, -1);
                        self.text(text + '...');
                    }
                }
            }

            function pagePrevious() {
                if (scope.page > 0) {
                    scope.page--;
                }
                animateChart();
            }

            function pageNext() {
                if (scope.page < scope.pageMax) {
                    scope.page++;
                }
                animateChart();
            }

            function getPagePosition() {
                return -((widthInner / ITEMS_IN_VIEW) * (pageStart || 0));
            }
        }
    }
})(window.d3);
