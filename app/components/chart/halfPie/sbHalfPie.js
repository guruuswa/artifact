(function(d3) {
    angular
        .module('appComponentChart')
        .directive('sbHalfPie', directive);

    function directive($timeout) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                height: '@height',
                data: '=data',
                colors: '=colors',
                slice: '=slice',
                applyFilter: '=filter'
            },
            link: link,
            templateUrl: 'components/chart/halfPie/sbHalfPie.html'
        };

        function link(scope, element) {
            const SIDEBAR_TRANSITION_DURATION = 300;
            const RING_WIDTH = 40;
            let leftPadding = 20;

            let elem = element[0],
                svg,
                width,
                height;

            scope.tooltip = {
                isActive: false,
                value: 0,
                x: 0,
                y: 0,
                circleColor: '#fff',
                timeRange: [],
                applyFilter: false
            };

            d3.select(window).on('resize.' + elem.classList[0], () => {
                scope.$apply(() => scope.tooltip.isActive = false);

                $timeout(() => {
                    renderChart();
                }, SIDEBAR_TRANSITION_DURATION);
            });

            scope.$watch('data', data => {
                if (data) {
                    renderChart();
                }
            });

            function renderChart() {
                d3.select(elem).selectAll('svg').remove();

                width = Math.min(elem.getBoundingClientRect().width, scope.height);
                height = Math.min(elem.getBoundingClientRect().width, scope.height)+30;

                svg = d3.select(elem)
                    .append('svg')
                    .attr({
                        width: width + leftPadding,
                        height: (height/2)+30
                    });

                var radius = Math.min(width, height) / 2,
                    pi = Math.PI,
                    pie, arc,
                    colors = d3.scale.ordinal().range(scope.colors.slicePalette);

                if (!scope.data) {
                    return;
                }

                var keys = scope.data.map(function(d) {
                    return d.range[0] || 0;
                });

                colors.domain(keys);

                pie = d3.layout.pie()
                    .sort(null)
                    .value(function(data) {
                        return data.value;
                    })
                    .startAngle(-90 * (pi / 180))
                    .endAngle(90 * (pi / 180));


                arc = d3.svg.arc()
                    .outerRadius(radius)
                    .innerRadius(radius - RING_WIDTH);

                svg.append('g')
                    .attr('width', width)
                    .attr('height', height)
                    .append('g')
                    .attr('transform', 'translate(' + ((width - radius) + (leftPadding/2)) + ',' + (height - radius) + ')')
                    .selectAll('path')
                    .data(pie(scope.data))
                    .enter()
                    .append('g')
                    .attr('class', ''+scope.slice);

                let labelData = [
                    {text: '00:00', x: 6 + (leftPadding/2), y: (height / 2) + 25},
                    {text: '24:00', x: (width - 32) + (leftPadding/2), y: (height / 2) + 25}
                ];

                for (let i = 0; i < labelData.length; i++) {
                    svg.append('g')
                        .append('text')
                        .text(labelData[i].text)
                        .attr('x', labelData[i].x)
                        .attr('y', labelData[i].y)
                        .attr('font-size', '10px')
                        .attr('fill', '#999999');
                }

                var blocks = d3.selectAll('g.'+scope.slice);

                blocks.append('path')
                    .attr('fill', function(data) {
                        return colors(data.data.range[0] || 0);
                    })
                    .attr('d', arc)
                    .attr('class', 'blockStyle')
                    .on({
                        mouseover: (data) => {
                            scope.$apply(() => {
                                var c = arc.centroid(data);
                                scope.tooltip.isActive = true;
                                scope.tooltip.value = data.data.label;
                                scope.tooltip.x = c[0] + (width - radius) + 20;
                                scope.tooltip.y = c[1] + (height - radius) + 6;
                                scope.tooltip.circleColor = data.data.color;
                                scope.tooltip.timeRange = data.data.range;
                                scope.tooltip.applyFilter = scope.applyFilter;
                            });
                        },
                        mouseout: () => {
                            scope.$apply(() => scope.tooltip.isActive = false);
                        }
                    });

                blocks.append('svg:line')
                    .attr('x1', function(data){
                        var c = arc.centroid(data);
                        return c[0]*1.3;
                    })
                    .attr('y1', function(data){
                        var c = arc.centroid(data);
                        return c[1]*1.3;
                    })
                    .attr('x2', function(data){
                        var c = arc.centroid(data);

                        //array element with index 0
                        if(typeof data.data.arcText !== 'undefined' && data.data.arcText !== ''){
                            return c[0]*1.5;
                        }

                        return c[0]*1.4;
                    })
                    .attr('y2', function(data){
                        var c = arc.centroid(data);

                        //array element with index 1
                        if(typeof data.data.arcText !== 'undefined' && data.data.arcText !== ''){
                            return c[1]*1.5;
                        }

                        return c[1]*1.4;
                    })
                    .attr('class', 'blockLine');

                blocks.append('svg:text')
                    .attr('transform', function(d){
                        var c = arc.centroid(d);
                        return 'translate(' + c[0]*1.6 + ', ' + c[1]*1.6 + ')';})
                    .attr('text-anchor', 'middle')
                    .text( function(data) {
                        return data.data.arcText;
                    })
                    .attr('font-size', '10px')
                    .attr('fill', '#999999');
            }
        }
    }
})(window.d3);