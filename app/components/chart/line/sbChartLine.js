(function(d3) {
    'use strict';

    angular
        .module('appComponentChart')
        .directive('sbLineGraph', directive);

    function directive($timeout) {
        return {
            replace: true,
            restrict: 'E',
            scope: {
                height: '@chartHeight',
                labels: '=chartDataLabels',
                values: '=chartDataValues',
                valuesCompare: '=chartDataValuesCompare',
                colors: '=chartColors',
                formatAmount: '=chartFormat'
            },
            templateUrl: 'components/chart/line/sbChartLine.html',
            link: linkFn
        };

        function linkFn(scope, element) {
            const SIDEBAR_TRANSITION_DURATION = 300;

            let graphContainer = d3.select(element[0]);
            let margin = {
                    top: 20,
                    right: 61,
                    bottom: 20,
                    left: 83
                },
                xAxisHeight = 20,
                yAxisWidth = 20,
                width = graphContainer.node().clientWidth - (margin.right + margin.left),
                height = scope.height - (margin.top + margin.bottom),
                svg,
                x,
                y,
                line,
                area,
                xAxis,
                yAxis,
                horizontalLineGroup,
                yCompare,
                timeoutPromise;

            // rangeBand required for ordinal
            scope.$watch('values', initChart);

            function initChart() {
                if (!(scope.values && scope.values.length)) {
                    return;
                }

                x = d3.scale.ordinal().rangeBands([0, width], 0, -0.4).domain(scope.labels);
                y = d3.scale.linear().range([height, 0]).domain([0, d3.max(scope.values)]).nice();

                yCompare = d3.scale.linear().range([height, 0]).domain([0, d3.max(scope.values)]).nice();

                // create a line function that converts scope.values[] into x and y points
                line = d3.svg.line()
                    .x((d, i) => x(scope.labels[i]) + (x.rangeBand() / 2))
                    .y((d) => y(d));

                area = d3.svg.area()
                    .x((d, i) => x(scope.labels[i]) + (x.rangeBand() / 2))
                    .y0(height)
                    .y1(d => y(d));

                // create xAxis
                xAxis = d3.svg.axis()
                    .scale(x)
                    .tickFormat((d, i) => scope.labels[i]);

                // create yAxis
                yAxis = d3.svg.axis()
                    .scale(y)
                    .ticks(4)
                    .orient('left')
                    .tickFormat(d => scope.formatAmount(d));

                render();
            }

            d3.select(window).on('resize', resize);

            function resize() {
                if (timeoutPromise) {
                    $timeout.cancel(timeoutPromise);
                }

                if (scope.values) {
                    timeoutPromise = $timeout(() => {
                        width = graphContainer.node().clientWidth - (margin.right + margin.left);

                        x.rangeBands([0, width], 0, -0.4);
                        y.range([height, 0]);

                        render();
                    }, SIDEBAR_TRANSITION_DURATION);
                }
            }

            function render() {
                graphContainer.selectAll('*').remove();

                svg = graphContainer.append('svg').attr({
                        width: width + margin.right + margin.left,
                        height: height + margin.top + margin.bottom
                    })
                    .append('g')
                    .attr({
                        transform: `translate( ${ margin.left + yAxisWidth / 2 }, ${ margin.top - xAxisHeight / 2 } )`
                    });

                horizontalLineGroup = svg.append('g')
                    .selectAll('line.grid-line').data(y.ticks(4)).enter()
                    .append('line')
                    .attr({
                        class: 'grid-line',
                        x1: 0,
                        x2: width,
                        y1: d => y(d),
                        y2: d => y(d)
                    });

                // Add xAxis
                svg.append('g')
                    .attr({
                        class: 'x axis',
                        transform: `translate(0, ${height})`
                    })
                    .call(xAxis);

                // Add yAxis
                svg.append('g')
                    .attr({
                        class: 'y axis'
                    })
                    .call(yAxis);

                // Add path with scope.values
                if (scope.valuesCompare && scope.valuesCompare.length) {
                    svg.append('path')
                        .attr({
                            d: line(scope.valuesCompare),
                            class: 'path-line'
                        });
                    svg.append('path')
                        .attr({
                            d: area(scope.valuesCompare),
                            class: 'path-area',
                            fill: scope.colors.colorCompare
                        });
                }

                svg.append('path')
                    .attr({
                        d: line(scope.values),
                        class: 'path-line'
                    });

                svg.append('path')
                    .attr({
                        d: area(scope.values),
                        class: 'path-area',
                        fill: scope.colors.color
                    });
            }
        }
    }
})(window.d3);
