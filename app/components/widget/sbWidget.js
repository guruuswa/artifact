(function () {
    angular
        .module('appComponentWidget')
        .directive('sbWidget', directive);

    function directive() {
        return {
            restrict: 'E',
            replace: true,
            transclude: {
                header: '?widgetHeader',
                body: '?widgetBody'
            },
            scope: {
                title: '@widgetTitle',
                loaderTarget: '@widgetLoaderTarget'
            },
            templateUrl: 'components/widget/sbWidget.html',
            link: link
        };

        function link() {
            // link logic
        }
    }
})();
