describe('sbWidget', () => {
    let $compile, $scope, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        element = angular.element('<sb-widget></sb-widget>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));


    describe('sbWidget created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-widget')).toBeGreaterThan(-1);
        });
    });
});