(function () {
		angular
		.module('appComponentContactus')
		.directive('sbContactus', directive);

		function directive() {
			return {
				restrict: 'E',
				replace: true,
				scope: {},
				templateUrl: 'components/contactus/sbContactus.html',
				controller: controller,
				controllerAs: 'sbContactusCtrl',
				link: link
			};

			function controller() {
				// controller logic
			}

			function link() {
				// link logic
			}
		}
})();
