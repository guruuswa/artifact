(function() {
    angular
        .module('appComponentTooltip')
        .directive('sbTooltip', directive);

    function directive() {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            scope: {
                showTooltip: '=showTooltip',
                showCircle: '=showCircle',
                xCoordinate:'=x',
                yCoordinate:'=y',
                circleColor: '=circleColor'
            },
            templateUrl: 'components/tooltip/sbTooltip.html'
        };
    }
})();

