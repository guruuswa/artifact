(function () {
    angular
        .module('appComponentSidebar')
        .directive('sidebar', directive);

    function directive(sidebarFactory, merchantsFactory, userFactory, loadingHelper) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            templateUrl: 'components/sidebar/sidebar.html',
            link: link
        };

        function link(scope) {
            angular.extend(scope, {
                sidebar: sidebarFactory.get(),
                currentUser: userFactory.getUser(),
                merchantList: merchantsFactory.getWithAllStores().result,
                activeMerchant: merchantsFactory.getActive(),
                helpOpen: false,
                contactUsOpen: false,
                setFilter: setFilter,
                openContactUs: openContactUs,
                openHelp: openHelp,
                toggleSidebar: toggleSidebar
            });

            sidebarFactory.onUpdate.subscribe(scope, () => {
                scope.sidebar = sidebarFactory.get();
            });

            function setFilter(merchant) {
                loadingHelper.startFullPage();
                scope.activeMerchant = merchantsFactory.setActive(merchant.id);
            }

            function openContactUs() {
                scope.contactUsOpen = true;
            }

            function openHelp() {
                scope.helpOpen = true;
            }

            function toggleSidebar() {
                sidebarFactory.toggle();
            }
        }
    }
})();