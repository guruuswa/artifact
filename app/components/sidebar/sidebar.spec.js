describe('sidebar', () => {
    const MERCHANTS_REQUEST = '/_search';
    const MERCHANTS_REQUEST_TEST = RegExp(MERCHANTS_REQUEST, 'i');
    let factory, $httpBackend;
    let requestHandler;

    let $compile, $scope, element, elementScope, sidebarFactory;

    beforeEach(inject((_$compile_, _$rootScope_, _merchantsFactory_, _$httpBackend_, _sidebarFactory_) => {
        factory = _merchantsFactory_;
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend.when('POST', MERCHANTS_REQUEST_TEST).respond(mockMerchants());

        sidebarFactory = _sidebarFactory_;

        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        element = angular.element('<sidebar></sidebar>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    describe('sidebar created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sidebar')).toBeGreaterThan(-1);
        });

        it('displays contact modal on click', () => {
            elementScope.openContactUs();
            expect(elementScope.contactUsOpen).toBe(true);
        });

        it('displays help modal on click', () => {
            elementScope.openHelp();
            expect(elementScope.helpOpen).toBe(true);
        });

        it('toggles sidebar', () => {
            elementScope.toggleSidebar();
            expect(sidebarFactory.get().isOpen).toBeTruthy();
        });
    });

    function mockMerchants() {
        return [{
            id: 'BP EDENVALE NORTH',
            name: 'BP EDENVALE NORTH'
        }, {
            id: 'BP WINMORE',
            name: 'BP WINMORE'
        }, {
            id: 'BP RIDGE OASIS',
            name: 'BP RIDGE OASIS'
        }, {
            id: 'SASOL RANDHART',
            name: 'SASOL RANDHART'
        }, {
            id: 'VILLAGE ROAD MOTORS',
            name: 'VILLAGE ROAD MOTORS'
        }];
    }
});