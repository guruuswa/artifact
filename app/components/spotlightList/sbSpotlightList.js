(function () {
    angular
        .module('appComponentSpotlightList')
        .directive('sbSpotlightList', directive);

    function directive(spotlightFactory, userFactory, merchantsFactory) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                item: '='
            },
            templateUrl: 'components/spotlightList/sbSpotlightList.html',
            link: link
        };

        function link(scope) {
            angular.extend(scope, {
                spotlightList: getSpotlight(),
                currentUser: userFactory.getUser(),
                helpOpen: false,
                contactUsOpen: false,
                openContactUs: openContactUs,
                openHelp: openHelp
            });

            spotlightFactory.onUpdate.subscribe(scope, () => {
                scope.spotlightList = getSpotlight();
            });

            merchantsFactory.activeUpdate.subscribe(scope, () => {
                scope.spotlightList = getSpotlight();
            });

            function openContactUs() {
                scope.contactUsOpen = true;
            }

            function openHelp() {
                scope.helpOpen = true;
            }

            function getSpotlight() {
                scope.spotlightList = spotlightFactory.clear();
                return spotlightFactory.get(merchantsFactory.getActive()).result;
            }
        }
    }
})();