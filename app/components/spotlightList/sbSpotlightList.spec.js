describe('sbSpotlightList', () => {
    const SPOTLIGHT_REQUEST = '/_search';
    const SPOTLIGHT_REQUEST_TEST = RegExp(SPOTLIGHT_REQUEST, 'i');
    let factory, $httpBackend;
    let requestHandler;

    let $compile, $scope, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_, _spotlightFactory_, _$httpBackend_) => {
        factory = _spotlightFactory_;
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend.when('POST', SPOTLIGHT_REQUEST_TEST).respond(mockSpotlight());

        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        element = angular.element('<sb-spotlight-list><sb-spotlight-list>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    describe('sbSpotlightList created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('spotlight-list')).toBeGreaterThan(-1);
        });

        it('displays contact modal on click', () => {
            elementScope.openContactUs();
            expect(elementScope.contactUsOpen).toBe(true);
        });

        it('displays help modal on click', () => {
            elementScope.openHelp();
            expect(elementScope.helpOpen).toBe(true);
        });
    });

    function mockSpotlight() {
        return [{
            title: 'Sales values',
            value: 1218036975.57,
            percentage: 15,
            isIncrease: true,
            icon: 'icon-graph',
            currency: 'R'
        }, {
            title: 'Sales volume',
            value: 1717956,
            percentage: 2,
            isIncrease: true,
            icon: 'icon-cart'
        }, {
            title: 'Average transaction value',
            value: 709.00,
            percentage: 2,
            isIncrease: false,
            icon: 'icon-card',
            currency: 'R'
        }];
    }
});