(function () {
    angular
        .module('appComponentNewReturningCustomers')
        .directive('sbNewReturningCustomers', directive);

    function directive($filter, loadingHelper, merchantsFactory, merchantSalesFactory, newVsReturningCustomersFactory) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            templateUrl: 'components/newReturningCustomers/sbNewReturningCustomers.html',
            controller: controller,
            controllerAs: 'sbNewReturningCustomersCtrl',
            link: link
        };

        function controller() {
            // controller logic
        }

        function link(scope) {
            angular.extend(scope, {
                merchantList: merchantsFactory.getWithAllStores().result,
                activeMerchant: merchantsFactory.getActive(),
                nvrData: '',
                filterType: false,
                filterStores: [],
                showFilterStores: false,
                showHideAllStoreChartPopover: showHideAllStoreChartPopover,
                selectFilterStore: selectFilterStore,
                storeLabel: '',
                chartColors: {
                    barPalette: [
                        '#3366cc',
                        '#00aa77'
                    ],
                    textPalette: [
                        '#3366cc',
                        '#00aa77',
                        '#666666'
                    ]
                }
            });

            merchantsFactory.activeUpdate.subscribe(scope, () => {
                scope.activeMerchant = merchantsFactory.getActive();
                scope.filterStores.map(x => {
                    x.isActive = x.value === scope.activeMerchant.id || scope.activeMerchant.id === null;
                    return x;
                });
                reload();
            });

            reload();

            merchantsFactory.activeUpdate.subscribe(scope, () => {
                scope.activeMerchant = merchantsFactory.getActive();
                scope.filterStores.map(x => x.isActive = x.value === scope.activeMerchant.id);
                scope.nvrData = newVsReturningCustomersFactory.getById(scope.activeMerchant.id);
                scope.storeLabel = scope.activeMerchant.name;
            });

            function showHideAllStoreChartPopover() {
                scope.showFilterStores = !scope.showFilterStores;
            }

            function selectFilterStore(index) {

                scope.filterStores.map(x => {
                    x.isActive = false;
                });

                scope.showFilterStores = true;
                scope.filterStores[index].isActive = true;
                scope.storeLabel = scope.filterStores[index].label;
                scope.nvrData = newVsReturningCustomersFactory.getById(scope.filterStores[index].value);
                showHideAllStoreChartPopover();
            }

            function reload() {
                loadingHelper.start('sbNewReturningCustomers', 'newReturningCustomers');

                newVsReturningCustomersFactory.get().then(() => {
                    scope.nvrData = newVsReturningCustomersFactory.getById();

                    loadingHelper.stop('sbNewReturningCustomers', 'newReturningCustomers');
                });

                merchantsFactory.getWithAllStores().then(data => {
                    scope.filterStores = data.map(x => {
                        return {
                            value: x.id,
                            label: x.name,
                            isActive: false
                        };
                    });
                    scope.filterStores[0].isActive = true;
                    scope.storeLabel = scope.filterStores[0].label;
                });
            }
        }
    }
})();