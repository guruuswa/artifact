(function () {
	angular
		.module('appComponentSignout')
		.directive('sbSignout', directive);

		function directive() {
			return {
				restrict: 'E',
				replace: true,
				scope: {
					cancel: '=callbackCancel',
					signout: '=callbackSignout'
				},
				templateUrl: 'components/signout/sbSignout.html'
			};
		}
})();