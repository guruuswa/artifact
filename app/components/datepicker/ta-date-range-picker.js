(function(moment) {
    angular
        .module('appComponentDatepicker')
        .directive('taDateRangePicker', directive);

    function directive($compile, $timeout, $templateRequest, filtersFactory) {
        var CUSTOM = 'CUSTOM';

        return {
            scope: {
                model: '=ngModel',
                filterOpenCallback: '=?',
                ranges: '=?',
                callback: '&?',
                updateFunction: '&?',
                getDefaultRanges: '&?',
                filterTitle: '=?',
                datepickerId: '@',
                visible: '='
            },
            replace: true,
            // template: '<div class=\'selectbox\'><i class=\'fa fa-calendar\'></i> <span ng-show=\'model\'>{{model.start.format(\'LL\')}} - {{model.end.format(\'LL\')}}</span> <span ng-hide=\'model\'>Select date range</span> <b class=\'caret\'></b></div>',
            templateUrl: 'components/datepicker/filter.html',
            link: link
        };

        function link(scope, element) {
            scope.weekDays = moment.weekdaysMin();
            scope.currentViewLeft = 'day';
            scope.currentViewRight = 'day';
            scope.allMonths = getMonths();
            // sets default date to be displayed in the filter.html
            scope.defaultFilterByDate = filtersFactory.getActive().dateRangeDescription;

            //set default ranges
            if (!(scope.ranges && scope.ranges.length)) {
                scope.ranges = scope.getDefaultRanges();
            }

            scope.show = function() {
                //clear prevs
                scope.currentSelection = null;

                //prepare
                prepareMonths(scope);
                scope.previousSelection = scope.model;
                scope.selection = scope.model;
                prepareRanges(scope);
                scope.visible = true;
                return scope.visible;
            };

            scope.hide = function($event) {
                if ($event !== null && typeof $event !== 'undefined') {
                    if (typeof $event.stopPropagation === 'function') {
                        $event.stopPropagation();
                    }
                }
                scope.currentViewLeft = 'day';
                scope.currentViewRight = 'day';
                scope.visible = false;
                scope.start = null;
                return scope.start;
            };

            scope.cancel = function($event) {
                if(typeof scope.previousSelection !== 'undefined'){
                    scope.selection = scope.previousSelection;
                }

                scope.ok($event, true);
            };

            scope.handlePickerClick = function($event) {
                return $event !== null && typeof $event !== 'undefined' ? typeof $event.stopPropagation === 'function' ? $event.stopPropagation() : void 0 : void 0;
            };

            scope.select = function(day, $event) {
                if ($event !== null && typeof $event !== 'undefined') {
                    if (typeof $event.stopPropagation === 'function') {
                        $event.stopPropagation();
                    }
                }
                if (day.disabled) {
                    return;
                }

                //both dates are already selected, reset dates
                var current = scope.getCurrentSelection();

                var date = day.date;
                if ((current.start && current.end) || !current.start) {
                    current.start = moment(date);
                    current.end = null;
                    scope.inputDates[0] = current.start.format('DD MMM YY');
                } else if (current.start && !current.end) {
                    if (current.start.isAfter(date, 'day')) {
                        current.start = moment(date);
                        scope.inputDates[0] = current.start.format('DD MMM YYYY');
                    } else if (current.start.isBefore(date, 'day')) {
                        current.end = moment(date);
                        scope.inputDates[1] = current.end.format('DD MMM YYYY');
                    }
                }

                scope.resetRangeClass();
            };

            scope.setRange = function(item, $event, ranges) {
                for (let i = 0; i < ranges.length; i++) {
                    ranges[i].active = false;
                }
                item.active = true;

                if (!item.range) {
                    return;
                }

                scope.showCalendars = true;
                scope.showLeftCalendar = true;
                scope.showRightCalendar = true;

                if (item.range === CUSTOM) {
                    scope.selection = {};
                    return;
                }

                scope.currentSelection = item.range.clone();
                scope.currentSelection.description = moment(scope.currentSelection.start).format('DD MMM YY') + ' - ' + moment(scope.currentSelection.end).format('DD MMM YY');
                scope.selection = scope.currentSelection.clone();

                //move to month
                scope.months[0] = createMonth(scope.currentSelection.start.clone());
                scope.months[1] = createMonth(scope.currentSelection.end.clone());
                scope.inputDates[0] = scope.currentSelection.start.format('YYYY-MM-DD');
                scope.inputDates[1] = scope.currentSelection.end.format('YYYY-MM-DD');

                //hide second calendar if range doesn't roll over
                var startDate = new Date(scope.currentSelection.start);
                var endDate = new Date(scope.currentSelection.end);
                if (startDate.getFullYear() === endDate.getFullYear()) {
                    if (startDate.getMonth() === endDate.getMonth()) {
                        scope.showRightCalendar = false;
                    }
                }

                scope.ok($event, false);
            };

            scope.ok = function($event, hide) {
                if ($event !== null && typeof $event !== 'undefined') {
                    if (typeof $event.stopPropagation === 'function') {
                        $event.stopPropagation();
                    }
                }

                $timeout(function() {
                    if (scope.callback) {
                        scope.updateFunction();
                        return scope.callback();
                    }
                });

                if (hide) {
                    return scope.hide();
                }
            };

            scope.clear = function($event) {
                if ($event !== null && typeof $event !== 'undefined') {
                    if (typeof $event.stopPropagation === 'function') {
                        $event.stopPropagation();
                    }
                }
                scope.selection = null;
                scope.ok($event, true);
            };

            scope.applySelection = function($event, ranges) {
                if ($event !== null && typeof $event !== 'undefined') {
                    if (typeof $event.stopPropagation === 'function') {
                        $event.stopPropagation();
                    }
                }

                scope.showCalendars = true;
                scope.showLeftCalendar = true;
                scope.showRightCalendar = true;
                scope.selection = moment.range(scope.currentSelection.start.clone(), scope.currentSelection.end.clone());

                let description = '';
                for (let i = 0; i < ranges.length; i++) {
                    if (ranges[i].active) {
                        if (ranges[i].range === 'CUSTOM') {
                            description = scope.currentSelection.start.format('DD MMM YY') + ' - ' + scope.currentSelection.end.format('DD MMM YY');
                        } else {
                            description = ranges[i].label;
                        }
                    }
                }
                scope.selection.description = description;

                scope.model = scope.selection;

                scope.ok($event, true);
            };

            scope.move = function(date, n, $event, first) {
                if ($event !== null && typeof $event !== 'undefined') {
                    if (typeof $event.stopPropagation === 'function') {
                        $event.stopPropagation();
                    }
                }

                var newDate = date.clone().add(n, 'months');
                var rightCalendar = new Date(scope.months[1].date);
                rightCalendar.setMonth(rightCalendar.getMonth() - 1);
                var leftCalendar = new Date(scope.months[0].date);
                scope.months[0].rightArrowClass = 'unavailable';
                scope.months[1].leftArrowClass = 'unavailable';

                if (first && scope.months[1].date > newDate) {
                    scope.months[0] = createMonth(newDate);
                    scope.months[0].rightArrowClass = 'available';
                    scope.months[1].leftArrowClass = 'available';
                }

                if (scope.months[0].date < newDate) {
                    scope.months[1] = createMonth(newDate);
                    scope.months[0].rightArrowClass = 'available';
                    scope.months[1].leftArrowClass = 'available';
                }

                if (rightCalendar.getMonth() === leftCalendar.getMonth()) {
                    scope.months[0].rightArrowClass = 'unavailable';
                    scope.months[1].leftArrowClass = 'unavailable';
                }
            };

            scope.getCurrentSelection = function() {
                if (!scope.currentSelection && scope.selection) {
                    scope.currentSelection = scope.selection.clone();
                }
                if (!scope.currentSelection) {
                    scope.currentSelection = {};
                }
                return scope.currentSelection;
            };

            scope.getClassName = function(day) {
                var current = scope.getCurrentSelection();

                if (!day || day.number === false) {
                    return 'off';
                }

                if (current) {
                    if (current.start && current.start.isSame(day.date, 'day')) {
                        return 'active start-date';
                    }
                    if (current.end && current.end.isSame(day.date, 'day')) {
                        return 'active end-date';
                    }
                    if (current.start && current.end && current.start.isBefore(day.date, 'day') && current.end.isAfter(day.date, 'day')) {
                        return 'in-range';
                    }
                }
                return 'available';
            };

            scope.resetRangeClass = function() {
                var found = false;
                var current = scope.getCurrentSelection();
                for (var i = 0; i < scope.ranges.length; i++) {
                    var item = scope.ranges[i];
                    item.active = false;
                    if (item.range && item.range !== CUSTOM && current.start && current.end) {
                        if (current.start.isSame(item.range.start, 'day') && current.end.isSame(item.range.end, 'day')) {
                            item.active = true;
                            found = true;
                        }
                    }
                }
                if (!found) {
                    scope.ranges[scope.ranges.length - 1].active = true;
                }
            };

            scope.updateStartOrEndDate = function(first, last) {
                var current = scope.getCurrentSelection();

                if (first) {
                    var start = moment(scope.inputDates[0]);
                    if (!start) {
                        return;
                    }

                    current.start = start;
                    if (!current.end || current.end.isBefore(start, 'day')) {
                        current.end = start;
                        scope.inputDates[1] = current.end.format('YYYY-MM-DD');
                    }
                } else if (last) {
                    var end = moment(scope.inputDates[1]);
                    if (!end) {
                        return;
                    }

                    current.end = end;
                    if (!current.start || current.start.isAfter(end, 'day')) {
                        current.start = end;
                        scope.inputDates[0] = current.start.format('YYYY-MM-DD');
                    }
                }
                scope.resetRangeClass();
            };

            scope.moveToMonth = function(first, index) {
                if (!first) {
                    return;
                }

                var start = moment(scope.inputDates[0]);
                if (!start) {
                    return;
                }

                if (!start.isSame(scope.months[index].date, 'month')) {
                    //move to month
                    scope.months[0] = createMonth(start.clone());
                    scope.months[1] = createMonth(start.clone().add(1, 'months'));
                }
            };

            scope.selectMonth = function(monthIndex, leftOrRightIndex) {
                var start = new Date(scope.inputDates[leftOrRightIndex]);
                if (!start) {
                    return;
                }

                start.setMonth(monthIndex);
                scope.inputDates[leftOrRightIndex] = moment(start).format('YYYY-MM-DD');
                scope.resetRangeClass();

                if (leftOrRightIndex === 0) {
                    scope.currentSelection.start = moment(start);
                } else {
                    scope.currentSelection.end = moment(start);
                }
                scope.currentSelection.description = moment(scope.currentSelection.start).format('DD MMM YY') + ' - ' + moment(scope.currentSelection.end).format('DD MMM YY');
                scope.months[leftOrRightIndex] = createMonth(moment(start));
                scope.switchView(leftOrRightIndex);
            };

            scope.selectYear = function(year, leftOrRightIndex) {
                var start = new Date(scope.inputDates[leftOrRightIndex]);
                if (!start) {
                    return;
                }

                start.setFullYear(year);
                scope.inputDates[leftOrRightIndex] = moment(start).format('YYYY-MM-DD');
                scope.resetRangeClass();

                if (leftOrRightIndex === 0) {
                    scope.currentSelection.start = moment(start);
                } else {
                    scope.currentSelection.end = moment(start);
                }
                scope.currentSelection.description = moment(scope.currentSelection.start).format('DD MMM YY') + ' - ' + moment(scope.currentSelection.end).format('DD MMM YY');
                scope.months[leftOrRightIndex] = createMonth(moment(start));
                scope.switchView(leftOrRightIndex);
            };

            scope.switchView = function(index) {
                if (index === 0) {
                    switch (scope.currentViewLeft) {
                        case 'year':
                            scope.currentViewLeft = 'month';
                            scope.allMonths = validateMonths(scope.inputDates[0], scope.inputDates[1], 'left');
                            break;
                        case 'day':
                            scope.currentViewLeft = 'year';
                            scope.yearRangeLeft = getYearRange(scope.inputDates[0], scope.inputDates[1], 'left');
                            break;
                        case 'month':
                            scope.currentViewLeft = 'day';
                            break;
                    }
                    scope.currentViewRight = 'day';
                } else {
                    switch (scope.currentViewRight) {
                        case 'year':
                            scope.currentViewRight = 'month';
                            scope.allMonths = validateMonths(scope.inputDates[1], scope.inputDates[0], 'right');
                            break;
                        case 'day':
                            scope.currentViewRight = 'year';
                            scope.yearRangeRight = getYearRange(scope.inputDates[1], scope.inputDates[0], 'right');
                            break;
                        case 'month':
                            scope.currentViewRight = 'day';
                            break;
                    }
                    scope.currentViewLeft = 'day';
                }
            };

            scope.getYears = function(leftOrRightIndex) {
                if (leftOrRightIndex === 0) {
                    return scope.yearRangeLeft;
                } else {
                    return scope.yearRangeRight;
                }
            };

            scope.moveYearRange = function(leftOrRightIndex, n) {
                var leftCalDate = new Date(scope.inputDates[0]);
                var rightCalDate = new Date(scope.inputDates[1]);
                var i = 0;
                if (leftOrRightIndex === 0) {
                    for (i = 0; i < scope.yearRangeLeft.length; i++) {
                        if (n > 0) {
                            scope.yearRangeLeft[i].desc += 10;
                        } else {
                            scope.yearRangeLeft[i].desc -= 10;
                        }

                        scope.yearRangeLeft[i].state = '';
                        if (scope.yearRangeLeft[i].desc > rightCalDate.getFullYear()) {
                            scope.yearRangeLeft[i].state = 'disabled';
                        }
                    }
                } else {
                    for (i = 0; i < scope.yearRangeRight.length; i++) {
                        if (n > 0) {
                            scope.yearRangeRight[i].desc += 10;
                        } else {
                            scope.yearRangeRight[i].desc -= 10;
                        }

                        scope.yearRangeRight[i].state = '';
                        if (scope.yearRangeRight[i].desc < leftCalDate.getFullYear()) {
                            scope.yearRangeRight[i].state = 'disabled';
                        }
                    }
                }
            };


            /**************************************************************************************/
            //load popup template
            $templateRequest('components/datepicker/datePickerPopup.html').then(function(html) {
                var template = angular.element(html);

                element.append(template);
                $compile(template)(scope);
                element.bind('click', function(e) {
                    scope.filterOpenCallback(scope.datepickerId);

                    if (e !== null) {
                        if (typeof e.stopPropagation === 'function') {
                            e.stopPropagation();
                        }
                    }
                    return scope.$apply(function() {
                        if (scope.visible) {
                            return scope.hide();
                        } else {
                            return scope.show();
                        }
                    });
                });
                var documentClickFn = function() {
                    scope.$apply(function() {
                        return scope.hide();
                    });
                    return true;
                };
                angular.element(document).bind('click', documentClickFn);
                scope.$on('$destroy', function() {
                    return angular.element(document).unbind('click', documentClickFn);
                });
            });
        }

        function getYearRange(current, other, LeftOrRight) {
            var start = new Date(current);
            var startYear = start.getFullYear() - 5;
            var validateDate = new Date(other);
            var validateDateYear = validateDate.getFullYear();
            var years = [];
            var counter = 0;
            for (var i = startYear; i < startYear + 10; i++) {
                var yearDesc = startYear + counter;
                var linkState = '';
                if (LeftOrRight === 'left') {
                    if (yearDesc > validateDateYear) {
                        linkState = 'disabled';
                    }
                } else if (LeftOrRight === 'right') {
                    if (yearDesc < validateDateYear) {
                        linkState = 'disabled';
                    }
                }
                years.push({
                    desc: yearDesc,
                    state: linkState
                });
                counter++;
            }
            return years;
        }

        function validateMonths(current, other, LeftOrRight) {
            var start = new Date(current);
            var startYear = start.getYear();
            var validateDate = new Date(other);
            var validateDateYear = validateDate.getYear();
            var months = getMonths();

            if (startYear === validateDateYear) {
                var validateDateMonth = validateDate.getMonth();
                for (var i = 0; i < months.length; i++) {
                    var linkState = '';
                    if (LeftOrRight === 'left') {
                        if (months[i].id > validateDateMonth) {
                            linkState = 'disabled';
                        }
                    }
                    if (LeftOrRight === 'right') {
                        if (months[i].id < validateDateMonth) {
                            linkState = 'disabled';
                        }
                    }
                    months[i].state = linkState;
                }
            }
            return months;
        }

        function getMonths() {
            var months = [{
                id: 0,
                name: 'January',
                state: ''
            }, {
                id: 1,
                name: 'February',
                state: ''
            }, {
                id: 2,
                name: 'March',
                state: ''
            }, {
                id: 3,
                name: 'April',
                state: ''
            }, {
                id: 4,
                name: 'May',
                state: ''
            }, {
                id: 5,
                name: 'June',
                state: ''
            }, {
                id: 6,
                name: 'July',
                state: ''
            }, {
                id: 7,
                name: 'August',
                state: ''
            }, {
                id: 8,
                name: 'September',
                state: ''
            }, {
                id: 9,
                name: 'October',
                state: ''
            }, {
                id: 10,
                name: 'November',
                state: ''
            }, {
                id: 11,
                name: 'December',
                state: ''
            }];
            return months;
        }

        function prepareRanges(scope) {
            if (scope.ranges[scope.ranges.length - 1].range !== CUSTOM) {
                scope.ranges.push({
                    label: 'Custom',
                    range: CUSTOM
                });
            }

            scope.resetRangeClass();
            //if (scope.ranges[scope.ranges.length - 1].active) {
            scope.showCalendars = true;
            scope.showLeftCalendar = true;
            scope.showRightCalendar = true;
            //}
        }

        function prepareMonths(scope) {
            scope.months = [];
            var start = null;
            var end = null;
            if (scope.model) {
                start = scope.model.start;
                end = scope.model.end;
            }

            if (!start) {
                start = moment();
            }
            if (!end) {
                end = moment().add(1, 'month');
            }

            scope.months.push(createMonth(start.clone().startOf('month')));
            scope.months.push(createMonth(start.clone().startOf('month').add(1, 'month')));

            scope.months[0].rightArrowClass = 'unavailable';
            scope.months[1].leftArrowClass = 'unavailable';

            scope.inputDates = [];
            scope.inputDates.push(start.format('YYYY-MM-DD'));
            scope.inputDates.push(end.format('YYYY-MM-DD'));
        }

        function createMonth(date) {
            var month = {
                name: date.format('MMM YYYY'),
                date: date,
                weeks: getWeeks(date),
                leftArrowClass: 'available',
                rightArrowClass: 'available'
            };
            return month;
        }

        function sameMonth(a, b, other) {
            if (a.month() !== b.month()) {
                return other;
            }
            return a.date();
        }

        function getWeeks(m) {
            var lastOfMonth = m.clone().endOf('month'),
                lastOfMonthDate = lastOfMonth.date(),
                firstOfMonth = m.clone().startOf('month'),
                currentWeek = firstOfMonth.clone().day(0),
                startOfWeek,
                endOfWeek;

            var thisMonth = m.month();
            var thisYear = m.year();

            var weeks = [];
            while (currentWeek < lastOfMonth) {
                startOfWeek = sameMonth(currentWeek.clone().day(0), firstOfMonth, 1);
                endOfWeek = sameMonth(currentWeek.clone().day(6), firstOfMonth, lastOfMonthDate);

                var week = [];
                for (var i = startOfWeek; i <= endOfWeek; i++) {
                    week.push({
                        number: i,
                        date: new Date(thisYear, thisMonth, i)
                    });
                }

                var days = week.length;
                if (days < 7) {
                    if (weeks.length === 0) {
                        while (days < 7) {
                            week.splice(0, 0, {
                                number: false,
                                disabled: true
                            });
                            days += 1;
                        }
                    } else {
                        while (days < 7) {
                            week.push({
                                number: false,
                                disabled: true
                            });
                            days += 1;
                        }
                    }
                }
                weeks.push(week);

                currentWeek.add(7, 'd');
            }

            return weeks;
        }


        // function getDefaultRanges() {
        //     return [{
        //         label: 'Yesterday',
        //         range: moment().range(moment().startOf('day').subtract(1, 'day'), moment().endOf('day'))
        //     }, {
        //         label: 'Last week',
        //         range: moment().range(moment().startOf('week').subtract(1, 'week').startOf('day').add(1, 'day'), moment().startOf('week').startOf('day'))
        //     }, {
        //         label: 'Last month',
        //         range: moment().range(moment().startOf('month').subtract(1, 'month').startOf('day'), moment().startOf('month').startOf('day').subtract(1, 'day'))
        //     }, {
        //         label: 'Last year',
        //         range: moment().range(moment().startOf('year').subtract(1, 'year').startOf('day'), moment().startOf('year').startOf('day').subtract(1, 'day'))
        //     }];
        // }
    }
})(window.moment);
