describe('given datepicker is loaded', function() {
    //Arrange, Act, Assert (AAA)
    let $compile, $scope, element, elementScope, $templateCache;

    beforeEach(function() {
        module('appComponentDatepicker');
        module('templates');
    });

    beforeEach(inject(function(_$rootScope_, _$compile_, _$templateCache_) {
        //Make sure it is loaded
        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        $templateCache = _$templateCache_;

        element = angular.element(' <ta-date-range-picker get-default-ranges="sbFilterBarCtrl.getDefaultRanges()"></ta-date-range-picker>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    describe('when close button is clicked', function() {
        it('it should close the datepicker', function() {
            //Action
            elementScope.hide();

            //Assert
            expect(elementScope.visible).toEqual(false);
        });
    });
});