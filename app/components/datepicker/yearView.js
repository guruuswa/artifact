(function() {
    'use strict';
    angular
        .module('appComponentDatepicker')
        .directive('yearView', yearView);

    function yearView() {
        var directive = {
            templateUrl: 'components/datepicker/yearView.html',
            restrict: 'E',
            replace: true
        };

        return directive;
    }
})();