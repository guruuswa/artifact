(function() {
    'use strict';
    angular
        .module('appComponentDatepicker')
        .directive('dayView', dayView);

    function dayView() {
        var directive = {
            templateUrl: 'components/datepicker/dayView.html',
            restrict: 'E',
            replace: true
        };

        return directive;
    }
})();