(function() {
    'use strict';
    angular
        .module('appComponentDatepicker')
        .directive('monthView', monthView);

    function monthView() {
        var directive = {
            templateUrl: 'components/datepicker/monthView.html',
            restrict: 'E',
            replace: true
        };

        return directive;
    }
})();