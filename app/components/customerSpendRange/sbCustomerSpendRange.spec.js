describe('sbCustomerSpendRange', () => {
    let $compile, $scope, $httpBackend, configApiEndpoint, element, elementScope, merchantArguments, spendRangeArguments;

    beforeEach(inject((_$compile_, _$rootScope_, _$httpBackend_, _configApiEndpoint_) => {
        $httpBackend = _$httpBackend_;
        $compile = _$compile_;
        $scope = _$rootScope_.$new();
        configApiEndpoint = _configApiEndpoint_;

        merchantArguments = ['POST', configApiEndpoint, undefined, (headers) => headers.Namespace === 'merchants'];
        spendRangeArguments = ['POST', configApiEndpoint, undefined, (headers) => headers.Namespace === 'spendRange'];
        $httpBackend.when.apply(null, merchantArguments).respond(MOCK_MERCHANTS);
        $httpBackend.when.apply(null, spendRangeArguments).respond(MOCK_SPEND_RANGE);

        element = angular.element('<sb-customer-spend-range></sb-customer-spend-range>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();

        $httpBackend.flush();
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });


    describe('sbCustomerSpendRange created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-customerSpendRange')).toBeGreaterThan(-1);
        });
    });
});