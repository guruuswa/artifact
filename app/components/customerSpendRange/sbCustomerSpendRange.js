(function () {
    angular
        .module('appComponentCustomerSpendRange')
        .directive('sbCustomerSpendRange', directive);

    function directive($q, merchantsFactory, loadingHelper, customerSpendRangeFactory, filtersFactory) {
        return {
            scope: {},
            restrict: 'E',
            replace: true,
            templateUrl: 'components/customerSpendRange/sbCustomerSpendRange.html',
            link: link
        };

        function link(scope) {
            angular.extend(scope, {
                merchantList: merchantsFactory.getWithAllStores().result,
                activeMerchant: merchantsFactory.getActive(),
                activeMerchantIndex: 0,
                activeLegendIndex: 0,
                activeDates: {
                    main: undefined,
                    compare: undefined
                },
                customerSpendData: undefined,
                customerSpendDataCompare: undefined,
                pieColors: ['#0E4190', '#3A64AE', '#10AADE', '#23AC77', '#A91352', '#ED672A', '#FAB62B'],
                setActiveLegendCallback: setActiveLegend,
                setActiveMerchantCallback: setActiveMerchant
            });


            merchantsFactory.activeUpdate.subscribe(scope, () => {
                scope.activeMerchant = merchantsFactory.getActive();
                scope.filterStores.map(x => {
                    x.isActive = x.value === scope.activeMerchant.id || scope.activeMerchant.id === null;
                    return x;
                });
                scope.activeMerchantIndex = scope.filterStores.findIndex(element => element.isActive);
                reload(scope.activeMerchant);
            });

            merchantsFactory.getWithAllStores().then(data => {
                scope.filterStores = data.map(x => {
                    return {
                        value: x.id,
                        label: x.name,
                        isActive: true
                    };
                });
                reload(scope.activeMerchant);
            });


            function setActiveMerchant(merchant) {
                scope.activeMerchant = merchant;
                reload(merchant);
            }

            function setActiveLegend(index) {
                scope.activeLegendIndex = index;
            }

            function reload(sliderMerchant) {
                const LOADING_DATA = 'sbSalesByStore';
                const LOADING_DATA_TARGET = 'customerSpendRange';
                let storesQuery, activeFilters, promise, promiseCompare, filterQuery;

                loadingHelper.start(LOADING_DATA, LOADING_DATA_TARGET);

                storesQuery = sliderMerchant && sliderMerchant.id ?
                    merchantsFactory.getQueryArray(sliderMerchant) :
                    merchantsFactory.getQueryArray.apply(null, merchantsFactory.get().result);
                activeFilters = filtersFactory.getActive();
                filterQuery = filtersFactory.getQueryArray();

                promise = customerSpendRangeFactory.get(storesQuery, activeFilters.dateRange, filterQuery);
                if (activeFilters.compareDateRange.dateFrom && activeFilters.compareDateRange.dateTo) {
                    promiseCompare = customerSpendRangeFactory.get(storesQuery, activeFilters.compareDateRange, filterQuery);
                }

                $q.all([promise, promiseCompare]).then((data) => {
                    let indexes = data.map(d => !d ? -1 : d.findIndex(x => x.value > 0)).filter(f => f !== -1);
                    scope.customerSpendData = data[0];
                    scope.customerSpendDataCompare = data[1];
                    scope.activeDates.main = activeFilters.dateRange.dateDescription;
                    scope.activeDates.compare = activeFilters.compareDateRange.dateDescription;
                    setActiveLegend(Math.min.apply(null, indexes));
                }).catch((reason) => {
                    scope.messages = !!scope.messages ? scope.messages : [];
                    scope.messages.push('An error occurred during the operation: ' + reason);
                }).finally(() => {
                    loadingHelper.stop(LOADING_DATA, LOADING_DATA_TARGET);
                });
            }
        }
    }
})();