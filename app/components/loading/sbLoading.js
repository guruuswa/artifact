(function() {
    angular
        .module('appComponentLoading')
        .directive('sbLoading', directive);

    function directive(loadingHelper) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                target: '@target'
            },
            templateUrl: 'components/loading/sbLoading.html',
            link: link
        };

        function link(scope) {
            scope.loadingTracker = loadingHelper.get(scope.target);
            scope.isActive = false;
            scope.$watch('loadingTracker', (newVal) => {
                scope.isActive = !!Object.keys(newVal).length && (scope.target === 'ALL' || !loadingHelper.hasFullPage());
            }, true);
        }
    }
})();