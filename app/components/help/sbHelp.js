(function () {
	angular
		.module('appComponentHelp')
		.directive('sbHelp', directive);

		function directive() {
			return {
				restrict: 'E',
				replace: true,
				templateUrl: 'components/help/sbHelp.html'
			};
		}
})();