describe('sbFilter', () => {
    let $compile, $scope, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        element = angular.element('<sb-filter filter-item="{ options: [{ isActive: false }, { isActive: false }] }"></sb-filter>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    describe('sbFilter created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('filter')).toBeGreaterThan(-1);
        });

        it('sets activeId', () => {
            let id = 0;
            elementScope.selectPopup(id);
            expect(elementScope.item.options[1].isActive).toBe(false);
        });
    });
});