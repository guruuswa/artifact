(function() {
    angular
        .module('appComponentFilter')
        .directive('sbFilter', directive);

    function directive() {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                item: '=filterItem',
                open: '=filterOpenCallback'
            },
            templateUrl: 'components/filter/sbFilter.html',
            link: link
        };

        function link(scope) {

            scope.selectPopup = (id) => {
                scope.item.activeId = id;
                scope.item.popupOpen = false;
                scope.item.options[id].isActive = true;

                for (var i = 0; i < scope.item.options.length; i++) {
                    if (i !== id) {
                        scope.item.options[i].isActive = false;
                    }
                }
            };
        }
    }
})();