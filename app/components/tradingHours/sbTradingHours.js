(function () {
    angular
        .module('appComponentTradingHours')
        .directive('sbTradingHours', directive);

    function directive(tradingHoursFactory, merchantsFactory) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            templateUrl: 'components/tradingHours/sbTradingHours.html',
            link: link
        };

        function link(scope) {
            angular.extend(scope, {
                pieColors: {
                    slicePalette: []
                }
            });

            scope.getData = function (activeMerchant) {
                //get value data
                tradingHoursFactory.getValue(activeMerchant).then((data) => {
                    scope.valueData = data.objArray;

                    scope.pieColors.slicePalette = [];
                    for (let i = 0; i < data.objArray.length; i++) {
                        //inject colors in the right order
                        scope.pieColors.slicePalette.push(data.objArray[i].color);
                    }

                    scope.valueSlice = 'valueSlice';
                    scope.applyFilterForValue = true;
                }).catch((reason) => {
                    scope.messages = !!scope.messages ? scope.messages : [];
                    scope.messages.push('An error occurred during the operation: ' + reason);
                });

                //get volume data
                tradingHoursFactory.getVolume(activeMerchant).then((data) => {
                    scope.volumeData = data.objArray;

                    scope.pieColors.slicePalette = [];
                    for (let i = 0; i < data.objArray.length; i++) {
                        //inject colors in the right order
                        scope.pieColors.slicePalette.push(data.objArray[i].color);
                    }

                    scope.volumeSlice = 'volumeSlice';
                    scope.applyFilterForVolume = false;
                }).catch((reason) => {
                    scope.messages = !!scope.messages ? scope.messages : [];
                    scope.messages.push('An error occurred during the operation: ' + reason);
                });
            };

            merchantsFactory.activeUpdate.subscribe(scope, () => {
                scope.activeMerchant = merchantsFactory.getActive();
                scope.getData(scope.activeMerchant);
            });

            scope.activeMerchant = merchantsFactory.getActive();
            scope.getData(scope.activeMerchant);

        }
    }
})();