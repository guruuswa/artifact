describe('sbTradingHours', () => {
    const ELASTIC_REQUEST = '/_search';
    const ELASTIC_REQUEST_TEST = RegExp(ELASTIC_REQUEST, 'i');
    let $compile, $scope, element, elementScope, $httpBackend, $q;

    beforeEach(inject((_$compile_, _$rootScope_,_$httpBackend_,_$q_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();
        $httpBackend = _$httpBackend_;
        $q = _$q_;

        element = angular.element('<sb-trading-hours></sb-trading-hours>');
        $compile(element)($scope);

        $httpBackend.when('POST', ELASTIC_REQUEST_TEST).respond();

        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    describe('sbTradingHours created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-tradingHours')).toBeGreaterThan(-1);
        });
    });

    describe('scope data', () => {
        it('has default value for pieColor', () => {
            expect(elementScope.pieColors).toEqual({slicePalette: []});
        });

        it('has activeMerchant data set', () => {
            expect(elementScope.activeMerchant).toBeDefined();
        });

        it('has called the get data function', () => {
            //arrange
            spyOn(elementScope, 'getData');

            //act
            elementScope.getData({id: null, name: 'All stores'});

            //assert
            expect(elementScope.getData).toHaveBeenCalledWith({id: null, name: 'All stores'});
        });
    });
});