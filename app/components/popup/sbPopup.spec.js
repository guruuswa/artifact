describe('sbPopup', () => {
    let $compile, $scope, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        element = angular.element('<sb-popup></sb-popup>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    describe('sbPopup created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-popup')).toBeGreaterThan(-1);
        });
    });
});