(function() {
    angular
        .module('appComponentPopup')
        .directive('sbPopup', directive);

    function directive() {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
                options: '=popupOptions',
                selected: '=popupSelected',
                title: '@popupTitle',
                formName: '@popupFormName',
                doneCallback: '=popupDoneCallback',
                doneDisabled: '=popupDoneDisable'
            },
            templateUrl: 'components/popup/sbPopup.html',
            link: link
        };

        function link(scope, element, attr) {
            if (attr.multiselect === '') {
                scope.multiSelect = true;
            }
        }
    }
})();