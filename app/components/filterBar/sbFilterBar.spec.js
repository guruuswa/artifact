describe('sbFilterBar', () => {
    const REQUEST_URL = 'mocks/spotlightFilters.json';
    const REQUEST_TEST = RegExp(REQUEST_URL, 'i');
    let $compile, $scope, $httpBackend, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_, _$httpBackend_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();
        $httpBackend = _$httpBackend_;

        $httpBackend.when('GET', REQUEST_TEST).respond(MOCK_SPOTLIGHT_FILTERS);
        $httpBackend.expect('GET', REQUEST_TEST);

        element = angular.element('<sb-filter-bar></sb-filter-bar>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    afterEach(() => {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('sbFilterBar created', () => {
        it('has element', () => {
            $httpBackend.flush();
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-filterBar')).toBeGreaterThan(-1);
        });
    });
});
