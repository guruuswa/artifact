(function(moment) {
    angular
        .module('appComponentFilterBar')
        .directive('sbFilterBar', directive);

    function directive(merchantsFactory, filtersFactory, loadingHelper) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            templateUrl: 'components/filterBar/sbFilterBar.html',
            controller: controller,
            controllerAs: 'sbFilterBarCtrl'
        };

        function controller() {
            angular.extend(this, {
                filters: filtersFactory.get().result,
                filterByTitle: 'Filter by',
                compareToTitle: 'Compare to',
                filterDateRange: angular.extend(getDefaultRanges()[1].range, {
                    description: getDefaultRanges()[1].label
                }),
                compareDateRange: undefined,
                datePickerVisible: {
                    0: false,
                    1: false
                },
                refresh: refresh.bind(this),
                setActive: setActive.bind(this),
                setActiveDatepicker: setActiveDatepicker.bind(this),
                getDefaultRanges: getDefaultRanges,
                getCompareRanges: getCompareRanges
            });

            (function() {
                this.refresh();
            }).call(this);

            function refresh() {
                const AGE = this.filters.find(x => x.name === 'ageFilter');
                const RACE = this.filters.find(x => x.name === 'raceFilter');
                const GENDER = this.filters.find(x => x.name === 'genderFilter');

                let ageFilter = AGE ? AGE.options[AGE.activeId] : {};
                let raceFilter = RACE ? RACE.options[RACE.activeId] : {};
                let genderFilter = GENDER ? GENDER.options[GENDER.activeId] : {};

                filtersFactory.setActive(formatFilterRange(this.filterDateRange), formatFilterRange(this.compareDateRange), ageFilter, raceFilter, genderFilter);
                loadingHelper.startFullPage();
                merchantsFactory.activeUpdate.broadcast();
            }

            function setActive(id) {
                this.filters.map(x => {
                    x.popupOpen = x.id === id && !x.popupOpen;
                    return x;
                });
            }

            function setActiveDatepicker(id) {
                angular.forEach(this.datePickerVisible, (val, key) => {
                    if (key !== id) {
                        this.datePickerVisible[key] = false;
                    }
                }, this);
                this.setActive();
            }

            function getDefaultRanges() {
                return [{
                    label: 'Today',
                    range: moment().range(moment().startOf('day'), moment().endOf('day'))
                }, {
                    label: 'This week',
                    range: moment().range(moment().startOf('week').add(1, 'day').startOf('day'), moment().endOf('day'))
                }, {
                    label: 'This month',
                    range: moment().range(moment().startOf('month').startOf('day'), moment().endOf('day'))
                }, {
                    label: 'This year',
                    range: moment().range(moment().startOf('year').startOf('day'), moment().endOf('day'))
                }];
            }

            function getCompareRanges() {
                return [{
                    label: 'Yesterday',
                    range: moment().range(moment().startOf('day').subtract(1, 'day'), moment().endOf('day').subtract(1, 'day'))
                }, {
                    label: 'Last week',
                    range: moment().range(moment().startOf('week').subtract(1, 'week').startOf('day').add(1, 'day'), moment().startOf('week').startOf('day'))
                }, {
                    label: 'Last month',
                    range: moment().range(moment().startOf('month').subtract(1, 'month').startOf('day'), moment().startOf('month').startOf('day').subtract(1, 'day'))
                }, {
                    label: 'Last year',
                    range: moment().range(moment().startOf('year').subtract(1, 'year').startOf('day'), moment().startOf('year').startOf('day').subtract(1, 'day'))
                }];
            }

            function formatFilterRange(dateRange) {
                let offset = 7;
                if (dateRange === undefined) {
                    return [];
                }
                return {
                    dateFrom: moment(dateRange.start).subtract(offset, 'month').format('YYYY-MM-DD'),
                    dateTo: moment(dateRange.end).subtract(offset - 1, 'month').format('YYYY-MM-DD'),
                    dateDescription: dateRange.description
                };
            }
        }
    }
})(window.moment);
