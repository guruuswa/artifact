(function () {
    angular
    .module('appComponentCustomerLocation')
    .directive('sbCustomerLocation', directive);

    function directive(merchantsFactory, loadingHelper, customerLocationFactory, googleMapFactory) {
            return {
                    restrict: 'E',
                    replace: true,
                    scope: {},
                    templateUrl: 'components/customerLocation/sbCustomerLocation.html',
                    link: link
            };

            function link(scope, element) {
                const LOADING_DATA = 'customerLocation';
                const LOADING_DATA_TARGET = 'customerLocation';

                scope.location = {};

                scope.getData = getData;
                merchantsFactory.activeUpdate.subscribe(scope, updateOnMerchantChange);

                element.on('load', scope.getData(merchantsFactory.getActive()));

                function getData(activeMerchant){
                    loadingHelper.start(LOADING_DATA, LOADING_DATA_TARGET);
                    let promise = customerLocationFactory.getData(activeMerchant);
                    promise.then(function(result){
                        scope.location = result;
                        updateMapMarkers(scope.location);
                    }).finally(() =>{
                        loadingHelper.stop(LOADING_DATA, LOADING_DATA_TARGET);
                    });
                    return promise;
                }

                function updateOnMerchantChange(){
                    scope.activeMerchant = merchantsFactory.getActive();
                    scope.getData(scope.activeMerchant);

                }

                function updateMapMarkers(locations){
                    googleMapFactory.createMap('map');
                    for(let i =0; i<locations.merchantLocations.length; i++){
                        googleMapFactory.addMarker(locations.merchantLocations[i].lat, locations.merchantLocations[i].lon, '//maps.google.com/mapfiles/ms/icons/red-dot.png');
                    }

                    googleMapFactory.addHeatMap(locations.customerLocations);
                }
                googleMapFactory.createMap('map');
            }
    }
})();
