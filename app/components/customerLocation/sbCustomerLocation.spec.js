describe('sbCustomerLocation', () => {
    const ELASTIC_REQUEST = '/_search';
    const ELASTIC_REQUEST_TEST = RegExp(ELASTIC_REQUEST, 'i');
    let $compile, $scope, element, elementScope, $httpBackend, $q;


    beforeEach(()=>{
        module('appComponentCustomerLocation');
        module(($provide) => {
            $provide.value('googleMapFactory', {createMap: ()=>{}, addMarker: ()=>{}, addHeatMap: ()=>{}});
            $provide.value('tradingDaysFactory', {getData: ()=>{
                let deferred = $q.defer();
                deferred.resolve({name: 'test'});
                return deferred.promise;
            }});

        });

        inject((_$compile_, _$rootScope_,_$httpBackend_, _$q_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();
        $httpBackend = _$httpBackend_;
        $q = _$q_;

        element = angular.element('<sb-customer-location></sb-customer-location>');
        $compile(element)($scope);
        $httpBackend.when('POST', ELASTIC_REQUEST_TEST).respond();

        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    })});

        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-customerLocation')).toBeGreaterThan(-1);
        });

    it('should set the customer location object when getData is called', () => {
        expect(elementScope.location).toEqual({});
        var promise = elementScope.getData();
        promise.then(function () {
            expect(elementScope.location).toEqual({name: 'test'});
        });
    });
});