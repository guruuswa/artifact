(function () {
    angular
        .module('appComponentSlider')
        .directive('sbSlider', directive);

    function directive($timeout) {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            scope: {
                list: '=sliderList',
                active: '=sliderActive',
                activeIndex: '=sliderActiveIndex',
                animationDuration: '@sliderAnimationDuration',
                itemsPerSlide: '@sliderItemsPerSlide'
            },
            templateUrl: 'components/slider/sbSlider.html',
            link: link
        };

        function link(scope, element) {
            let slider = element[0].querySelector('.sb-slider__list');
            let activePage = 0;
            let animationTimeout;

            angular.extend(scope, {
                activeList: [],
                page: 0,
                maxPage: 0,
                inProgress: {
                    next: false,
                    previous: false
                },
                animationDuration: scope.animationDuration || '1000',
                itemsPerSlide: Number(scope.itemsPerSlide || 1),
                nextPage: nextPage,
                previousPage: previousPage
            });

            scope.$watch('list', (newVal) => {
                if (angular.isDefined(newVal)) {
                    scope.maxPage = Math.floor(newVal.length / scope.itemsPerSlide);
                    navigateToPage(scope.page);
                }
            }, true);

            scope.$watch('activeIndex', (newVal) => {
                if (angular.isDefined(newVal)) {
                    scope.page = Math.floor(newVal / scope.itemsPerSlide);
                    navigateToPage(scope.page);
                }
            });

            function nextPage() {
                if (scope.page < scope.maxPage && !scope.inProgress.previous) {
                    scope.page++;
                    navigateToPage(scope.page);
                }
            }

            function previousPage() {
                if (scope.page > 0 && !scope.inProgress.next) {
                    scope.page--;
                    navigateToPage(scope.page);
                }
            }

            function navigateToPage(page) {
                const ITEMS_PER_SLIDE = Number(scope.itemsPerSlide);
                let startPoint, endPoint, slideAmount;
                let transitionVal = slider.style.transition;

                if (animationTimeout) {
                    $timeout.cancel(animationTimeout);
                }

                if (page === scope.maxPage && activePage < page) {
                    startPoint = activePage * ITEMS_PER_SLIDE;
                    endPoint = scope.list.length;
                } else if (activePage === scope.maxPage && activePage > page) {
                    startPoint = page * ITEMS_PER_SLIDE;
                    endPoint = scope.list.length;
                } else {
                    startPoint = (page < activePage ? page : activePage) * ITEMS_PER_SLIDE;
                    endPoint = (page < activePage ? activePage : page) * ITEMS_PER_SLIDE + ITEMS_PER_SLIDE;
                }

                scope.activeList = scope.list.slice(startPoint, endPoint);

                slideAmount = -(slider.getBoundingClientRect().width / ITEMS_PER_SLIDE) * (endPoint - startPoint - ITEMS_PER_SLIDE);

                if (page < activePage) {
                    setStyle('none', slideAmount);
                    $timeout(setStyle.bind(null, transitionVal, 0), 0);
                    scope.inProgress.previous = true;
                } else {
                    setStyle(transitionVal, slideAmount);
                    scope.inProgress.next = true;
                }

                animationTimeout = $timeout(() => {
                    if (page === scope.maxPage && activePage < page) {
                        startPoint = scope.list.length - ITEMS_PER_SLIDE;
                        endPoint = scope.list.length;
                    } else {
                        startPoint = page * ITEMS_PER_SLIDE;
                        endPoint = page * ITEMS_PER_SLIDE + ITEMS_PER_SLIDE;
                    }
                    setStyle('none', 0);
                    scope.activeList = scope.list.slice(startPoint, endPoint);
                    activePage = page;
                    $timeout(setStyle.bind(null, transitionVal, 0), 0);
                    angular.extend(scope.inProgress, {
                        next: false,
                        previous: false
                    });
                }, scope.animationDuration);

                function setStyle(transition, translateX) {
                    slider.style.transition = transition;
                    slider.style.transform = `translateX(${translateX}px)`;
                }
            }
        }
    }
})();
