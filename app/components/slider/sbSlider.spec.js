describe('sbSlider', () => {
    let $compile, $scope, $timeout, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_, _$timeout_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();
        $timeout = _$timeout_;

        element = angular.element('<sb-slider slider-list="[{id: 0}, {id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}]" slider-active="{id: 0}" slider-active-index="0" slider-animation-duration="0" slider-items-per-slide="4"></sb-slider>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();

        $timeout.flush();
    }));


    describe('sbSlider created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-slider')).toBeGreaterThan(-1);
        });
    });

    describe('navigate pages', () => {
        it('can go to the next page', () => {
            elementScope.nextPage();
            expect(elementScope.page).toBe(1);
        })

        it('can go to the previous page', () => {
            elementScope.page = 1;
            elementScope.previousPage();
            expect(elementScope.page).toBe(0);
        });
    });
});