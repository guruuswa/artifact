(function () {
    angular
        .module('appComponentSalesByStore')
        .directive('sbSalesByStore', directive);

    function directive($filter, merchantsFactory, loadingHelper, merchantSalesFactory, filtersFactory) {
        return {
            scope: {},
            restrict: 'E',
            replace: true,
            templateUrl: 'components/salesByStore/sbSalesByStore.html',
            link: link
        };

        function link(scope) {
            angular.extend(scope, {
                merchantList: merchantsFactory.getWithAllStores().result,
                activeMerchant: merchantsFactory.getActive(),
                chartData: [],
                chartColors: {
                    linePalette: [
                        '#FF671B',
                        '#FFB822'
                    ],
                    barPalette: [
                        '#3367CD',
                        '#d7d7d7'
                    ]
                },
                filterType: false,
                filterStores: [],
                showFilterStores: false,
                formatAmount: formatAmount,
                showHideAllStoreChartPopover: showHideAllStoreChartPopover,
                refreshFilters: refreshFilters,
                selectFilterStore: selectFilterStore,
                noneSelected: false
            });


            merchantSalesFactory.onUpdate.subscribe(scope, () => {
                reload();
            });

            merchantsFactory.activeUpdate.subscribe(scope, () => {
                scope.activeMerchant = merchantsFactory.getActive();
                scope.filterStores.map(x => {
                    x.isActive = x.value === scope.activeMerchant.id || scope.activeMerchant.id === null;
                    return x;
                });
                reload();
            });

            merchantsFactory.getWithAllStores().then(data => {
                scope.filterStores = data.map(x => {
                    return {
                        value: x.id,
                        label: x.name,
                        isActive: x.id === scope.activeMerchant.id || scope.activeMerchant.id === null
                    };
                });
                reload();
            });


            function formatAmount(amount) {
                let symbol = !scope.filterType ? 'R ' : '';
                return $filter('filterCurrency')(amount, symbol, 0);
            }

            function showHideAllStoreChartPopover() {
                scope.showFilterStores = !scope.showFilterStores;
                if (!scope.showFilterStores) {
                    reload();
                }
            }

            function refreshFilters() {
                scope.chartData = scope.apiData
                    .map(x => {
                        let valueRange;
                        if(x.shouldCompare) {
                            valueRange = scope.filterType ? [x.volume, x.volumeCompare] : [x.value, x.valueCompare];
                        } else {
                            valueRange = scope.filterType ? [x.volume] : [x.value];
                        }
                        return {
                            value: valueRange,
                            label: x.id + '|' + x.merchant //pipe char splits the words into lines in sbChartBar.js
                        };
                    })
                    .sort((a,b) => b.value[0]-a.value[0]);

                scope.isData = scope.chartData.filter(e => e.value.filter(f => f > 0).length).length;
                scope.hasCompare = scope.apiData[0].shouldCompare;
                scope.currentDateFilter = filtersFactory.getActive().dateRange.dateDescription;
                scope.compareDateFilter = filtersFactory.getActive().compareDateRange.dateDescription;
            }

            function selectFilterStore(index) {
                let activeState = scope.filterStores[index].isActive;
                if (index === 0) {
                    scope.filterStores.map(x => {
                        x.isActive = activeState || (activeState === false && x.value === scope.activeMerchant.id && scope.activeMerchant.id !== null);
                        return x;
                    });
                } else {
                    scope.filterStores[0].isActive = angular.copy(scope.filterStores).filter(x => x.value !== null).every(x => x.isActive);
                }

                scope.noneSelected = scope.filterStores.every(x => !x.isActive);
            }

            function reload() {
                const LOADING_DATA = 'sbSalesByStore';
                const LOADING_DATA_TARGET = 'salesByStore';
                let selectedFilterStores = scope.filterStores.filter(x => x.isActive && x.value !== null).map(x => x.value);

                loadingHelper.start(LOADING_DATA, LOADING_DATA_TARGET);

                if (!selectedFilterStores.length) {
                    getMerchantSalesData(merchantsFactory.getQueryArray(scope.activeMerchant.id));
                } else {
                    getMerchantSalesData(merchantsFactory.getQueryArray.apply(null, selectedFilterStores));
                }

                function getMerchantSalesData(storesArray) {
                    let promise = merchantSalesFactory.get(
                        storesArray,
                        [filtersFactory.getActive().dateRange, filtersFactory.getActive().compareDateRange],
                        filtersFactory.getQueryArray()
                    );

                    promise.then((data) => {
                        scope.apiData = data;
                        refreshFilters();
                    }).catch((reason) => {
                        scope.messages = !!scope.messages ? scope.messages : [];
                        scope.messages.push('An error occurred during the operation: ' + reason);
                    }).finally(() => {
                        loadingHelper.stop(LOADING_DATA, LOADING_DATA_TARGET);
                    });
                }
            }
        }
    }
})();
