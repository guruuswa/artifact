describe('sbSalesByStore', () => {
    const MERCHANTS_REQUEST = '/_search';
    const MERCHANTS_REQUEST_TEST = RegExp(MERCHANTS_REQUEST, 'i');
    let factory, $httpBackend;
    let requestHandler;

    let $compile, $scope, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_, _merchantsFactory_, _$httpBackend_) => {
        factory = _merchantsFactory_;
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend.when('POST', MERCHANTS_REQUEST_TEST).respond(mockMerchants());

        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        element = angular.element('<sb-sales-by-store></sb-sales-by-store>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    describe('sbSalesByStore created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-salesByStore')).toBeGreaterThan(-1);
        });
    });

    function mockMerchants() {
        return [{
            id: 'BP EDENVALE NORTH',
            name: 'BP EDENVALE NORTH'
        }, {
            id: 'BP WINMORE',
            name: 'BP WINMORE'
        }, {
            id: 'BP RIDGE OASIS',
            name: 'BP RIDGE OASIS'
        }, {
            id: 'SASOL RANDHART',
            name: 'SASOL RANDHART'
        }, {
            id: 'VILLAGE ROAD MOTORS',
            name: 'VILLAGE ROAD MOTORS'
        }];
    }
});