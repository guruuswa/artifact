(function () {
    angular
        .module('appComponentSalesTrends')
        .directive('sbSalesTrends', directive);

    function directive($filter, merchantsFactory, loadingHelper, salesTrendsFactory, filtersFactory) {
        return {
            scope: {},
            restrict: 'E',
            replace: true,
            templateUrl: 'components/salesTrends/sbSalesTrends.html',
            link: link
        };

        function link(scope) {
            angular.extend(scope, {
                merchantList: merchantsFactory.getWithAllStores().result,
                activeMerchant: merchantsFactory.getActive(),
                chartDataValues: [],
                chartDataLabels: [],
                chartDataValuesCompare: [],
                chartColors: {
                    color: [
                        'rgba(11, 170, 221, 0.7)'
                    ],
                    colorCompare: [
                        'rgba(204, 204, 204, 0.7)'
                    ]
                },
                filterType: false,
                filterStores: [],
                showFilterStores: false,
                formatAmount: formatAmount,
                showHideAllStoreChartPopover: showHideAllStoreChartPopover,
                refreshFilters: refreshFilters,
                selectFilterStore: selectFilterStore,
                noneSelected: false
            });

            salesTrendsFactory.onUpdate.subscribe(scope, () => {
                reload();
            });

            merchantsFactory.activeUpdate.subscribe(scope, () => {
                scope.activeMerchant = merchantsFactory.getActive();
                scope.filterStores.map(x => {
                    x.isActive = x.value === scope.activeMerchant.id || scope.activeMerchant.id === null;
                    return x;
                });
                reload();
            });

            merchantsFactory.getWithAllStores().then(data => {
                scope.filterStores = data.map(x => {
                    return {
                        value: x.id,
                        label: x.name,
                        isActive: x.id === scope.activeMerchant.id || scope.activeMerchant.id === null
                    };
                });
                reload();
            });

            function formatAmount(amount) {
                let symbol = !scope.filterType ? 'R ' : '';
                return $filter('filterCurrency')(amount, symbol, 0);
            }

            function refreshFilters() {

                scope.chartDataValues = scope.chartData.map((x) => {
                    return x.salesValue;
                });

                scope.chartDataLabels = scope.chartData.map((x) => {
                    return x.salesDate;
                });

                scope.hasCompare = scope.chartData.map((x) => {
                    return x.shouldCompare;
                });
                scope.isData = scope.chartDataValues.filter(e => e > 0).length;
                scope.currentDateFilter = filtersFactory.getActive().dateRange.dateDescription;
                scope.compareDateFilter = filtersFactory.getActive().compareDateRange.dateDescription;
                scope.hasCompare = (scope.compareDateFilter ? true : false);
            }

            function showHideAllStoreChartPopover() {
                scope.showFilterStores = !scope.showFilterStores;
                if (!scope.showFilterStores) {
                    reload();
                }
            }

            function selectFilterStore(index) {
                let activeState = scope.filterStores[index].isActive;
                if (index === 0) {
                    scope.filterStores.map(x => {
                        x.isActive = activeState || (activeState === false && x.value === scope.activeMerchant.id && scope.activeMerchant.id !== null);
                        return x;
                    });
                } else {
                    scope.filterStores[0].isActive = angular.copy(scope.filterStores).filter(x => x.value !== null).every(x => x.isActive);
                }

               scope.noneSelected = scope.filterStores.every(x => !x.isActive);
            }

            function reload() {
                const LOADING_DATA = 'sbSalesTrends';
                const LOADING_DATA_TARGET = 'salesTrends';
                let selectedFilterStores = scope.filterStores.filter(x => x.isActive && x.value !== null).map(x => x.value);

                loadingHelper.start(LOADING_DATA, LOADING_DATA_TARGET);

                if (!selectedFilterStores.length) {
                    getSalesTrends(merchantsFactory.getQueryArray(scope.activeMerchant.id));
                } else {
                    getSalesTrends(merchantsFactory.getQueryArray.apply(null, selectedFilterStores));
                }

                function getSalesTrends(storesArray) {
                    let promise = salesTrendsFactory.get(
                        storesArray,
                        [filtersFactory.getActive().dateRange, filtersFactory.getActive().compareDateRange],
                        filtersFactory.getQueryArray()
                    );

                    promise.then((data) => {
                        scope.chartData = data;
                        refreshFilters();
                        
                        if (scope.hasCompare) {
                            scope.chartDataValuesCompare = scope.chartData.map((x) => {
                                return x.salesValueCompare;
                            });
                        }
                    }).catch((reason) => {
                        scope.messages = !!scope.messages ? scope.messages : [];
                        scope.messages.push('An error occurred during the operation: ' + reason);
                    }).finally(() => {
                        loadingHelper.stop(LOADING_DATA, LOADING_DATA_TARGET);
                    });
                }
            }
        }
    }
})();
