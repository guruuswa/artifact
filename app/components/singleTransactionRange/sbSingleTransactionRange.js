(function () {
    angular
        .module('appComponentSingleTransactionRange')
        .directive('sbSingleTransactionRange', directive);

    function directive(singleTransactionFactory, merchantsFactory, loadingHelper) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            templateUrl: 'components/singleTransactionRange/sbSingleTransactionRange.html',
            controller: controller,
            controllerAs: 'sbSingleTransactionRangeCtrl',
            link: link
        };

        function controller() {
            //controller logic
        }

        function link(scope) {
            const LOADING_DATA = 'singleTransactionRange';
            const LOADING_DATA_TARGET = 'singleTransactionRange';
            let highestComplete = false;
            let lowestComplete = false;

            scope.getData = function (activeMerchant) {
                loadingHelper.start(LOADING_DATA, LOADING_DATA_TARGET);

                singleTransactionFactory.getHighest(activeMerchant).then((result) => {
                    scope.higestTransaction = result;
                }).finally(() => {
                    highestComplete = true;
                    if (lowestComplete) {
                        loadingHelper.stop(LOADING_DATA, LOADING_DATA_TARGET);
                    }
                });

                singleTransactionFactory.getLowest(activeMerchant).then((result) => {
                    scope.lowestTransaction = result;
                }).finally(() => {
                    lowestComplete = true;
                    if (highestComplete) {
                        loadingHelper.stop(LOADING_DATA, LOADING_DATA_TARGET);
                    }
                });
            };

            merchantsFactory.activeUpdate.subscribe(scope, () => {
                scope.activeMerchant = merchantsFactory.getActive();
                scope.getData(scope.activeMerchant);
            });

            scope.activeMerchant = merchantsFactory.getActive();
            scope.getData(scope.activeMerchant);
        }
    }
})();