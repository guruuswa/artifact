describe('sbSingleTransactionRange', () => {
    const ELASTIC_REQUEST = '/_search';
    const ELASTIC_REQUEST_TEST = RegExp(ELASTIC_REQUEST, 'i');
    let $compile, $scope, element, elementScope, $httpBackend;

    beforeEach(inject((_$compile_, _$rootScope_,_$httpBackend_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();
        $httpBackend = _$httpBackend_;

        element = angular.element('<sb-single-transaction-range></sb-single-transaction-range>');
        $compile(element)($scope);
        
        $httpBackend.when('POST', ELASTIC_REQUEST_TEST).respond();
        
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    describe('sbSingleTransactionRange created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-singleTransactionRange')).toBeGreaterThan(-1);
        });
    });
});