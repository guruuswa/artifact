(function () {
    angular
        .module('appComponentToolbar')
        .directive('sbToolbar', directive);

    function directive($location, merchantsFactory) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            templateUrl: 'components/toolbar/sbToolbar.html',
            controller: controller,
            controllerAs: 'sbToolbarCtrl',
            link: link
        };

        function controller($scope) {
            this.activeMerchant = merchantsFactory.getActive();
            merchantsFactory.activeUpdate.subscribe($scope, () => {
                this.activeMerchant = merchantsFactory.getActive();
            });

            this.routes = [{
                name: 'Overview',
                url: ''
            }, {
                name: 'Your Sales',
                url: 'your-sales'
            }, {
                name: 'Your Customers',
                url: 'your-customers'
            }];
        }

        function link(scope) {
            function updatePage() {
                scope.pageChange = (pageItemUrl) => {
                    scope.activePageUrl = $location.path().substring(1);

                    if (scope.activePageUrl === '' && pageItemUrl === '' || pageItemUrl === scope.activePageUrl) {
                        return true;
                    }
                };
            }
            updatePage();
        }
    }
})();
