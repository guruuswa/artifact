describe('sbToolbar', () => {
    let $compile, $scope, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        element = angular.element('<sb-toolbar></sb-toolbar>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    describe('sbToolbar created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-toolbar')).toBeGreaterThan(-1);
        });
    });
});