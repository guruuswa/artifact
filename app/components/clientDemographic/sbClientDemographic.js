(function () {
    angular
    .module('appComponentClientDemographic')
    .directive('sbClientDemographic', directive);

    function directive(clientDemographicFactory, merchantsFactory) {
        return {
                restrict: 'E',
                replace: true,
                scope: {},
                templateUrl: 'components/clientDemographic/sbClientDemographic.html',
                link: link
        };

        function link(scope) {
            angular.extend(scope, {
                pieColors: {
                    slicePalette: []
                }
            });

            scope.getData = function(activeMerchant){
                 //get race data
                 clientDemographicFactory.getRace(activeMerchant).then((data) => {
                     scope.clientDemographicRaceData = data.objArray;

                     scope.pieColors.slicePalette = [];
                     for (let i = 0; i < data.objArray.length; i++) {
                         //inject colors in the right order
                         scope.pieColors.slicePalette.push(data.objArray[i].color);
                     }
                 }).catch((reason) => {
                     scope.messages = !!scope.messages ? scope.messages : [];
                     scope.messages.push('An error occurred during the operation: ' + reason);
                 });

                 //get gender data
                 clientDemographicFactory.getGender(activeMerchant).then((data) => {
                     scope.clientDemographicGenderData = data.objArray;

                     for (let i = 0; i < data.objArray.length; i++) {
                        if(data.objArray[i].label === 'M'){
                            scope.clientDemographicMaleData = data.objArray[i];
                            scope.clientDemographicMaleData.label = 'Male';
                        }
                        else if(data.objArray[i].label === 'F'){
                            scope.clientDemographicFemaleData = data.objArray[i];
                            scope.clientDemographicFemaleData.label = 'Female';
                        }
                     }
                 }).catch((reason) => {
                    scope.messages = !!scope.messages ? scope.messages : [];
                    scope.messages.push('An error occurred during the operation: ' + reason);
                 });
            };

            merchantsFactory.activeUpdate.subscribe(scope, () => {
                scope.activeMerchant = merchantsFactory.getActive();
                scope.getData(scope.activeMerchant);
            });

            scope.activeMerchant = merchantsFactory.getActive();
            scope.getData(scope.activeMerchant);
        }
    }
})();
