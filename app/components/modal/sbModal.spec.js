describe('sbModal', () => {
    let $compile, $scope, element, elementScope;

    beforeEach(inject((_$compile_, _$rootScope_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();

        element = angular.element('<sb-modal></sb-modal>');
        $compile(element)($scope);
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

    describe('sbModal created', () => {
        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('modal')).toBeGreaterThan(-1);
        });

        it('sets isActive false on close click', () => {
            elementScope.closeModal();
            expect(elementScope.isActive).toBe(false);
        });
    });
});