(function () {
    angular
        .module('appComponentModal')
        .directive('sbModal', directive);

        function directive() {
            return {
                restrict: 'E',
                replace: true,
                transclude: true,
                scope: {
                    title: '@modalTitle',
                    showSubmit: '@showSubmit',
                    isActive: '=modalShow'
                },
                templateUrl: 'components/modal/sbModal.html',
                link: link
            };

            function link(scope) {
                scope.closeModal = function() {
                    scope.isActive = false;
                };
            }
        }
})();