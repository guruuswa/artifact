describe('sbSingleTransactionRange', () => {
    const ELASTIC_REQUEST = '/_search';
    const ELASTIC_REQUEST_TEST = RegExp(ELASTIC_REQUEST, 'i');
    let $compile, $scope, element, elementScope, $httpBackend, factory, $q;

    beforeEach(inject((_$compile_, _$rootScope_,_$httpBackend_, _tradingDaysFactory_, _$q_) => {
        $compile = _$compile_;
        $scope = _$rootScope_.$new();
        $httpBackend = _$httpBackend_;
        factory = _tradingDaysFactory_;
        $q = _$q_;


        element = angular.element('<sb-trading-days></sb-trading-days>');
        $compile(element)($scope);
        
        $httpBackend.when('POST', ELASTIC_REQUEST_TEST).respond();
        
        $scope.$digest();

        elementScope = element.isolateScope() || element.scope();
    }));

        it('has element', () => {
            expect(element).toBeDefined();
            expect(element[0].className.indexOf('sb-tradingDays')).toBeGreaterThan(-1);
        });

    it('should set the trading days object when getData is called', () => {
        spyOn(factory, 'getData').and.returnValue($q.when({name: 'test'}));

        expect(elementScope.tradingDays).toEqual({});
        var promise = elementScope.getData();
        promise.then(function () {
            expect(elementScope.tradingDays).toEqual({name: 'test'});
        });
    });



});