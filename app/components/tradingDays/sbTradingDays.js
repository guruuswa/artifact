(function () {
    angular
        .module('appComponentTradingDays')
        .directive('sbTradingDays', directive);

    function directive(merchantsFactory, loadingHelper, tradingDaysFactory) {
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            templateUrl: 'components/tradingDays/sbTradingDays.html',
            link: link
        };

        function link(scope, element) {
            const LOADING_DATA = 'tradingDays';
            const LOADING_DATA_TARGET = 'tradingDays';

            scope.tradingDays = {};
            scope.tooltip = {
                show: false,
                x: 0,
                y: 0
            };

            scope.getData = getData;
            scope.showValueTooltip = showValueTooltip;
            scope.showVolumeTooltip = showVolumeTooltip;
            scope.hideTooltip = hideTooltip;
            merchantsFactory.activeUpdate.subscribe(scope, updateOnMerchantChange);

            element.on('load', scope.getData(merchantsFactory.getActive()));

            function getData(activeMerchant) {
                loadingHelper.start(LOADING_DATA, LOADING_DATA_TARGET);
                let promise = tradingDaysFactory.getData(activeMerchant);
                promise.then(function (result) {
                    scope.tradingDays = result;
                }).finally(() => {
                    loadingHelper.stop(LOADING_DATA, LOADING_DATA_TARGET);
                });
                return promise;
            }

            function updateOnMerchantChange() {
                scope.activeMerchant = merchantsFactory.getActive();
                scope.getData(scope.activeMerchant);
            }

            function showValueTooltip(event, day) {
                let element = angular.element(event.target);
                let xPosition = element.prop('offsetLeft') + (element.prop('offsetWidth') / 2);
                let yPosition = element.prop('offsetTop') + element.prop('offsetHeight') + 39;

                scope.tooltip.valueShow = true;
                setTooltipValues(xPosition, yPosition, day);
            }

            function showVolumeTooltip(event, day) {
                let element = angular.element(event.target);
                let xPosition = element.prop('offsetLeft') + (element.prop('offsetWidth') / 2);
                let yPosition = element.prop('offsetTop') + element.prop('offsetHeight') + 154;

                scope.tooltip.volumeShow = true;
                setTooltipValues(xPosition, yPosition, day);
            }

            function setTooltipValues(xPosition, yPosition, day) {
                scope.tooltip.x = xPosition;
                scope.tooltip.y = yPosition;
                scope.tooltip.value = day.value;
                scope.tooltip.volume = day.volume;
            }

            function hideTooltip() {
                scope.tooltip.volumeShow = false;
                scope.tooltip.valueShow = false;
            }
        }
    }
})();