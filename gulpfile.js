'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var del = require('del');
var fs = require('fs');
var karma = require('karma');
var mergeStream = require('merge-stream');
var proxyMiddleware = require('http-proxy-middleware');
var runSequence = require('run-sequence');
var wiredep = require('wiredep');
var yargs = require('yargs');

var exec = require('child_process').exec;
var protractor = require("gulp-protractor").protractor;

var _env = yargs.argv.env || process.env.NODE_ENV || 'debug';
var _config = getConfig();
var _src = getSrc();
var _version = getVersion();

//
// Proxy
//

var proxy = proxyMiddleware(_config.apiEndpoint, {target: _config.apiRoot});

//
// MAIN TASKS
//

// compile task
gulp.task('compile', ['clean'], function(done) {
    return runSequence(['bower', 'constant', 'scripts', 'styles', 'images', 'html', 'copy'], 'index', 'manifest', done);
});

// build task
gulp.task('build', ['compile'], function(done) {
    return runSequence('unit-test', done);
});

// default task
gulp.task('default', ['compile'], function(done) {
    return runSequence('watch', done);
});

// index task
gulp.task('index', function() {
    return gulp.src(prefixGlob(appDir(), _src.index))
        .pipe($.if(_config.debug, $.plumber()))
        .pipe($.tokenReplace(getOptions('tokenReplace')))
        .pipe($.inject(gulp.src(prefixGlob(destDir() + 'libs/', _src.js), {
            read: false
        }), Object.assign(getOptions('inject'), {
            name: 'libs'
        })))
        .pipe($.inject(gulp.src(prefixGlob(destDir() + 'libs/', _src.css), {
            read: false
        }), Object.assign(getOptions('inject'), {
            name: 'libs'
        })))
        .pipe($.inject(gulp.src(prefixGlob(destDir() + 'config/', _src.js), {
            read: false
        }), Object.assign(getOptions('inject'), {
            name: 'config'
        })))
        .pipe($.inject(gulp.src(prefixGlob(destDir() + 'templates/', _src.js), {
            read: false
        }), Object.assign(getOptions('inject'), {
            name: 'templates'
        })))
        .pipe($.inject(gulp.src(prefixGlob(destDir() + 'js/', _src.js), {
            read: false
        }), Object.assign(getOptions('inject'), {
            name: 'app'
        })))
        .pipe($.inject(gulp.src(prefixGlob(destDir() + 'css/', _src.css), {
            read: false
        }), Object.assign(getOptions('inject'), {
            name: 'app'
        })))
        .pipe(gulp.dest(destDir()));
});

// html task
gulp.task('html', function() {
    return gulp.src(prefixGlob(appDir(), _src.html))
        .pipe($.if(_config.debug, $.plumber()))
        .pipe($.minifyHtml())
        .pipe($.angularTemplatecache(getOptions('angularTemplatecache')))
        .pipe($.uglify())
        .pipe(gulp.dest(destDir() + 'templates'));
});

// scripts task
gulp.task('scripts', function() {
    return gulp.src(prefixGlob(appDir(), _src.js))
        .pipe($.if(_config.debug, $.plumber()))
        .pipe($.jshint('.jshintrc'))
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.jshint.reporter('fail'))
        .pipe($.if(_config.sourcemaps, $.sourcemaps.init()))
        .pipe($.ngAnnotate())
        .pipe($.babel())
        .pipe($.if(!_config.debug, $.concat('app.js')))
        .pipe($.if(!_config.debug, $.uglify()))
        .pipe($.if(_config.sourcemaps, $.sourcemaps.write()))
        .pipe(gulp.dest(destDir() + 'js'));
});

// styles task
gulp.task('styles', function() {
    return gulp.src(prefixGlob(appDir(), _src.scss))
        .pipe($.if(_config.debug, $.plumber()))
        .pipe($.if(_config.sourcemaps, $.sourcemaps.init()))
        .pipe($.sass(getOptions('sass')))
        .pipe($.autoprefixer())
        .pipe($.if(!_config.debug, $.minifyCss()))
        .pipe($.if(_config.sourcemaps, $.sourcemaps.write()))
        .pipe(gulp.dest(destDir() + 'css'));
});

// images task
gulp.task('images', function() {
    return gulp.src(prefixGlob(appDir(), _src.img))
        .pipe($.if(_config.debug, $.plumber()))
        .pipe($.imagemin())
        .pipe(gulp.dest(destDir()));
});

// bower task
gulp.task('bower', function() {
    var js;
    var css;
    var streams = [];

    if (wiredep().js) {
        js = gulp.src(wiredep().js)
            .pipe($.if(_config.debug, $.plumber()))
            .pipe($.concat('libs.js'))
            .pipe($.if(!_config.debug, $.uglify()))
            .pipe(gulp.dest(destDir() + 'libs'));
        streams.push(js);
    }

    if (wiredep().css) {
        css = gulp.src(wiredep().css)
            .pipe($.if(_config.debug, $.plumber()))
            .pipe($.concat('libs.css'))
            .pipe($.if(!_config.debug, $.minifyCss()))
            .pipe(gulp.dest(destDir() + 'libs'));
        streams.push(css);
    }

    return mergeStream(streams);
});

// constant task
gulp.task('constant', function() {
    return $.ngConstantFork(getOptions('ngConstantFork'))
        .pipe($.if(_config.debug, $.plumber()))
        .pipe(gulp.dest(destDir() + 'config'));
});

// copy task
gulp.task('copy', function() {
    return gulp.src(prefixGlob(appDir(), _src.copy))
        .pipe($.if(_config.debug, $.plumber()))
        .pipe(gulp.dest(destDir()));
});

// clean task
gulp.task('clean', function(done) {
    del(destDir(), done);
});

// manifest task
// TODO Replace with Service Workers in future
gulp.task('manifest', function() {
    return gulp.src(prefixGlob(destDir(), ['**/*.*']))
        .pipe($.if(_config.debug, $.plumber()))
        .pipe($.manifest(getOptions('manifest')))
        .pipe(gulp.dest(destDir()));
});

// version task
gulp.task('version', function() {
    return gulp.src(prefixGlob(appDir(), `config.${_env}.json`))
        .pipe($.if(_config.debug, $.plumber()))
        .pipe($.bump(getOptions('bump')))
        .pipe(gulp.dest(appDir()));
});

// unit-test task
gulp.task('unit-test', function (done) {
    new karma.Server(getOptions('karma'), done).start();
});

// watch task
gulp.task('watch', function() {
    browserSync(getOptions('browserSync'));
    gulp.watch(prefixGlob(appDir(), _src.config), () => {
        return runSequence('constant', 'index', 'manifest', browserSync.reload);
    });
    gulp.watch(prefixGlob(appDir(), _src.js), () => {
        return runSequence('scripts', 'unit-test', 'index', 'manifest', browserSync.reload);
    });
    gulp.watch(prefixGlob(appDir(), _src.scss), () => {
        return runSequence('styles', 'index', 'manifest', browserSync.reload);
    });
    gulp.watch(prefixGlob(appDir(), _src.img), () => {
        return runSequence('images', 'manifest', browserSync.reload);
    });
    gulp.watch(prefixGlob(appDir(), _src.html), () => {
        return runSequence('html', 'unit-test', 'index', 'manifest', browserSync.reload);
    });
    gulp.watch(prefixGlob(appDir(), _src.copy), () => {
        return runSequence('copy', 'manifest', browserSync.reload);
    });
    gulp.watch(prefixGlob(appDir(), _src.index), () => {
        return runSequence('index', 'manifest', browserSync.reload);
    });
    gulp.watch(prefixGlob(appDir(), _src.spec), () => {
        return runSequence('unit-test', browserSync.reload);
    });
});


// temporay hack for google maps
var replace = require('gulp-replace');

gulp.task('fix_google_maps', ['compile'], function () {
    return gulp.src([destDir() + 'index.html'])
        .pipe(replace('https://maps.googleapis.com/maps/api/js', 'http://localhost'))
        .pipe(gulp.dest(destDir()));
}); 

// distributable
gulp.task('distri', ['compile'], function (done) {
    exec('cd builds/staging && tar -czph -f ../web.tgz ./', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        done(err);
    });  
});

// cucumber
gulp.task('cucumber', ['fix_google_maps'], function() {

    browserSync(getOptions('browserSync'));

    var tags = yargs.argv.tags;

    return gulp.src([]).pipe(protractor({ 
        keepAlive: false,
        configFile: "protractor.conf.js",
        args: tags ? ['--cucumberOpts.tags', tags] : []
    }))
    .on('end', function() {
        process.exit();
    })
    .on('error', function() {
        process.exit(1);
    });
});

//
// DIRECTORIES
//

function appDir() {
    return 'app/';
}

function destDir(environment) {
    var directory = 'builds/';
    environment = environment || _env;
    return directory + environment + '/';
}

function configDir() {
    return appDir() + 'configs/';
}

//
// CONFIGS
//

function getConfig(environment) {
    var defaultConfig;
    var envConfig;
    if (_config) {
        return _config;
    }
    environment = environment || _env;
    defaultConfig = JSON.parse(fs.readFileSync(configDir() + 'config.json'));
    envConfig = JSON.parse(fs.readFileSync(configDir() + `config.${environment}.json`));
    _config = Object.assign(defaultConfig, envConfig);
    return _config;
}

function getSrc() {
    var srcs;
    if (_src) {
        return _src;
    }
    srcs = {
        config: ['**/config*.json'],
        index: ['index.html'],
        html: ['**/*.html', '!index.html'],
        js: ['**/*.js', '!**/*.spec.js', '!**/*.e2e.js'],
        css: ['**/*.css'],
        scss: ['**/*.scss'],
        img: ['**/*.{png,jpg,gif,svg}'],
        spec: ['**/*.spec.js']
    };
    srcs.copy = getCopyGlob(srcs);
    return srcs;
}

function getVersion() {
    var configVersion = _config.version;
    var argvVersion = yargs.argv.version;
    if (typeof argvVersion === 'undefined') {
        return configVersion;
    }
    return `${configVersion.slice(0,configVersion.lastIndexOf('.'))}.${argvVersion}`;
}

function getOptions(module) {
    var moduleOptions = {
        angularTemplatecache: {
            module: 'templates',
            standalone: true,
            moduleSystem: 'IIFE'
        },
        sass: {
            outputStyle: 'expanded'
        },
        manifest: {
            timestamp: false,
            hash: true,
            preferOnline: true,
            filename: _config.appcacheManifest,
            exclude: [_config.appcacheManifest, 'web.config', 'coverage/**/*']
        },
        ngConstantFork: {
            name: 'config',
            dest: 'config.js',
            constants: getConstantConfig(),
            noFile: true
        },
        inject: {
            ignorePath: destDir(),
            addRootSlash: false
        },
        tokenReplace: {
            global: _config,
            preserveUnknownTokens: true,
            prefix: '__',
            suffix: '__'
        },
        bump: {
            version: _version
        },
        browserSync: {
            server: {
                baseDir: destDir(),
                port: 3000,
                middleware: [proxy]
            },
            open: false
        },
        karma: {
            browsers: ['PhantomJS'],
            frameworks: ['jasmine'],
            plugins: ['karma-jasmine', 'karma-phantomjs-launcher', 'karma-babel-preprocessor', 'karma-coverage', 'karma-htmlfile-reporter', 'karma-junit-reporter'],
            singleRun: true,
            files: [].concat(
                wiredep({
                    devDependencies: true
                }).js,
                destDir() + 'config/**/*.js',
                destDir() + 'templates/**/*.js',
                destDir() + 'js/**/*.js',
                prefixGlob(appDir(), _src.spec)
            ),
            reporters: ['progress', 'coverage', 'html', 'junit'],
            preprocessors: getKarmaPreprocessors(),
            coverageReporter: {
                dir: `${destDir()}/coverage`,
                reporters: getKarmaReporters(),
                check: getKarmaCheck()
            },
            babelPreprocessor: {
                options: {
                    presets: ['es2015'],
                    sourceMap: 'inline'
                }
            },
            htmlReporter: {
              outputFile: `reports/junit/test-results.html`,
            },
            junitReporter: {
                outputDir: `reports/junit`,
                outputFile: undefined,
                suite: ''
            }
        }
    };
    return (moduleOptions[module] || {});
}

function getKarmaPreprocessors() {
    var karmaPreprocessors = {};
    karmaPreprocessors[prefixGlob(appDir(), _src.spec)] = ['babel'];
    karmaPreprocessors[destDir() + 'js/**/*.js'] = ['coverage'];
    return karmaPreprocessors;
}

function getKarmaReporters() {
    return [{
        type: 'html', subdir: '.'
    }, {
        type: 'cobertura', subdir: '.', file: 'xml-summary.xml'
    }, {
        type: 'text', subdir: '.', file: 'text-summary.txt'
    }];
}

function getKarmaCheck() {
    return {
        global: {
            statements: 0,
            branches: 0,
            functions: 0,
            lines: 0
        }
    };
}

//
// HELPERS
//

function prefixGlob(prefix, glob) {
    glob = Array.isArray(glob) ? glob : [glob];
    return glob.map((el) => {
        var isNeg = el.charAt(0) === '!';
        el = isNeg ? el.replace(/!/, '') : el;
        return (isNeg ? '!' : '') + prefix + el;
    });
}

function getCopyGlob(sources) {
    var copySrc = JSON.parse(JSON.stringify(sources));
    var copyGlob = [];
    for (var key in copySrc) {
        if (!copySrc.hasOwnProperty(key)) {
            continue;
        }
        if (Array.isArray(copySrc[key])) {
            copySrc[key] = copySrc[key].filter((el) => {
                return el.charAt(0) !== '!';
            });
        } else {
            copySrc[key] = copySrc[key].charAt(0) !== '!' ? [copySrc[key]] : [];
        }
        copyGlob = copyGlob.concat(copySrc[key]);
    }
    return ['**/*.*'].concat(prefixGlob('!', copyGlob));
}

function getConstantConfig() {
    var constantConfig = {};
    for (var key in _config) {
        if (!_config.hasOwnProperty(key)) {
            continue;
        }
        constantConfig[`config${toFirstUpper(key)}`] = _config[key];
    }
    return constantConfig;

    function toFirstUpper(str) {
        return str.replace(/\w\S*/g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1);
        });
    }
}
